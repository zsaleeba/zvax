#include <iostream>
#include <vector>
#include <cstdint>
#include <string>
#include <memory>

#include "instruction.h"
#include "instructiondecoder.h"


//
// InstructionDecoder constructor
//
InstructionDecoder::InstructionDecoder() : Component(), instr_buf_addr(0), instr_buf_rd_req(false), instr_buf_out(0), instr_buf_rd_ok(false),
                                           opcode(0), op0mode(0), op0reg(0), op0value(0), op1mode(0), op1reg(0), op1value(0),
                                           op2mode(0), op2reg(0), op2value(0), decoded_ok(false) 
{
    type = Component::Type::INSTRUCTION_DECODER;
}

//
// InstructionDecoder destructor
//
InstructionDecoder::~InstructionDecoder() {}

//
// Method to update inputs
//
void InstructionDecoder::update_inputs() 
{
    // Go through the input ports and update the corresponding attributes
    for (const auto& port : input_ports)
    {
        if (port.id == "instr_buf_out")
        {
            instr_buf_out = port.value;
        } 
        else if (port.id == "instr_buf_rd_ok")
        {
            instr_buf_rd_ok = port.value;
        }
    }
}

//
// Method to execute decode operation
//
void InstructionDecoder::execute() 
{
    // Read the instruction set if we haven't already
    if (!instruction_set) 
    {
        instruction_set = std::make_shared<InstructionSet>();
    }

    // Request data from the instruction buffer
    instr_buf_addr = 0; // Assuming the instruction address is always the start
    instr_buf_rd_req = true;
    std::cout << "Requesting instruction from buffer, addr=" << instr_buf_addr << std::endl;

    // Check if the read from the buffer was successful
    if (instr_buf_rd_ok) 
    {
        instr_buf_rd_req = false; // Reset the read request

        // Decode the instruction
        opcode = static_cast<uint32_t>(instr_buf_out & 0xFF); // First byte is the opcode
        uint32_t instr = static_cast<uint32_t>(instr_buf_out >> 8); // Remaining part of the instruction

        // Decode operand 0
        op0mode = instr & 0xF; // Next 4 bits
        instr >>= 4;
        op0reg = instr & 0xF; // Next 4 bits
        instr >>= 4;

        // Decode operand 1
        op1mode = instr & 0xF; // Next 4 bits
        instr >>= 4;
        op1reg = instr & 0xF; // Next 4 bits
        instr >>= 4;

        // Decode operand 2
        op2mode = instr & 0xF; // Next 4 bits
        instr >>= 4;
        op2reg = instr & 0xF; // Next 4 bits

        // For simplicity, assume operand values are part of the instruction
        op0value = (instr & 0xFFFF); // Assume next 16 bits for operand 0 value
        instr >>= 16;
        op1value = (instr & 0xFFFF); // Assume next 16 bits for operand 1 value
        instr >>= 16;
        op2value = instr; // Remaining bits for operand 2 value

        decoded_ok = true;
        std::cout << "Instruction decoded: opcode=" << opcode << ", op0mode=" << op0mode << ", op0reg=" << op0reg
                  << ", op0value=" << op0value << ", op1mode=" << op1mode << ", op1reg=" << op1reg << ", op1value=" << op1value
                  << ", op2mode=" << op2mode << ", op2reg=" << op2reg << ", op2value=" << op2value << std::endl;
    } 
    else 
    {
        decoded_ok = false;
        std::cout << "Instruction decode failed: buffer read not successful" << std::endl;
    }
}

//
// Method to update the output ports
//
void InstructionDecoder::update_outputs() 
{
    // Update the output ports with the decoded instruction
    for (auto& port : output_ports) 
    {
        if (port.id == "opcode") 
        {
            port.value = opcode;
        } 
        else if (port.id == "op0mode") 
        {
            port.value = op0mode;
        } 
        else if (port.id == "op0reg") 
        {
            port.value = op0reg;
        } 
        else if (port.id == "op0value") 
        {
            port.value = op0value;
        } 
        else if (port.id == "op1mode") 
        {
            port.value = op1mode;
        } 
        else if (port.id == "op1reg") 
        {
            port.value = op1reg;
        } 
        else if (port.id == "op1value") 
        {
            port.value = op1value;
        } 
        else if (port.id == "op2mode") 
        {
            port.value = op2mode;
        } 
        else if (port.id == "op2reg") 
        {
            port.value = op2reg;
        } 
        else if (port.id == "op2value") 
        {
            port.value = op2value;
        } 
        else if (port.id == "decoded_ok") 
        {
            port.value = decoded_ok;
        }
    }
}


//
// Decode the VAX-11/780 opcode
//
bool InstructionDecoder::decode_opcode(uint8_t from_offset, uint8_t bytes_available, uint16_t &opcode, uint8_t &bytes_read, bool &opcode_fault)
{
    // Check if we have enough bytes to read the opcode
    if ( (buf[from_offset] == static_cast<uint8_t>(Opcode::EXTENDED) && bytes_available < 2) || (bytes_available < 1) )
    {
        return false;
    }

    // Read the opcode
    if (buf[from_offset] == static_cast<uint8_t>(Opcode::EXTENDED))
    {
        opcode = static_cast<uint16_t>(0x0100 + buf[from_offset + 1]);
        bytes_read = 2;
    }
    else
    {
        opcode = static_cast<uint16_t>(buf[from_offset]);
        bytes_read = 1;
    }

    // Check if the opcode is valid
    if (!instruction_set->is_valid_opcode(opcode)) 
    {
        opcode_fault = true;
        return false;
    }

    return true;
}

//
// Decode an operand
//
bool InstructionDecoder::decode_operand(uint8_t from_offset, uint8_t bytes_available, Access access, DataType data_type, Operand &operand, uint8_t &bytes_read, bool &operand_fault)
{
    // Check if we have enough bytes to read the operand
    if (bytes_available < 1)
    {
        return false;
    }

    operand.access = access;
    operand.data_type = data_type;
    operand_fault = false;

    // Is this a branch displacement?
    if (access == Access::BRANCH)
    {
        // Read the branch displacement
        operand.addr_mode = AddressingMode::BRANCH;
        return decode_by_data_type(from_offset, bytes_available, data_type, operand.value, operand.value1, operand.value2, bytes_read);
    }

    // Read the first byte of the operand
    uint8_t off = from_offset;
    uint8_t operand_byte = buf[off++];
    bytes_read = 1;

    // Special case literals
    if ((operand_byte 0xc0) == 0)
    {
        operand.addr_mode = AddressingMode::LITERAL;
        operand.value = operand_byte & 0x3F;
        operand.value1 = operand.value;
        operand.value2 = 0;
        return true;
    }

    // Decode the addressing mode
    operand.addr_mode = static_cast<AddressingMode>(operand_byte >> 4);

    // Decode the register number
    operand.reg = operand_byte & 0x0F;

    // Handle indexed mode
    if (operand.addr_mode == AddressingMode::INDEXED)
    {
        // Save the index register
        operand.index_reg = operand.reg;
        bytes_read += 1;

        // Read another byte for the main addressing mode
        if (bytes_available < 2)
        {
            return false;
        }

        // Read the next byte of the operand
        operand_byte = buf[off++];
        bytes_read++;

        // Decode the addressing mode and register number
        operand.addr_mode = static_cast<AddressingMode>(operand_byte >> 4);
        operand.reg = operand_byte & 0x0F;
    }

    // Handle the other addressing modes
    switch (operand.addr_mode)
    {
        case AddressingMode::REGISTER:
        case AddressingMode::REGISTER_DEFERRED:
        case AddressingMode::AUTO_DECREMENT:
        case AddressingMode::AUTO_INCREMENT:
        case AddressingMode::AUTO_INCREMENT_DEFERRED:
            // There's nothing more to do for these modes
            break;

        case AddressingMode::BYTE_DISP:
        case AddressingMode::BYTE_DISP_DEFERRED:
        case AddressingMode::BYTE_RELATIVE:
        case AddressingMode::BYTE_RELATIVE_DEFERRED:
        {
            // Read the byte value
            uint8_t v;
            if (!decode_byte(off, bytes_available, v, bytes_read))
            {
                return false;
            }
            operand.value = static_cast<uint32_t>(v);
            operand.value1 = static_cast<uint64_t>(operand.value);
            operand.value2 = 0;
            break;

        case AddressingMode::WORD_DISP:
        case AddressingMode::WORD_DISP_DEFERRED:
        case AddressingMode::WORD_RELATIVE:
        case AddressingMode::WORD_RELATIVE_DEFERRED:
        {
            // Read the word value
            uint16_t v;
            if (!decode_word(off, bytes_available, v, bytes_read))
            {
                return false;
            }
            operand.value = static_cast<uint32_t>(v);
            operand.value1 = static_cast<uint64_t>(v);
            operand.value2 = 0;
            break;
        }

        case AddressingMode::LONG_DISP:
        case AddressingMode::LONG_DISP_DEFERRED:
        case AddressingMode::LONG_RELATIVE:
        case AddressingMode::LONG_RELATIVE_DEFERRED:
        case AddressingMode::ABSOLUTE:
        {
            // Read the longword value
            uint32_t v;
            if (!decode_longword(off, bytes_available, v, bytes_read))
            {
                return false;
            }
            operand.value = v;
            operand.value1 = static_cast<uint64_t>(v);
            operand.value2 = 0;
            break;
        }

        case AddressingMode::IMMEDIATE:
            // Read an immediate value of any size
            if (!decode_by_data_type(off, bytes_available, data_type, operand.value, operand.value1, operand.value2, bytes_read))
            {
                return false;
            }
        
        default:
            operand_fault = true;
            break;
    }

    return true;
}

//
// Decode the 'case' instruction.
// Actually just help calculate the length of the instruction.
//
bool InstructionDecoder::decode_case(uint32_t base, uint32_t limit, uint8_t &bytes_read)
{
    bytes_read += limit - base;
}

//
// Get a byte from the buffer
//
bool InstructionDecoder::decode_byte(uint8_t from_offset, uint8_t bytes_available, uint8_t &value, uint8_t &bytes_read)
{
    // Check if we have enough bytes to read the value
    if (from_offset + 1 >= bytes_available)
    {
        return false;
    }

    // Read the value
    value = buf[from_offset];
    bytes_read += 1;

    return true;
}

//
// Get a word from the buffer
//
bool InstructionDecoder::decode_word(uint8_t from_offset, uint8_t bytes_available, uint16_t &value, uint8_t &bytes_read)
{
    // Check if we have enough bytes to read the value
    if (from_offset + 2 >= bytes_available)
    {
        return false;
    }

    // Read the value
    value = static_cast<uint16_t>(buf[from_offset]) | (static_cast<uint16_t>(buf[from_offset + 1]) << 8);
    bytes_read += 2;

    return true;
}

//
// Get a longword from the buffer
//
bool InstructionDecoder::decode_longword(uint8_t from_offset, uint8_t bytes_available, uint32_t &value, uint8_t &bytes_read)
{
    // Check if we have enough bytes to read the value
    if (from_offset + 4 >= bytes_available)
    {
        return false;
    }

    // Read the value
    value = static_cast<uint32_t>(buf[from_offset]) | (static_cast<uint32_t>(buf[from_offset + 1]) << 8) |
            (static_cast<uint32_t>(buf[from_offset + 2]) << 16) | (static_cast<uint32_t>(buf[from_offset + 3]) << 24);
    bytes_read += 4;

    return true;
}

//
// Get a quadword from the buffer
//
bool InstructionDecoder::decode_quadword(uint8_t from_offset, uint8_t bytes_available, uint64_t &value, uint8_t &bytes_read)
{
    // Check if we have enough bytes to read the value
    if (from_offset + 8 >= bytes_available)
    {
        return false;
    }

    // Read the value
    value = static_cast<uint64_t>(buf[from_offset]) | (static_cast<uint64_t>(buf[from_offset + 1]) << 8) |
            (static_cast<uint64_t>(buf[from_offset + 2]) << 16) | (static_cast<uint64_t>(buf[from_offset + 3]) << 24) |
            (static_cast<uint64_t>(buf[from_offset + 4]) << 32) | (static_cast<uint64_t>(buf[from_offset + 5]) << 40) |
            (static_cast<uint64_t>(buf[from_offset + 6]) << 48) | (static_cast<uint64_t>(buf[from_offset + 7]) << 56);
    bytes_read += 8;

    return true;
}

//
// Get an octaword from the buffer
//
bool InstructionDecoder::decode_octaword(uint8_t from_offset, uint8_t bytes_available, uint64_t &value1, uint64_t &value2, uint8_t &bytes_read)
{
    // Check if we have enough bytes to read the value
    if (from_offset + 16 >= bytes_available)
    {
        return false;
    }

    // Read the values
    value1 = static_cast<uint64_t>(buf[from_offset]) | (static_cast<uint64_t>(buf[from_offset + 1]) << 8) |
             (static_cast<uint64_t>(buf[from_offset + 2]) << 16) | (static_cast<uint64_t>(buf[from_offset + 3]) << 24) |
             (static_cast<uint64_t>(buf[from_offset + 4]) << 32) | (static_cast<uint64_t>(buf[from_offset + 5]) << 40) |
             (static_cast<uint64_t>(buf[from_offset + 6]) << 48) | (static_cast<uint64_t>(buf[from_offset + 7]) << 56);
    value2 = static_cast<uint64_t>(buf[from_offset + 8]) | (static_cast<uint64_t>(buf[from_offset + 9]) << 8) |
             (static_cast<uint64_t>(buf[from_offset + 10]) << 16) | (static_cast<uint64_t>(buf[from_offset + 11]) << 24) |
             (static_cast<uint64_t>(buf[from_offset + 12]) << 32) | (static_cast<uint64_t>(buf[from_offset + 13]) << 40) |
             (static_cast<uint64_t>(buf[from_offset + 14]) << 48) | (static_cast<uint64_t>(buf[from_offset + 15]) << 56);
    bytes_read += 16;

    return true;
}

//
// Get a value based on the data type
//
bool InstructionDecoder::decode_by_data_type(uint8_t from_offset, uint8_t bytes_available, DataType data_type, uint32_t &value, uint64_t &value1, uint64_t &value2, uint8_t &bytes_read)
{
    switch (data_type)
    {
        case DataType::BYTE:
        {
            uint8_t v;
            if (!decode_byte(from_offset, bytes_available, v, bytes_read))
            {
                return false;
            }
            value = static_cast<uint32_t>(v);
            value1 = static_cast<uint64_t>(v);
            value2 = 0;
            break;
        }
        case DataType::WORD:
        {
            uint16_t v;
            if (!decode_word(from_offset, bytes_available, v, bytes_read))
            {
                return false;
            }
            value = static_cast<uint32_t>(v);
            value1 = static_cast<uint64_t>(v);
            value2 = 0;
            break;
        }
        case DataType::LONGWORD:
        case DataType::F_FLOATING:
        {
            if (!decode_longword(from_offset, bytes_available, value, bytes_read))
            {
                return false;
            }
            value1 = static_cast<uint64_t>(value);
            value2 = 0;
            break;
        }
        case DataType::QUADWORD:
        case DataType::D_FLOATING:
        case DataType::G_FLOATING:
        {
            if (!decode_quadword(from_offset, bytes_available, value1, bytes_read))
            {
                return false;
            }
            value = static_cast<uint32_t>(value1);
            value2 = 0;
            break;
        }
        case DataType::OCTAWORD:
        case DataType::H_FLOATING:
        {
            if (!decode_octaword(from_offset, bytes_available, value1, value2, bytes_read))
            {
                return false;
            }
            value = static_cast<uint32_t>(value1);
            break;
        }
        default:
            return false;
    }

    return true;
}
