#ifndef MICROCODEROM_H
#define MICROCODEROM_H

#include <vector>
#include <cstdint>
#include <stdexcept>

#include "component.h"


static constexpr uint32_t MICROCODE_SIZE = 3072; // Memory size in words


//
// Class representing a microcode ROM, inheriting from Component
//

class MicrocodeRom : public Component
{
private:
    std::vector<uint64_t> memory; // Microcode store
    
public:
    uint32_t addr;      // Address to access (in)
    bool     rd_req;    // Read request signal (in)
    uint64_t data_out;  // Data read from ROM (out)

    MicrocodeRom();
    virtual ~MicrocodeRom();

    // Read and write memory to a file
    void read_microcode(const std::string &filename);
    void write_memory(const std::string &filename);

    // Method to update inputs
    void update_inputs() override;

    // Method to execute memory operations
    void execute() override;

    // Update the output ports
    void update_outputs() override;
};

#endif // MICROCODEROM_H
