#ifndef MEMORY_H
#define MEMORY_H

#include <vector>
#include <cstdint>
#include <stdexcept>

#include "component.h"


static constexpr uint32_t MEMORY_SIZE = 65536; // Memory size in words


//
// Class representing RAM memory, inheriting from Component
//

class Memory : public Component
{
private:
    std::vector<uint32_t> memory; // Memory array

public:
    uint32_t addr;      // Address to access (in)
    uint32_t data_in;   // Data to write to memory (in)
    bool     wr_req;    // Write request signal (in)
    bool     rd_req;    // Read request signal (in)
    uint32_t data_out;  // Data read from memory (out)

    Memory();
    virtual ~Memory();

    // Read and write memory to a file
    void read_memory(const std::string &filename);
    void write_memory(const std::string &filename);

    // Method to update inputs
    void update_inputs() override;

    // Method to execute memory operations
    void execute() override;

    // Update the output ports
    void update_outputs() override;
};

#endif // MEMORY_H
