#ifndef REGISTER_H
#define REGISTER_H

#include <cstdint>

#include "component.h"

//
// Class representing a single register, inheriting from Component
//

class Register : public Component
{
private:
    uint32_t value;  // Register value

public:
    uint32_t in;     // Input data (in)
    uint32_t out;    // Output data (out)
    bool     load;   // Write request signal (in)

    Register();
    virtual ~Register();

    // Method to update inputs
    void update_inputs() override;

    // Method to execute read/write operations
    void execute() override;

    // Update the output ports
    void update_outputs() override;
};

#endif // REGISTER_H
