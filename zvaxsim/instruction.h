#ifndef INSTRUCTION_H
#define INSTRUCTION_H

#include <cstdint>
#include <vector>
#include <string>


//
// All VAX-11/780 opcodes
//

enum class Opcode
{
    HALT         = 0x00,
    NOP          = 0x01,
    REI          = 0x02,
    BPT          = 0x03,
    RET          = 0x04,
    RSB          = 0x05,
    LDPCTX       = 0x06,
    SVPCTX       = 0x07,
    CVTPS        = 0x08,
    CVTSP        = 0x09,
    INDEX        = 0x0a,
    CRC          = 0x0b,
    PROBER       = 0x0c,
    PROBEW       = 0x0d,
    INSQUE       = 0x0e,
    REMQUE       = 0x0f,
    BSBB         = 0x10,
    BRB          = 0x11,
    BNEQ         = 0x12,
    BEQL         = 0x13,
    BGTR         = 0x14,
    BLEQ         = 0x15,
    JSB          = 0x16,
    JMP          = 0x17,
    BGEQ         = 0x18,
    BLSS         = 0x19,
    BGTRU        = 0x1a,
    BLEQU        = 0x1b,
    BVC          = 0x1c,
    BVS          = 0x1d,
    BCC          = 0x1e,
    BCS          = 0x1f,
    ADDP4        = 0x20,
    ADDP6        = 0x21,
    SUBP4        = 0x22,
    SUBP6        = 0x23,
    CVTPT        = 0x24,
    MULP         = 0x25,
    CVTTP        = 0x26,
    DIVP         = 0x27,
    MOVC3        = 0x28,
    CMPC3        = 0x29,
    SCANC        = 0x2a,
    SPANC        = 0x2b,
    MOVC5        = 0x2c,
    CMPC5        = 0x2d,
    MOVTC        = 0x2e,
    MOVTUC       = 0x2f,
    BSBW         = 0x30,
    BRW          = 0x31,
    CVTWL        = 0x32,
    CVTWB        = 0x33,
    MOVP         = 0x34,
    CMPP3        = 0x35,
    CVTPL        = 0x36,
    CMPP4        = 0x37,
    EDITPC       = 0x38,
    MATCHC       = 0x39,
    LOCC         = 0x3a,
    SKPC         = 0x3b,
    MOVZWL       = 0x3c,
    ACBW         = 0x3d,
    MOVAW        = 0x3e,
    PUSHAW       = 0x3f,
    ADDF2        = 0x40,
    ADDF3        = 0x41,
    SUBF2        = 0x42,
    SUBF3        = 0x43,
    MULF2        = 0x44,
    MULF3        = 0x45,
    DIVF2        = 0x46,
    DIVF3        = 0x47,
    CVTFB        = 0x48,
    CVTFW        = 0x49,
    CVTFL        = 0x4a,
    CVTRFL       = 0x4b,
    CVTBF        = 0x4c,
    CVTWF        = 0x4d,
    CVTLF        = 0x4e,
    ACBF         = 0x4f,
    MOVF         = 0x50,
    CMPF         = 0x51,
    MNEGF        = 0x52,
    TSTF         = 0x53,
    EMODF        = 0x54,
    POLYF        = 0x55,
    CVTFD        = 0x56,
    RESERVED_57  = 0x57,
    ADAWI        = 0x58,
    RESERVED_59  = 0x59,
    RESERVED_5A  = 0x5a,
    RESERVED_5B  = 0x5b,
    INSQHI       = 0x5c,
    INSQTI       = 0x5d,
    REMQHI       = 0x5e,
    REMQTI       = 0x5f,
    ADDD2        = 0x60,
    ADDD3        = 0x61,
    SUBD2        = 0x62,
    SUBD3        = 0x63,
    MULD2        = 0x64,
    MULD3        = 0x65,
    DIVD2        = 0x66,
    DIVD3        = 0x67,
    CVTDB        = 0x68,
    CVTDW        = 0x69,
    CVTDL        = 0x6a,
    CVTRDL       = 0x6b,
    CVTBD        = 0x6c,
    CVTWD        = 0x6d,
    CVTLD        = 0x6e,
    ACBD         = 0x6f,
    MOVD         = 0x70,
    CMPD         = 0x71,
    MNEGD        = 0x72,
    TSTD         = 0x73,
    EMODD        = 0x74,
    POLYD        = 0x75,
    CVTDF        = 0x76,
    RESERVED_77  = 0x77,
    ASHL         = 0x78,
    ASHQ         = 0x79,
    EMUL         = 0x7a,
    EDIV         = 0x7b,
    CLRQ         = 0x7c,
    MOVQ         = 0x7d,
    MOVAQ        = 0x7e,
    PUSHAQ       = 0x7f,
    ADDB2        = 0x80,
    ADDB3        = 0x81,
    SUBB2        = 0x82,
    SUBB3        = 0x83,
    MULB2        = 0x84,
    MULB3        = 0x85,
    DIVB2        = 0x86,
    DIVB3        = 0x87,
    BISB2        = 0x88,
    BISB3        = 0x89,
    BICB2        = 0x8a,
    BICB3        = 0x8b,
    XORB2        = 0x8c,
    XORB3        = 0x8d,
    MNEGB        = 0x8e,
    CASEB        = 0x8f,
    MOVB         = 0x90,
    CMPB         = 0x91,
    MCOMB        = 0x92,
    BITB         = 0x93,
    CLRB         = 0x94,
    TSTB         = 0x95,
    INCB         = 0x96,
    DECB         = 0x97,
    CVTBL        = 0x98,
    CVTBW        = 0x99,
    MOVZBL       = 0x9a,
    MOVZBW       = 0x9b,
    ROTL         = 0x9c,
    ACBB         = 0x9d,
    MOVAB        = 0x9e,
    PUSHAB       = 0x9f,
    ADDW2        = 0xa0,
    ADDW3        = 0xa1,
    SUBW2        = 0xa2,
    SUBW3        = 0xa3,
    MULW2        = 0xa4,
    MULW3        = 0xa5,
    DIVW2        = 0xa6,
    DIVW3        = 0xa7,
    BISW2        = 0xa8,
    BISW3        = 0xa9,
    BICW2        = 0xaa,
    BICW3        = 0xab,
    XORW2        = 0xac,
    XORW3        = 0xad,
    MNEGW        = 0xae,
    CASEW        = 0xaf,
    MOVW         = 0xb0,
    CMPW         = 0xb1,
    MCOMW        = 0xb2,
    BITW         = 0xb3,
    CLRW         = 0xb4,
    TSTW         = 0xb5,
    INCW         = 0xb6,
    DECW         = 0xb7,
    BISPSW       = 0xb8,
    BICPSW       = 0xb9,
    POPR         = 0xba,
    PUSHR        = 0xbb,
    CHMK         = 0xbc,
    CHME         = 0xbd,
    CHMS         = 0xbe,
    CHMU         = 0xbf,
    ADDL2        = 0xc0,
    ADDL3        = 0xc1,
    SUBL2        = 0xc2,
    SUBL3        = 0xc3,
    MULL2        = 0xc4,
    MULL3        = 0xc5,
    DIVL2        = 0xc6,
    DIVL3        = 0xc7,
    BISL2        = 0xc8,
    BISL3        = 0xc9,
    BICL2        = 0xca,
    BICL3        = 0xcb,
    XORL2        = 0xcc,
    XORL3        = 0xcd,
    MNEGL        = 0xce,
    CASEL        = 0xcf,
    MOVL         = 0xd0,
    CMPL         = 0xd1,
    MCOML        = 0xd2,
    BITL         = 0xd3,
    CLRL         = 0xd4,
    TSTL         = 0xd5,
    INCL         = 0xd6,
    DECL         = 0xd7,
    ADWC         = 0xd8,
    SBWC         = 0xd9,
    MTPR         = 0xda,
    MFPR         = 0xdb,
    MOVPSL       = 0xdc,
    PUSHL        = 0xdd,
    MOVAL        = 0xde,
    PUSHAL       = 0xdf,
    BBS          = 0xe0,
    BBC          = 0xe1,
    BBSS         = 0xe2,
    BBCS         = 0xe3,
    BBSC         = 0xe4,
    BBCC         = 0xe5,
    BBSSI        = 0xe6,
    BBCCI        = 0xe7,
    BLBS         = 0xe8,
    BLBC         = 0xe9,
    FFS          = 0xea,
    FFC          = 0xeb,
    CMPV         = 0xec,
    CMPZV        = 0xed,
    EXTV         = 0xee,
    EXTZV        = 0xef,
    INSV         = 0xf0,
    ACBL         = 0xf1,
    AOBLSS       = 0xf2,
    AOBLEQ       = 0xf3,
    SOBGEQ       = 0xf4,
    SOBGTR       = 0xf5,
    CVTLB        = 0xf6,
    CVTLW        = 0xf7,
    ASHP         = 0xf8,
    CVTLP        = 0xf9,
    CALLG        = 0xfa,
    CALLS        = 0xfb,
    XFC          = 0xfc,
    EXTENDED     = 0xfd,
    RESERVED_FE  = 0xfe,
    RESERVED_FF  = 0xff,
    MFVP         = 0x131,   // All opcodes from here on are mapped from the extended set starting with 0xfd
    CVTDH        = 0x132,
    CVTGF        = 0x133,
    VLDL         = 0x134,
    VGATHL       = 0x135,
    VLDQ         = 0x136,
    VGATHQ       = 0x137,
    ADDG2        = 0x140,
    ADDG3        = 0x141,
    SUBG2        = 0x142,
    SUBG3        = 0x143,
    MULG2        = 0x144,
    MULG3        = 0x145,
    DIVG2        = 0x146,
    DIVG3        = 0x147,
    CVTGB        = 0x148,
    CVTGW        = 0x149,
    CVTGL        = 0x14a,
    CVTRGL       = 0x14b,
    CVTBG        = 0x14c,
    CVTWG        = 0x14d,
    CVTLG        = 0x14e,
    ACBG         = 0x14f,
    MOVG         = 0x150,
    CMPG         = 0x151,
    MNEGG        = 0x152,
    TSTG         = 0x153,
    EMODG        = 0x154,
    POLYG        = 0x155,
    CVTGH        = 0x156,
    ADDH2        = 0x160,
    ADDH3        = 0x161,
    SUBH2        = 0x162,
    SUBH3        = 0x163,
    MULH2        = 0x164,
    MULH3        = 0x165,
    DIVH2        = 0x166,
    DIVH3        = 0x167,
    CVTHB        = 0x168,
    CVTHW        = 0x169,
    CVTHL        = 0x16a,
    CVTRHL       = 0x16b,
    CVTBH        = 0x16c,
    CVTWH        = 0x16d,
    CVTLH        = 0x16e,
    ACBH         = 0x16f,
    MOVH         = 0x170,
    CMPH         = 0x171,
    MNEGH        = 0x172,
    TSTH         = 0x173,
    EMODH        = 0x174,
    POLYH        = 0x175,
    CVTHG        = 0x176,
    CLRH         = 0x17c,
    MOVO         = 0x17d,
    MOVAO        = 0x17e,
    PUSHAO       = 0x17f,
    VVADDL       = 0x180,
    VSADDL       = 0x181,
    VVADDG       = 0x182,
    VSADDG       = 0x183,
    VVADDF       = 0x184,
    VSADDF       = 0x185,
    VVADDD       = 0x186,
    VSADDD       = 0x187,
    VVSUBL       = 0x188,
    VSSUBL       = 0x189,
    VVSUBG       = 0x18a,
    VSSUBG       = 0x18b,
    VVSUBF       = 0x18c,
    VSSUBF       = 0x18d,
    VVSUBD       = 0x18e,
    VSSUBD       = 0x18f,
    VVMULL       = 0x1a0,
    VSMULL       = 0x1a1,
    VVMULG       = 0x1a2,
    VSMULG       = 0x1a3,
    VVMULF       = 0x1a4,
    VSMULF       = 0x1a5,
    VVMULD       = 0x1a6,
    VSMULD       = 0x1a7,
    VSYNC        = 0x1a8,
    MTVP         = 0x1a9,
    VVDIVG       = 0x1aa,
    VSDIVG       = 0x1ab,
    VVDIVF       = 0x1ac,
    VSDIVF       = 0x1ad,
    VVDIVD       = 0x1ae,
    VSDIVD       = 0x1af,
    VVCMPL       = 0x1c0,
    VSCMPL       = 0x1c1,
    VVCMPG       = 0x1c2,
    VSCMPG       = 0x1c3,
    VVCMPF       = 0x1c4,
    VSCMPF       = 0x1c5,
    VVCMPD       = 0x1c6,
    VSCMPD       = 0x1c7,
    VVBISL       = 0x1c8,
    VSBISL       = 0x1c9,
    VVBICL       = 0x1cc,
    VSBICL       = 0x1cd,
    VVSRLL       = 0x1e0,
    VSSRLL       = 0x1e1,
    VVSLLL       = 0x1e4,
    VSSLLL       = 0x1e5,
    VVXORL       = 0x1e8,
    VSXORL       = 0x1e9,
    VVCVT        = 0x1ec,
    IOTA         = 0x1ed,
    VVMERGE      = 0x1ee,
    VSMERGE      = 0x1ef,
    CVTHF        = 0x1f6,
    CVTHD        = 0x1f7
};

//
// How many bytes the instruction operates on
//
enum class DataType
{
    BYTE,
    WORD,
    LONGWORD,
    QUADWORD,
    OCTAWORD,
    F_FLOATING,
    D_FLOATING,
    G_FLOATING,
    H_FLOATING,
    N_BRANCH_LIST
};

//
// All VAX-11/780 addressing modes    
//
enum class AddressingMode
{
    LITERAL                    = 0x00,
    BRANCH_DISPLACEMENT        = 0x01,
    INDEXED                    = 0x04,
    REGISTER                   = 0x05,
    REGISTER_DEFERRED          = 0x06,
    AUTODECREMENT              = 0x07,
    AUTOINCREMENT              = 0x08,
    AUTOINCREMENT_DEFERRED     = 0x09,
    BYTE_DISP                  = 0x0a,
    BYTE_DISP_DEFERRED         = 0x0b,
    WORD_DISP                  = 0x0c,
    WORD_DISP_DEFERRED         = 0x0d,
    LONGWORD_DISP              = 0x0e,
    LONGWORD_DISP_DEFERRED     = 0x0f,
    IMMEDIATE                  = 0x18,
    ABSOLUTE                   = 0x19,
    BYTE_RELATIVE              = 0x1a,
    BYTE_RELATIVE_DEFERRED     = 0x1b,
    WORD_RELATIVE              = 0x1c,
    WORD_RELATIVE_DEFERRED     = 0x1d,
    LONGWORD_RELATIVE          = 0x1e,
    LONGWORD_RELATIVE_DEFERRED = 0x1f
};

//
// How an operand is accessed
//
enum class Access
{
    READ,                   // We run the microcode to read this operand before executing the instruction
    WRITE,                  // We run the microcode to get the effective address for this operand before executing the instruction, then write it back after
    MODIFY,                 // We run the microcode to read this operand before executing the instruction, then write it back after
    BRANCH,                 // This operand is a branch displacement
    ADDRESS,                // We run the microcode to get the effective address for this operand before executing the instruction
    V_ADDRESS,              // Like an address but can also refer to a register
    CUSTOM                  // The instruction uses this operand in a custom way, let the instruction microcode handle it
};

//
// Defines what kinds of operands an instruction takes
//
class Operand
{
public:
    DataType data_type;  // How many bytes of extra data are read from the operand
    Access   access;     // How the operand is accessed
};

// Get the size from the data type
uint32_t data_size_from_type(DataType data_type) const;


//
// Class defining an instruction's opcode, data size, and operands
//
class Instruction
{
public:
    static constexpr uint32_t OPERANDS_MAX = 6;

public:
    Instruction() : opcode(Opcode::HALT), mnemonic("HALT"), data_type(DataType::LONGWORD), is_valid(false) {}
    Instruction(Opcode opcode, const std::string &mnemonic, DataType data_type, const std::vector<Operand> &operands) : opcode(opcode), mnemonic(mnemonic), data_type(data_type), operands(operands), is_valid(true) {}
    
    Opcode               opcode;
    std::string          mnemonic;
    DataType             data_type;
    std::vector<Operand> operands;
    bool                 is_valid;
};

//
// Class defining the entire VAX-11/780 instruction set
//
class InstructionSet
{
private:
    static constexpr uint32_t INSTRUCTION_COUNT = 512;
    static constexpr const char *ISA_SPEC_FILENAME = "../zvaxsim/isa_spec.txt";

public:
    InstructionSet();

    // Get the instruction for a given opcode
    const Instruction &get_instruction(Opcode opcode) const { return instruction_set[static_cast<uint32_t>(opcode)]; }

    static bool is_valid_opcode(uint16_t opcode) { return opcode < instruction_set.size() && instruction_set.at(opcode); }

private:
    // Helper function to map access method character to Access enum
    Access map_access(char accessMethod, bool &success);

    // Helper function to map data type character to DataType enum
    DataType map_data_type(char dataType, bool &success);

    // Read the instruction set from a file
    void read_instruction_set(const char *filename);

private:
    // A table of all VAX-11/780 instructions
    std::vector<Instruction> instruction_set;
};

#endif // INSTRUCTION_H
