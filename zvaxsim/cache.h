#ifndef CACHE_H
#define CACHE_H

#include <vector>
#include <array>
#include <cstdint>
#include <stdexcept>

#include "component.h"


//
// Constants
//

static constexpr uint32_t CACHE_LINES           = 1024; // Number of cache lines in the cache
static constexpr uint32_t CACHE_SET_ASSOCIATIVE = 2;    // Two way set associative cache
static constexpr uint32_t CACHE_LINE_SIZE       = 4;    // Cache line size in words


//
// Class representing a cache line
//

class CacheLine
{
public:
    std::array<uint32_t, CACHE_LINE_SIZE> data; // Data stored in cache line
    bool     recently_used; // Recently used bit for cache line
    bool     valid;         // Valid bit for cache line
    bool     dirty;         // Dirty bit for cache line

    CacheLine();
};


//
// Class representing a data cache component, inheriting from Component
//

class Cache : public Component
{
private:
    typedef std::array<CacheLine, CACHE_SET_ASSOCIATIVE> CacheSet;
    typedef std::vector<CacheSet> CacheArray;

    CacheArray cache; // Cache array

public:
    uint32_t instr_addr;   // Address to access instructions in cache (in)
    bool     instr_rd_req; // Read request signal for instruction cache (in)
    uint32_t instr_out;    // Data read from cache (out)
    bool     instr_rd_ok;  // Read operation successful signal for instruction cache (out)
    uint32_t data_addr;    // Address to access data in cache (in)
    bool     data_wr_req;  // Write request signal
    uint32_t data_in;      // Data to write to cache (in)
    bool     data_rd_req;  // Read request signal
    uint32_t data_out;     // Data read from cache (out)
    bool     data_rd_ok;   // Read operation successful signal for data cache (out)

    Cache();
    virtual ~Cache();

    // Method to update cache inputs
    void update_inputs() override;

    // Method to execute cache operation
    void execute() override;

    // Update the output ports
    void update_outputs() override;
};

#endif // CACHE_H
