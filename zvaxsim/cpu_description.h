#ifndef CPUDESCRIPTION_H
#define CPUDESCRIPTION_H

#include <string>
#include <vector>
#include <fstream>
#include <memory>
#include <cstdint>
#include <nlohmann/json.hpp>
#include "component.h"
#include "connection.h"

// Main class for CPU description with dynamic update capabilities
class CpuDescription {
public:
    // Constructor
    CpuDescription();

    // Methods to load from and save to a file
    bool loadFromFile(const std::string &fileName);
    bool saveToFile(const std::string &fileName) const;

    // Methods to add, remove, and update components
    void addComponent(const Component &component);
    bool removeComponent(const std::string &id);
    bool updateComponent(const std::string &id, const Component &component);

    // Methods to add, remove, and update connections
    uint32_t addConnection(const Connection &connection);
    bool removeConnection(uint32_t id);
    bool updateConnection(uint32_t id, const Connection &connection);

    // List of components in the CPU
    std::vector<std::shared_ptr<Component>> components;

    // List of connections in the CPU
    std::vector<std::shared_ptr<Connection>> connections;

private:
    // Helper methods to read from and write to files
    bool readFile(const std::string &fileName, std::string &content) const;
    bool writeFile(const std::string &fileName, const std::string &content) const;
};

#endif // CPUDESCRIPTION_H
