#ifndef MICROPROGRAMCOUNTER_H
#define MICROPROGRAMCOUNTER_H

#include <cstdint>
#include <stdexcept>

#include "component.h"

//
// Class representing a microprogram counter, inheriting from Component
//

class MicroProgramCounter : public Component
{
public:
    uint32_t upc_set_in;   // Microprogram address input to set (in)
    bool     set_req;      // Set upc request signal (in)
    uint32_t upc;          // Microprogram counter value (out)

    MicroProgramCounter();
    virtual ~MicroProgramCounter();

    // Method to update inputs
    void update_inputs() override;

    // Method to execute MicroProgramCounter operation
    void execute() override;

    // Update the output ports
    void update_outputs() override;
};

#endif // MICROPROGRAMCOUNTER_H
