#ifndef PORT_H
#define PORT_H

#include <string>
#include <utility>
#include <nlohmann/json.hpp>

// Forward declaration of the Component class
class Component;

// Class representing a port with its attributes and JSON serialization support
class Port 
{
public:
    // The port direction
    enum class Direction
    {
        INPUT,
        OUTPUT
    };

    // The orientation
    enum class Orientation
    {
        UP,
        DOWN,
        LEFT,
        RIGHT
    };

public:
    std::string         id;          // Identifier for the port
    Direction           direction;   // Data direction of the port: INPUT or OUTPUT
    int                 bits;        // Number of bits for the port
    uint32_t            value;       // Value of the port, if it's an output port
    std::pair<int, int> location;    // Location on the screen of the port relative to its parent component
    Orientation         orientation; // Orientation of the port: UP, DOWN, LEFT, or RIGHT

    std::shared_ptr<Component> parent;   // Pointer to the parent component

    // Constructor
    Port() : id(""), direction(Direction::INPUT), bits(0), value(0), location({0.0f, 0.0f}), orientation(Orientation::RIGHT) {}

    // Set the parent component of the port
    void set_parent(std::shared_ptr<Component> parent) { this->parent = parent; }

    // Get the port's id path, which is <component_id>.<port_id>
    std::string get_id_path() const;

    // Macro to define how this class is serialized/deserialized using nlohmann::json
    NLOHMANN_DEFINE_TYPE_INTRUSIVE(Port, id, direction, bits, location, orientation)
};

// Custom serialization function for the Direction enum
void to_json(nlohmann::json& j, const Port::Direction& direction);
void from_json(const nlohmann::json& j, Port::Direction& direction);

// Custom serialization function for the Orientation enum
void to_json(nlohmann::json& j, const Port::Orientation& orientation);
void from_json(const nlohmann::json& j, Port::Orientation& orientation);

#endif // PORT_H
