#include "simulator.h"

// Constructor
Simulator::Simulator() {}

// Destructor
Simulator::~Simulator() {}

// Initialise the simulator
void Simulator::init()
{
    // Create the instruction set
    instruction_set = std::make_shared<InstructionSet>();

#if 0
    // Initialise all the components
    for (auto& [id, component] : components)
    {
        component->init(*this);
    }
#endif
}

// Method to add a component to the simulator
void Simulator::add_component(std::shared_ptr<Component> component)
{
    components[component->id] = component;
}

// Method to add a connection to the simulator
void Simulator::add_connection(std::shared_ptr<Connection> connection)
{
    connections[connection->id] = connection;
}

// Method to add a port to the simulator
void Simulator::add_port(std::shared_ptr<Port> port)
{
    ports[port->id] = port;
}

// Method to step the simulation by one cycle
void Simulator::step()
{
    update_inputs();
    execute();
    update_outputs();
}

// Method to update the inputs of all components
void Simulator::update_inputs()
{
    // Go through the connections and copy the muxed output values into the destination input ports
    for (auto& connection : connections)
    {
        connection.second->update(*this);
    }

    // Update all the components with the new input values
    for (auto& [id, component] : components)
    {
        component->update_inputs();
    }
}

// Method to execute all components
void Simulator::execute()
{
    for (auto& [id, component] : components)
    {
        component->execute();
    }
}

// Method to update the outputs of all components
void Simulator::update_outputs()
{
    for (auto& [id, component] : components)
    {
        component->update_outputs();
    }
}
