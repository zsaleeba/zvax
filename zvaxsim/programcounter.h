#ifndef PROGRAM_COUNTER_H
#define PROGRAM_COUNTER_H

#include <vector>
#include <cstdint>
#include <stdexcept>

#include "component.h"

//
// Class representing a Program Counter component, inheriting from Component
//

class ProgramCounter : public Component
{
public:
    uint32_t addr_in;   // Address input
    bool load;          // Load signal
    bool step;          // Step signal
    uint32_t step_size; // Step size
    uint32_t pc;        // Program counter value

    ProgramCounter();
    virtual ~ProgramCounter();

    // Method to update Program Counter inputs
    void update_inputs() override;

    // Method to execute Program Counter operation
    void execute() override;

    // Update the output ports
    void update_outputs() override;
};

#endif // PROGRAM_COUNTER_H
