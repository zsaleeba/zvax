#ifndef CONNECTION_H
#define CONNECTION_H

#include <string>
#include <vector>
#include <utility>
#include <cstdint>
#include <nlohmann/json.hpp>


// Forward declaration of the Simulator class
class Simulator;


//
// Class representing a path segment for connections with JSON serialization support
//

class PathSegment 
{
public:
    enum class Direction 
    {
        HORIZONTAL,
        VERTICAL
    };

public:
    Direction direction;        // Direction of the segment: "horizontal" or "vertical"
    std::pair<int, int> start;  // Starting point of the segment
    std::pair<int, int> end;    // Width and height of the segment
    bool      start_arrow;      // Flag for arrowhead at the start of the segment
    bool      end_arrow;        // Flag for arrowhead at the end of the segment
    int       mux;              // Mux value for the segment. -1 if it doesn't relate to a specific input

    // Macro to define how this class is serialized/deserialized using nlohmann::json
    NLOHMANN_DEFINE_TYPE_INTRUSIVE(PathSegment, direction, start, end, start_arrow, end_arrow, mux)
};

// Custom serialization function for the Direction enum
void to_json(nlohmann::json& j, const PathSegment::Direction& direction);
void from_json(const nlohmann::json& j, PathSegment::Direction& direction);


//
// Class representing a connection between ports with JSON serialization support
//

class Connection 
{
public:
    // A unique id for the connection
    uint32_t                 id;

    // Definition of the connection
    std::vector<std::pair<std::string, int>> inputs;    // List of input port ids and their corresponding mux values
    std::vector<std::string> outputs;                   // List of output ports
    std::string              mux_input;                 // ID of the mux input
    std::vector<PathSegment> path;                      // Drawing specification for the connection

    // Constructor
    Connection();

    // Update the output ports of the connection based on the input ports and the mux
    void update(Simulator &simulator);

    // Macro to define how this class is serialized/deserialized using nlohmann::json
    NLOHMANN_DEFINE_TYPE_INTRUSIVE(Connection, inputs, outputs, mux_input, path)

private:
    // The next id to allocate
    static uint32_t next_id;

private:
    // Allocate a unique id
    static uint32_t getNextId();
};

// Custom serialization function for std::shared_ptr<Connection>
void to_json(nlohmann::json& j, const std::shared_ptr<Connection>& connection);
void from_json(const nlohmann::json& j, std::shared_ptr<Connection>& connection);

#endif // CONNECTION_H
