#include "instructionbuffer.h"
#include <iostream>

//
// InstructionBuffer constructor
//
InstructionBuffer::InstructionBuffer() : Component(), addr(0), buf(0), full(0), step_size(0), do_step(false), clear(false),
                                         instr_cache_addr(0), instr_cache_rd_req(false), instr_cache_out(0), instr_cache_rd_ok(false) 
{
    type = Component::Type::INSTRUCTION_BUFFER;
}

//
// InstructionBuffer destructor
//
InstructionBuffer::~InstructionBuffer() {}

//
// Method to update instruction buffer inputs
//
void InstructionBuffer::update_inputs() 
{
    // Go through the input ports and update the corresponding attributes
    for (const auto& port : input_ports)
    {
        if (port.id == "addr")
        {
            addr = port.value;
        } 
        else if (port.id == "step_size")
        {
            step_size = port.value;
        } 
        else if (port.id == "do_step")
        {
            do_step = port.value;
        } 
        else if (port.id == "clear")
        {
            clear = port.value;
        } 
        else if (port.id == "instr_cache_out")
        {
            instr_cache_out = port.value;
        } 
        else if (port.id == "instr_cache_rd_ok")
        {
            instr_cache_rd_ok = port.value;
        }
    }
}

//
// Method to execute instruction buffer operation
//
void InstructionBuffer::execute() 
{
    if (clear) 
    {
        // Clear the buffer and start reloading from addr
        buf = 0;
        full = 0;
        std::cout << "Instruction buffer cleared, reloading from addr=" << addr << std::endl;
    } 
    else if (do_step) 
    {
        // Step operation: remove step_size bytes from the LSB of the buffer
        buf >>= (step_size * 8); // Shift right by step_size bytes
        full -= step_size;
        std::cout << "Instruction buffer stepped, step_size=" << step_size << ", new buffer=" << buf << std::endl;
    }

    // Fill the buffer from cache if it is not more than five bytes full
    if (full <= 5 && !instr_cache_rd_req) 
    {
        instr_cache_addr = addr + full; // Set the cache address to read the next instruction
        instr_cache_rd_req = true;
        std::cout << "Requesting instruction from cache, addr=" << instr_cache_addr << std::endl;
    }

    // Check if the read from the cache was successful
    if (instr_cache_rd_ok && instr_cache_rd_req) 
    {
        instr_cache_rd_req = false; // Reset the read request
        // Add the new instruction to the MSB of the buffer
        buf |= (static_cast<uint64_t>(instr_cache_out) << (full * 8));
        full += 4; // Assume each instruction is 4 bytes
        std::cout << "New instruction loaded from cache, addr=" << instr_cache_addr << ", new buffer=" << buf << std::endl;
    }
}

// Method to update the output ports
void InstructionBuffer::update_outputs() 
{
    // Update the output ports with the buffer state
    for (auto& port : output_ports) 
    {
        if (port.id == "buf") 
        {
            port.value = buf;
        } 
        else if (port.id == "full") 
        {
            port.value = full;
        }
    }
}