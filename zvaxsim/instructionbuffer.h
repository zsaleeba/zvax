#ifndef INSTRUCTIONBUFFER_H
#define INSTRUCTIONBUFFER_H

#include <vector>
#include <cstdint>
#include <stdexcept>

#include "component.h"
#include "instruction.h"


//
// Class representing an instruction buffer operand
//
class InstructionBufferOperand
{
public:
    AddressingMode addr_mode;
    uint8_t                   reg_id;
    uint32_t                  value;
    uint8_t                   value_size;      // The number of bytes in the value

    InstructionBufferOperand(AddressingMode addr_mode, uint8_t reg_id, uint32_t value, uint8_t value_size) : addr_mode(addr_mode), reg_id(reg_id), value(value), value_size(value_size) {}
    InstructionBufferOperand() : addr_mode(AddressingMode::LITERAL), reg_id(0), value(0), value_size(0) {}
};

//
// Class representing an instruction buffer instruction
//
class InstructionBufferInstruction
{
public:
    uint16_t                 opcode;
    uint8_t                  operand_count;
    InstructionBufferOperand operands[Instruction::OPERANDS_MAX];
    uint8_t                  size; // The size of the instruction in bytes
};

//
// Class representing an instruction buffer component, inheriting from Component
// The 
//

class InstructionBuffer : public Component
{
public:
    // The number of bytes in each page of the instruction buffer
    static constexpr uint32_t PAGE_SIZE_BITS = 4;
    static constexpr uint32_t PAGE_SIZE = 1 << PAGE_SIZE_BITS;
    static constexpr uint32_t PAGE_COUNT_BITS = 2;
    static constexpr uint32_t PAGE_COUNT = 1 << PAGE_COUNT_BITS;

    // A bit mask to extract the byte within a page from an address
    static constexpr uint32_t PAGE_MASK = PAGE_SIZE - 1;

    // A bit mask to extract the page number from an address
    static constexpr uint32_t PAGE_NUMBER_MASK = ~PAGE_MASK & BUFFER_MASK;

    // The total size of the instruction buffer in bytes
    static constexpr uint32_t BUFFER_SIZE = PAGE_SIZE * PAGE_COUNT;

    // A bit mask to extract the byte within the buffer from an address
    static constexpr uint32_t BUFFER_MASK = BUFFER_SIZE - 1;

public:
    uint32_t buf_start_addr;               // The start address of the buffer (out)
    uint8_t  buf[BUFFER_SIZE];             // Instruction buffer (out)
    bool     page_available[PAGE_COUNT];   // Whether the page is available (out)
    uint32_t load_addr;                    // Address to access (in)
    bool     do_load;                      // Set the buffer address to addr (in)

    uint32_t instr_cache_addr;             // Address to access instructions in cache (out)
    bool     instr_cache_rd_req;           // Read request signal for instruction cache (out)
    uint32_t instr_cache_out;              // Data read from cache (in)
    bool     instr_cache_rd_ok;            // Read operation successful signal for instruction cache (in)

public:
    InstructionBuffer();
    virtual ~InstructionBuffer();

    // Method to update instruction buffer inputs
    void update_inputs() override;

    // Method to execute instruction buffer operation
    void execute() override;

    // Update the output ports
    void update_outputs() override;
};

#endif // INSTRUCTIONBUFFER_H
