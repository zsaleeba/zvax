#include <fstream>
#include <sstream>
#include <iostream>
#include <algorithm>

#include "instruction.h"

// Constructor
InstructionSet::InstructionSet()
{
    // Initialize the instruction set with empty instructions
    instruction_set.resize(INSTRUCTION_COUNT);
    read_instruction_set(ISA_SPEC_FILENAME);
}

// Helper function to map access method character to Access enum
Access InstructionSet::map_access(char accessMethod, bool &success)
{
    success = true;
    switch (accessMethod)
    {
        case 'r': return Access::READ;
        case 'w': return Access::WRITE;
        case 'm': return Access::MODIFY;
        case 'a': return Access::ADDRESS;
        case 'b': return Access::BRANCH;
        case 'v': return Access::V_ADDRESS;
        default:  {
            success = false;
            return Access::READ;
        }
    }
}

// Helper function to map data type character to DataType enum
DataType InstructionSet::map_data_type(char dataType, bool &success)
{
    success = true;
    switch (dataType)
    {
        case 'b': return DataType::BYTE;
        case 'w': return DataType::WORD;
        case 'l': return DataType::LONGWORD;
        case 'q': return DataType::QUADWORD;
        case 'o': return DataType::OCTAWORD;
        case 'f': return DataType::F_FLOATING;
        case 'd': return DataType::D_FLOATING;
        case 'g': return DataType::G_FLOATING;
        case 'h': return DataType::H_FLOATING;
        case 'n': return DataType::N_BRANCH_LIST;
        default:  {
            success = false;
            return DataType::LONGWORD; // Default fallback
        }
    }
}

// Read the instruction set from a file
void InstructionSet::read_instruction_set(const char *filename)
{
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "Error opening file: " << filename << std::endl;
        return;
    }

    instruction_set.clear();
    instruction_set.resize(INSTRUCTION_COUNT);

    std::string line;
    while (std::getline(file, line))
    {
        std::istringstream iss(line);
        std::string opcode_str, mnemonic, alu_data_type, operand_str;

        if (!(iss >> opcode_str >> mnemonic >> alu_data_type >> std::ws))
        {
            std::cerr << "Error reading line: " << line << std::endl;
            continue;
        }

        std::getline(iss, operand_str, '\t');

        // Parse opcode
        unsigned long opcode_val = std::stoul(opcode_str, nullptr, 16);
        Opcode opcode = static_cast<Opcode>(opcode_val);

        // Create instruction
        bool success = true;
        Instruction instruction;
        instruction.opcode = opcode;
        instruction.mnemonic = mnemonic;
        instruction.data_type = map_data_type(alu_data_type[0], success);
        if (!success)
        {
            std::cerr << "bad data type on instruction " << mnemonic << ": " << alu_data_type << std::endl;
        }

        instruction.is_valid = true;

        // Parse operands
        std::vector<Operand> operands;
        std::istringstream operand_iss(operand_str);
        std::string operand;
        while (std::getline(operand_iss, operand, ','))
        {
            // Trim spaces
            operand.erase(std::remove_if(operand.begin(), operand.end(), ::isspace), operand.end());

            // Parse access and data type
            char accessMethod = operand[operand.size() - 2];
            char dataTypeChar = operand[operand.size() - 1];

            bool access_success = true;
            bool data_type_success = true;
            Access access = map_access(accessMethod, access_success);
            DataType data_type = map_data_type(dataTypeChar, data_type_success);
            if (!access_success || !data_type_success)
            {
                std::cerr << "bad access and data type on instruction " << mnemonic << ": " << operand << std::endl;
            }
            Operand operand = { data_type, access };

            instruction.operands.push_back(operand);
        }

        if (opcode_val < INSTRUCTION_COUNT)
        {
            instruction_set[opcode_val] = instruction;
        }
        else
        {
            std::cerr << "Warning: Invalid opcode in file, ignoring entry." << std::endl;
            break;
        }
    }

    file.close();
}

//
// Get the size from the data type
//
uint32_t data_size_from_type(DataType data_type) const
{
    switch (data_type)
    {
        case DataType::BYTE:          return 1;
        case DataType::WORD:          return 2;
        case DataType::LONGWORD:      return 4;
        case DataType::QUADWORD:      return 8;
        case DataType::OCTAWORD:      return 16;
        case DataType::F_FLOATING:    return 4;
        case DataType::D_FLOATING:    return 8;
        case DataType::G_FLOATING:    return 8;
        case DataType::H_FLOATING:    return 16;
        case DataType::N_BRANCH_LIST: return 4;
        default: return 0;
    }
}
