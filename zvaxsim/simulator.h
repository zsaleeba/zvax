#ifndef SIMULATOR_H
#define SIMULATOR_H

#include <cstdint>
#include <map>
#include <string>
#include <memory>

#include "component.h"
#include "connection.h"
#include "port.h"
#include "instruction.h"


//
// The main simulator class
//

class Simulator
{
private:
    std::map<std::string, std::shared_ptr<Component>>  components;      // The components
    std::map<std::string, std::shared_ptr<Port>>       ports;           // The ports. Note that ports are accessed by <component_id>.<port_name>
    std::map<uint32_t, std::shared_ptr<Connection>>    connections;     // The connections
    std::shared_ptr<InstructionSet>                    instruction_set; // The instruction set

public:
    Simulator();
    virtual ~Simulator();

    // Initialise the simulator
    void init();

    // Method to add a component to the simulator
    void add_component(std::shared_ptr<Component> component);

    // Method to add a connection to the simulator
    void add_connection(std::shared_ptr<Connection> connection);

    // Method to add a port to the simulator
    void add_port(std::shared_ptr<Port> port);

    // Accessor methods
    std::shared_ptr<Component>  get_component(const std::string& id) { return components[id]; }
    std::shared_ptr<Port>       get_port(const std::string& id) { return ports[id]; }
    std::shared_ptr<Connection> get_connection(uint32_t id) { return connections[id]; }

    // Method to step the simulation by one cycle
    void step();

    // Method to update the inputs of all components
    void update_inputs();

    // Method to execute all components
    void execute();

    // Method to update the outputs of all components
    void update_outputs();
};

#endif // SIMULATOR_H
