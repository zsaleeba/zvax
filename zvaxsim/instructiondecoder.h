#ifndef INSTRUCTIONDECODER_H
#define INSTRUCTIONDECODER_H

#include <vector>
#include <cstdint>
#include <stdexcept>

#include "component.h"
#include "instruction.h"

//
// Class representing the VAX-11/780 instruction decoder, inheriting from Component
//

class InstructionDecoder : public Component
{
public:
    // A decoded operand
    class Operand
    {
    public:
        AddressingMode addr_mode;  // Addressing mode
        uint8_t        reg;        // Register number if register addressing mode
        uint32_t       value;      // Value or effective address - up to 32 bits
        uint64_t       value1;     // Value or effective address - up to 64 bits
        uint64_t       value2;     // Value or effective address - msb for 128-bit values along with value1
        bool           is_indexed; // True if indexed addressing mode
        uint8_t        index_reg;  // Index register number if indexed addressing mode
        DataType       data_type;  // The data type of the operand
        Access         access;     // How the operand is accessed

        Operand() : addr_mode(AddressingMode::LITERAL), reg(0), value(0), value1(0), value2(0), is_indexed(false), index_reg(0), data_type(DataType::BYTE), access(Access::READ) {}
        Operand(uint8_t from_offset, uint8_t bytes_available, DataType data_type, uint32_t &value, uint64_t &value1, uint64_t &value2, uint8_t &bytes_read) : addr_mode(AddressingMode::LITERAL), reg(0), value(value), value1(value1), value2(value2), is_indexed(false), index_reg(0), data_type(data_type), access(Access::READ) {}
    };

    static constexpr uint8_t MAX_OPERANDS = 6;  // Maximum number of operands in an instruction
    
public:
    // Interface to the instruction buffer
    uint32_t buf_start_addr;               // The start address of the buffer (out)
    uint8_t  buf[BUFFER_SIZE];             // Instruction buffer (out)
    bool     page_available[PAGE_COUNT];   // Whether the page is available (out)
    uint32_t load_addr;                    // Address to access (in)
    bool     do_load;                      // Set the buffer address to addr (in)

    // Decoded instruction fields
    Operand  operand[MAX_OPERANDS]; // Decoded operands (out)
    uint8_t  num_operands;          // Number of operands in the instruction (out)
    bool     is_complete;           // True if decoding is complete (out)
    bool     is_fault;              // Decoding failed due to an error (out)

    // Instruction set
    std::shared_ptr<InstructionSet> instruction_set;

private:
    // Decoder stages
    bool decode_opcode(uint8_t from_offset, uint8_t bytes_available, uint16_t &opcode, uint8_t &bytes_read, bool &opcode_fault);
    bool decode_operand(uint8_t from_offset, uint8_t bytes_available, Access access, DataType data_type, Operand &operand, uint8_t &bytes_read);
    bool decode_case(uint32_t base, uint32_t limit, uint8_t &bytes_read);

    // Read bytes from the buffer
    bool decode_byte(uint8_t from_offset, uint8_t bytes_available, uint8_t &value, uint8_t &bytes_read);
    bool decode_word(uint8_t from_offset, uint8_t bytes_available, uint16_t &value, uint8_t &bytes_read);
    bool decode_longword(uint8_t from_offset, uint8_t bytes_available, uint32_t &value, uint8_t &bytes_read);
    bool decode_quadword(uint8_t from_offset, uint8_t bytes_available, uint64_t &value, uint8_t &bytes_read);
    bool decode_octaword(uint8_t from_offset, uint8_t bytes_available, uint64_t &value1, uint64_t &value2, uint8_t &bytes_read);
    bool decode_by_data_type(uint8_t from_offset, uint8_t bytes_available, DataType data_type, uint32_t &value, uint64_t &value1, uint64_t &value2, uint8_t &bytes_read);

public:
    InstructionDecoder();
    virtual ~InstructionDecoder();

    // Method to update inputs
    void update_inputs() override;

    // Method to execute decode operation
    void execute() override;

    // Update the output ports
    void update_outputs() override;
};

#endif // INSTRUCTIONDECODER_H
