#include "memory.h"
#include <iostream>
#include <fstream>
#include <stdexcept>

//
// Memory constructor
//
Memory::Memory() : Component(), memory(MEMORY_SIZE, 0), addr(0), data_in(0), wr_req(false), rd_req(false), data_out(0)
{
    type = Component::Type::MEMORY;
}

//
// Memory destructor
//
Memory::~Memory() {}

//
// Method to update inputs
//
void Memory::update_inputs()
{
    // Go through the input ports and update the corresponding attributes
    for (const auto& port : input_ports)
    {
        if (port.id == "addr")
        {
            addr = port.value;
        }
        else if (port.id == "data_in")
        {
            data_in = port.value;
        }
        else if (port.id == "wr_req")
        {
            wr_req = port.value;
        }
        else if (port.id == "rd_req")
        {
            rd_req = port.value;
        }
    }
}

//
// Method to execute memory operations
//
void Memory::execute()
{
    if (wr_req)
    {
        if (addr < MEMORY_SIZE)
        {
            memory[addr] = data_in;
            std::cout << "Memory write: addr=" << addr << ", data=" << data_in << std::endl;
        }
        else
        {
            throw std::out_of_range("Memory write address out of range");
        }
    }

    if (rd_req)
    {
        if (addr < MEMORY_SIZE)
        {
            data_out = memory[addr];
            std::cout << "Memory read: addr=" << addr << ", data=" << data_out << std::endl;
        }
        else
        {
            throw std::out_of_range("Memory read address out of range");
        }
    }
}

//
// Method to read memory from a file
//
void Memory::read_memory(const std::string &filename)
{
    std::ifstream infile(filename, std::ios::binary);
    if (!infile)
    {
        throw std::runtime_error("Cannot open memory file for reading");
    }

    infile.read(reinterpret_cast<char*>(memory.data()), MEMORY_SIZE * sizeof(uint32_t));
    if (!infile)
    {
        throw std::runtime_error("Error reading memory file");
    }

    infile.close();
}

//
// Method to write memory to a file
//
void Memory::write_memory(const std::string &filename)
{
    std::ofstream outfile(filename, std::ios::binary);
    if (!outfile)
    {
        throw std::runtime_error("Cannot open memory file for writing");
    }

    outfile.write(reinterpret_cast<const char*>(memory.data()), MEMORY_SIZE * sizeof(uint32_t));
    if (!outfile)
    {
        throw std::runtime_error("Error writing memory file");
    }

    outfile.close();
}

//
// Method to update the output ports
//
void Memory::update_outputs()
{
    // Go through the output ports and update the corresponding attributes
    for (auto& port : output_ports)
    {
        if (port.id == "data_out")
        {
            port.value = data_out;
        }
    }
}
