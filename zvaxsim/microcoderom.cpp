#include "microcoderom.h"
#include <iostream>
#include <fstream>
#include <stdexcept>

//
// MicrocodeRom constructor
//
MicrocodeRom::MicrocodeRom() : Component(), memory(MICROCODE_SIZE, 0), addr(0), rd_req(false), data_out(0)
{
    type = Component::Type::MICROCODE_ROM;
}

//
// MicrocodeRom destructor
//
MicrocodeRom::~MicrocodeRom() {}

//
// Method to update inputs
//
void MicrocodeRom::update_inputs()
{
    // Go through the input ports and update the corresponding attributes
    for (const auto& port : input_ports)
    {
        if (port.id == "addr")
        {
            addr = port.value;
        }
        else if (port.id == "rd_req")
        {
            rd_req = port.value;
        }
    }
}

//
// Method to execute memory operations
//
void MicrocodeRom::execute()
{
    if (rd_req)
    {
        if (addr < MICROCODE_SIZE)
        {
            data_out = memory[addr];
            std::cout << "Microcode ROM read: addr=" << addr << ", data=" << data_out << std::endl;
        }
        else
        {
            throw std::out_of_range("Microcode ROM read address out of range");
        }
    }
}

//
// Method to read microcode from a file
//
void MicrocodeRom::read_microcode(const std::string &filename)
{
    std::ifstream infile(filename, std::ios::binary);
    if (!infile)
    {
        throw std::runtime_error("Cannot open microcode file for reading");
    }

    infile.read(reinterpret_cast<char*>(memory.data()), MICROCODE_SIZE * sizeof(uint64_t));
    if (!infile)
    {
        throw std::runtime_error("Error reading microcode file");
    }

    infile.close();
}

//
// Method to write memory to a file
//
void MicrocodeRom::write_memory(const std::string &filename)
{
    std::ofstream outfile(filename, std::ios::binary);
    if (!outfile)
    {
        throw std::runtime_error("Cannot open microcode file for writing");
    }

    outfile.write(reinterpret_cast<const char*>(memory.data()), MICROCODE_SIZE * sizeof(uint64_t));
    if (!outfile)
    {
        throw std::runtime_error("Error writing microcode file");
    }

    outfile.close();
}

//
// Update the output ports
//
void MicrocodeRom::update_outputs()
{
    // Update the output ports with the corresponding attributes
    for (auto& port : output_ports)
    {
        if (port.id == "data_out")
        {
            port.value = data_out;
        }
    }
}
