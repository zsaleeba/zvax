#include "microprogramcounter.h"
#include <iostream>

//
// MicroProgramCounter constructor
//
MicroProgramCounter::MicroProgramCounter() : Component(), upc_set_in(0), set_req(false), upc(0)
{
    type = Component::Type::MICROPROGRAM_COUNTER;
}

//
// MicroProgramCounter destructor
//
MicroProgramCounter::~MicroProgramCounter() {}

//
// Method to update MicroProgramCounter inputs
//
void MicroProgramCounter::update_inputs()
{
    // Go through the input ports and update the corresponding attributes
    for (const auto& port : input_ports)
    {
        if (port.id == "upc_set_in")
        {
            upc_set_in = port.value;
        }
        else if (port.id == "set_req")
        {
            set_req = port.value;
        }
    }
}

//
// Method to execute MicroProgramCounter operation
//
void MicroProgramCounter::execute()
{
    if (set_req)
    {
        upc = upc_set_in;
        std::cout << "MicroProgramCounter set: upc=" << upc << std::endl;
    }
    else
    {
        ++upc;
        std::cout << "MicroProgramCounter incremented: upc=" << upc << std::endl;
    }
}

//
// Method to update MicroProgramCounter outputs
//
void MicroProgramCounter::update_outputs()
{
    // Go through the output ports and update the corresponding attributes
    for (auto& port : output_ports)
    {
        if (port.id == "upc")
        {
            port.value = upc;
        }
    }
}
