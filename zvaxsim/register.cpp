#include "register.h"
#include <iostream>

//
// Register constructor
//
Register::Register() : Component(), in(0), out(0), load(false)
{
    type = Component::Type::REGISTER; // Assuming "REGISTER" is a suitable type for a single register
}

//
// Register destructor
//
Register::~Register() {}

//
// Method to update Register inputs
//
void Register::update_inputs()
{
    // Go through the input ports and update the corresponding attributes
    for (const auto& port : input_ports)
    {
        if (port.id == "in")
        {
            in = port.value;
        }
        else if (port.id == "load")
        {
            load = port.value;
        }
    }
}

//
// Method to execute register operation
//
void Register::execute()
{
    if (load)
    {
        out = in;
        std::cout << "Register loaded: data=" << out << std::endl;
    }
}

//
// Method to update Register outputs
//
void Register::update_outputs()
{
    // Go through the output ports and update the corresponding attributes
    for (auto& port : output_ports)
    {
        if (port.id == "out")
        {
            port.value = out;
        }
    }
}
