#include <cstdint>
#include <vector>
#include <string>

#include "connection.h"
#include "port.h"
#include "simulator.h"


// Unique id allocation
uint32_t Connection::next_id = 1;

// Constructor
Connection::Connection() 
{
    id = getNextId();
}

// Update the output ports of the connection based on the input ports and the mux
void Connection::update(Simulator& simulator)
{
    // Get the mux value from the output port given in mux_input.
    int mux = 0;
    if (!mux_input.empty())
    {
        mux = simulator.get_port(mux_input)->value;
    }

    // Loop through the input ports to find a matching mux value
    for (size_t i = 0; i < inputs.size(); i++)
    {
        // Get the input port id and mux value
        std::string inputPortId = inputs[i].first;
        int inputMux = inputs[i].second;

        // If the mux value matches, use this input port to update the output ports
        if (inputMux == mux)
        {
            // Get the input port
            std::string inputPortId = inputs[i].first;
            std::shared_ptr<Port> inputPort = simulator.get_port(inputPortId);

            // Loop through the outputs and set them all
            for (const auto& outputPortId : outputs)
            {
                // Get the output port
                std::shared_ptr<Port> outputPort = simulator.get_port(outputPortId);

                // Copy the input port value to the output port
                outputPort->value = inputPort->value;
            }

            break;
        }
    }
}


// Allocate a unique id
uint32_t Connection::getNextId()
{
    return next_id++;
}


// Custom serialization function for the Direction enum
void to_json(nlohmann::json& j, const PathSegment::Direction& direction)
{
    switch (direction) {
        case PathSegment::Direction::HORIZONTAL:
            j = "horizontal";
            break;
        case PathSegment::Direction::VERTICAL:
            j = "vertical";
            break;
    }
}

// Custom deserialization function for the Direction enum
void from_json(const nlohmann::json& j, PathSegment::Direction& direction)
{
    std::string directionStr = j.get<std::string>();
    if (directionStr == "horizontal") {
        direction = PathSegment::Direction::HORIZONTAL;
    } else if (directionStr == "vertical") {
        direction = PathSegment::Direction::VERTICAL;
    } else {
        throw std::invalid_argument("Invalid PathSegment::Direction value");
    }
}

// Custom serialization function for std::shared_ptr<Connection>
void to_json(nlohmann::json& j, const std::shared_ptr<Connection>& connection)
{
    if (connection)
    {
        j = *connection;
    }
    else
    {
        j = nullptr;
    }
}

// Custom deserialization function for std::shared_ptr<Connection>
void from_json(const nlohmann::json& j, std::shared_ptr<Connection>& connection)
{
    if (j.is_null())
    {
        connection = nullptr;
    }
    else
    {
        connection = std::make_shared<Connection>();
        from_json(j, *connection);
    }
}
