#include "mainwindow.h"
#include "simulator.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    Simulator sim;
    sim.init();

    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
