#include "port.h"
#include "component.h"


// Get the port's id path, which is <component_id>.<port_id>
std::string Port::get_id_path() const
{
    return parent->id + "." + id;
}

// Custom serialization function for the Direction enum
void to_json(nlohmann::json& j, const Port::Direction& direction)
{
    switch (direction)
    {
        case Port::Direction::INPUT:
            j = "input";
            break;
        case Port::Direction::OUTPUT:
            j = "output";
            break;
    }
}

// Custom deserialization function for the Direction enum
void from_json(const nlohmann::json& j, Port::Direction& direction)
{
    std::string directionStr = j.get<std::string>();
    if (directionStr == "input")
    {
        direction = Port::Direction::INPUT;
    }
    else if (directionStr == "output")
    {
        direction = Port::Direction::OUTPUT;
    }
    else
    {
        throw std::invalid_argument("Invalid Port::Direction value");
    }
}

// Custom serialization function for the Orientation enum
void to_json(nlohmann::json& j, const Port::Orientation& orientation)
{
    switch (orientation)
    {
        case Port::Orientation::LEFT:
            j = "left";
            break;
        case Port::Orientation::RIGHT:
            j = "right";
            break;
        case Port::Orientation::UP:
            j = "up";
            break;
        case Port::Orientation::DOWN:
            j = "down";
            break;
    }
}

// Custom deserialization function for the Orientation enum
void from_json(const nlohmann::json& j, Port::Orientation& orientation)
{
    std::string orientationStr = j.get<std::string>();
    if (orientationStr == "left")
    {
        orientation = Port::Orientation::LEFT;
    }
    else if (orientationStr == "right")
    {
        orientation = Port::Orientation::RIGHT;
    }
    else if (orientationStr == "up")
    {
        orientation = Port::Orientation::UP;
    }
    else if (orientationStr == "down")
    {
        orientation = Port::Orientation::DOWN;
    }
    else
    {
        throw std::invalid_argument("Invalid Port::Orientation value");
    }
}
