#ifndef COMPONENT_H
#define COMPONENT_H

#include <string>
#include <vector>
#include <utility>
#include <unordered_map>
#include <memory>
#include <nlohmann/json.hpp>

#include "port.h"


//
// Class representing a CPU component with its attributes and JSON serialization support
//

class Component 
{
public:
    // The types of component that can be created
    enum class Type
    {
        NONE,
        PROGRAM_COUNTER,
        INSTRUCTION_BUFFER,
        INSTRUCTION_DECODER,
        ALU,
        REGISTER,
        REGISTER_FILE,
        MEMORY,
        CACHE,
        MICROCODE_ROM,
        MICROPROGRAM_COUNTER
    };

public:
    Type                type;         // Type of the component (e.g., "PC", "ALU")
    std::string         id;           // Unique identifier for the component
    std::string         graphic;      // Filename for the graphic associated with this component
    std::pair<int, int> location;     // Screen location of the component
    std::vector<Port>   input_ports;  // Input ports
    std::vector<Port>   output_ports; // Output ports

    // Mapping between Type enum and string representation
    static const std::unordered_map<Type, std::string> type_to_string_map;

public:
    Component();
    virtual ~Component();

    // Method to update ALU inputs
    virtual void update_inputs() = 0;

    // Method to execute ALU operation
    virtual void execute() = 0;

    // Update the output ports
    virtual void update_outputs() = 0;

    // Name conversions
    static std::string type_to_string(Type type);
    static Type string_to_type(const std::string& typeStr);

    // Factory methods to create components
    static Component* create(Type type);
    static Component* create(const std::string& typeStr);
};


// Custom serialization function for the Type enum
void to_json(nlohmann::json& j, const Component::Type& type);
void from_json(const nlohmann::json& j, Component::Type& type);

// Custom serialization function for the Component class
void to_json(nlohmann::json& j, const Component& component);
void from_json(const nlohmann::json& j, Component& component);

// Custom serialization function for std::shared_ptr<Component>
void to_json(nlohmann::json& j, const std::shared_ptr<Component>& component);
void from_json(const nlohmann::json& j, std::shared_ptr<Component>& component);

#endif // COMPONENT_H
