#ifndef ALU_H
#define ALU_H

#include <vector>
#include <cstdint>
#include <stdexcept>

#include "component.h"

//
// Class representing an ALU component, inheriting from Component
//

class Alu : public Component
{
public:
    unsigned int func; // Function code for ALU operation
    uint32_t a;        // First input
    uint32_t b;        // Second input
    uint32_t out;      // ALU output

    Alu();
    virtual ~Alu();

    // Method to update ALU inputs
    void update_inputs() override;

    // Method to execute ALU operation
    void execute() override;

    // Update the output ports
    void update_outputs() override;
};

#endif // ALU_H
