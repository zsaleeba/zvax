#include "component.h"
#include "port.h"
#include "programcounter.h"
#include "instructionbuffer.h"
#include "instructiondecoder.h"
#include "alu.h"
#include "cache.h"
#include "register.h"
#include "registerfile.h"
#include "memory.h"
#include "cache.h"
#include "microcoderom.h"
#include "microprogramcounter.h"


// Mapping between Type enum and string representation
const std::unordered_map<Component::Type, std::string> Component::type_to_string_map = 
{
    {Component::Type::NONE,                 ""},
    {Component::Type::PROGRAM_COUNTER,      "pc"},
    {Component::Type::INSTRUCTION_BUFFER,   "instruction_buffer"},
    {Component::Type::INSTRUCTION_DECODER,  "instruction_decoder"},
    {Component::Type::ALU,                  "alu"},
    {Component::Type::REGISTER_FILE,        "register_file"},
    {Component::Type::MEMORY,               "memory"},
    {Component::Type::CACHE,                "cache"},
    {Component::Type::MICROCODE_ROM,        "microcode_rom"},
    {Component::Type::MICROPROGRAM_COUNTER, "microprogram_counter"},
    {Component::Type::REGISTER,             "register"}
};

// Constructor and destructor
Component::Component() : type(Type::PROGRAM_COUNTER), id(""), graphic(""), location({0.0f, 0.0f}), input_ports(), output_ports()
{}

Component::~Component()
{}

// Name conversions
std::string Component::type_to_string(Type type)
{
    auto it = type_to_string_map.find(type);
    if (it != type_to_string_map.end()) {
        return it->second;
    } else {
        throw std::invalid_argument("Invalid Component::Type value");
    }
}

Component::Type Component::string_to_type(const std::string& typeStr)
{
    for (const auto& pair : type_to_string_map) {
        if (pair.second == typeStr) {
            return pair.first;
        }
    }
    return Type::NONE;
}

// Factory methods to create components
Component* Component::create(Type type)
{
    switch (type) 
    {
        case Type::NONE:
            return nullptr;
        case Type::PROGRAM_COUNTER:
            return new ProgramCounter();
        case Type::INSTRUCTION_BUFFER:
            return new InstructionBuffer();
        case Type::INSTRUCTION_DECODER:
            return new InstructionDecoder();
        case Type::ALU:
            return new Alu();
        case Type::REGISTER:
            return new Register();
        case Type::REGISTER_FILE:
            return new RegisterFile();
        case Type::MEMORY:
            return new Memory();
        case Type::CACHE:
            return new Cache();
        case Type::MICROCODE_ROM:
            return new MicrocodeRom();
        case Type::MICROPROGRAM_COUNTER:
            return new MicroProgramCounter();
    }
    return nullptr;
}

Component* Component::create(const std::string& typeStr)
{
    return create(string_to_type(typeStr));
}

// Custom serialization function for the Type enum
void to_json(nlohmann::json& j, const Component::Type& type)
{
    j = Component::type_to_string(type);
}

// Custom deserialization function for the Type enum
void from_json(const nlohmann::json& j, Component::Type& type)
{
    std::string typeStr;
    j.get_to(typeStr);
    type = Component::string_to_type(typeStr);
}

// Custom serialization function for the Component class
void to_json(nlohmann::json& j, const Component& component)
{
    j = nlohmann::json{
        {"type", component.type},
        {"id", component.id},
        {"graphic", component.graphic},
        {"location", component.location},
        {"input_ports", component.input_ports},
        {"output_ports", component.output_ports}
    };
}

// Custom deserialization function for the Component class
void from_json(const nlohmann::json& j, Component& component)
{
    j.at("type").get_to(component.type);
    j.at("id").get_to(component.id);
    j.at("graphic").get_to(component.graphic);
    j.at("location").get_to(component.location);
    j.at("input_ports").get_to(component.input_ports);
    j.at("output_ports").get_to(component.output_ports);
}

// Custom serialization function for std::shared_ptr<Component>
void to_json(nlohmann::json& j, const std::shared_ptr<Component>& component)
{
    if (component)
    {
        j = *component;
    }
    else
    {
        j = nullptr;
    }
}

// Custom deserialization function for std::shared_ptr<Component>
void from_json(const nlohmann::json& j, std::shared_ptr<Component>& component)
{
    if (j.is_null())
    {
        component = nullptr;
    }
    else
    {
        component = std::shared_ptr<Component>(Component::create(j.at("type").get<std::string>()));
        if (component)
        {
            from_json(j, *component);
        }
    }
}
