#ifndef REGISTERFILE_H
#define REGISTERFILE_H

#include <array>
#include <cstdint>
#include <stdexcept>

#include "component.h"

// Constants
static constexpr uint32_t REGISTER_COUNT = 16; // Number of registers

//
// Class representing a register file, inheriting from Component
//

class RegisterFile : public Component
{
private:
    std::array<uint32_t, REGISTER_COUNT> registers; // Register file

public:
    uint32_t rd_addr0;     // Address of first register port (in)
    uint32_t rd_data_out0; // Data read from first register port (out)
    uint32_t rd_addr1;     // Address of second register port (in)
    uint32_t rd_data_out1; // Data read from second register port (out)
    uint32_t wr_addr;      // Register address to write to register file (in)
    uint32_t wr_data_in;   // Data to write to register file (in)
    bool     wr_req;       // Write request signal (in)

    RegisterFile();
    virtual ~RegisterFile();

    // Method to update inputs
    void update_inputs() override;

    // Method to execute read/write operations
    void execute() override;

    // Update the output ports
    void update_outputs() override;
};

#endif // REGISTERFILE_H
