#include "programcounter.h"
#include <iostream>

//
// ProgramCounter constructor
//
ProgramCounter::ProgramCounter() : Component(), addr_in(0), load(false), step(false), step_size(0), pc(0)
{
    type = Component::Type::PROGRAM_COUNTER;
}

//
// ProgramCounter destructor
//
ProgramCounter::~ProgramCounter() {}

//
// Method to update Program Counter inputs
//
void ProgramCounter::update_inputs()
{
    // Go through the input ports and update the corresponding attributes
    for (const auto& port : input_ports)
    {
        if (port.id == "addr_in")
        {
            addr_in = port.value;
        }
        else if (port.id == "load")
        {
            load = port.value;
        }
        else if (port.id == "step")
        {
            step = port.value;
        }
        else if (port.id == "step_size")
        {
            step_size = port.value;
        }
    }
}

//
// Method to execute Program Counter operation
//
void ProgramCounter::execute()
{
    if (load)
    {
        pc = addr_in;
        std::cout << "Program Counter loaded: pc=" << pc << std::endl;
    }

    if (step)
    {
        pc += step_size;
        std::cout << "Program Counter stepped: step_size=" << step_size << ", new pc=" << pc << std::endl;
    }
}

//
// Update Program Counter outputs
//
void ProgramCounter::update_outputs()
{
    // Go through the output ports and update the corresponding attributes
    for (auto& port : output_ports)
    {
        if (port.id == "pc")
        {
            port.value = pc;
        }
    }
}
