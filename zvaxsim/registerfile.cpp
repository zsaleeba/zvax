#include "registerfile.h"
#include <iostream>

//
// RegisterFile constructor
//
RegisterFile::RegisterFile() : Component(), rd_addr0(0), rd_data_out0(0), rd_addr1(0), rd_data_out1(0),
                               wr_addr(0), wr_data_in(0), wr_req(false)
{
    type = Component::Type::REGISTER_FILE;
    registers.fill(0); // Initialize all registers to zero
}

//
// RegisterFile destructor
//
RegisterFile::~RegisterFile() {}

//
// Method to update RegisterFile inputs
//
void RegisterFile::update_inputs()
{
    // Go through the input ports and update the corresponding attributes
    for (const auto& port : input_ports)
    {
        if (port.id == "rd_addr0")
        {
            rd_addr0 = port.value;
        }
        else if (port.id == "rd_addr1")
        {
            rd_addr1 = port.value;
        }
        else if (port.id == "wr_addr")
        {
            wr_addr = port.value;
        }
        else if (port.id == "wr_data_in")
        {
            wr_data_in = port.value;
        }
        else if (port.id == "wr_req")
        {
            wr_req = port.value;
        }
    }
}

//
// Method to execute read/write operations
//
void RegisterFile::execute()
{
    // Handle write request
    if (wr_req)
    {
        if (wr_addr < REGISTER_COUNT)
        {
            registers[wr_addr] = wr_data_in;
            std::cout << "Register write: addr=" << wr_addr << ", data=" << wr_data_in << std::endl;
        }
        else
        {
            throw std::out_of_range("Register write address out of range");
        }
    }

    // Handle read requests
    if (rd_addr0 < REGISTER_COUNT)
    {
        rd_data_out0 = registers[rd_addr0];
        std::cout << "Register read 0: addr=" << rd_addr0 << ", data=" << rd_data_out0 << std::endl;
    }
    else
    {
        throw std::out_of_range("Register read address 0 out of range");
    }

    if (rd_addr1 < REGISTER_COUNT)
    {
        rd_data_out1 = registers[rd_addr1];
        std::cout << "Register read 1: addr=" << rd_addr1 << ", data=" << rd_data_out1 << std::endl;
    }
    else
    {
        throw std::out_of_range("Register read address 1 out of range");
    }
}

//
// Method to update RegisterFile outputs
//
void RegisterFile::update_outputs()
{
    // Go through the output ports and update the corresponding attributes
    for (auto& port : output_ports)
    {
        if (port.id == "rd_data_out0")
        {
            port.value = rd_data_out0;
        }
        else if (port.id == "rd_data_out1")
        {
            port.value = rd_data_out1;
        }
    }
}
