/*
 * CpuDescription.cpp
 *
 * Description:
 * This source file contains the implementation of the CpuDescription class, which manages
 * a representation of a CPU's components and connections. The class supports dynamic
 * addition, removal, and modification of components and connections. It also provides
 * methods for loading from and saving to a JSON file, using the nlohmann::json library
 * for serialization and deserialization.
 *
 * The CpuDescription class uses standard C++ idioms to manage its state and interactions.
 *
 * Fields:
 * - components: A vector of Component objects representing the components of the CPU.
 * - connections: A vector of Connection objects representing the connections between the components.
 *
 * Classes:
 * - Port: Represents a port on a CPU component, including its ID, type (input/output), number of bits, location, and orientation.
 * - Component: Represents a CPU component, including its type, ID, graphic filename, location, and ports.
 * - PathSegment: Represents a segment of a connection's path, including its direction and length.
 * - Connection: Represents a connection between ports, including its ID, input ports, output ports, path segments, and arrowheads.
 *
 * Methods:
 * - CpuDescription(): Constructor.
 * - bool loadFromFile(const std::string &fileName): Loads the CPU description from a JSON file.
 * - bool saveToFile(const std::string &fileName) const: Saves the CPU description to a JSON file.
 * - void addComponent(const Component &component): Adds a new component.
 * - bool removeComponent(const std::string &id): Removes a component by ID.
 * - bool updateComponent(const std::string &id, const Component &component): Updates an existing component by ID.
 * - void addConnection(const Connection &connection): Adds a new connection.
 * - bool removeConnection(const std::string &id): Removes a connection by ID.
 * - bool updateConnection(const std::string &id, const Connection &connection): Updates an existing connection by ID.
 *
 * Usage:
 * To use this class, create an instance of CpuDescription, and use the provided methods to manage CPU components and connections.
 * Example:
 * ```
 * CpuDescription cpuDescription;
 *
 * // Add a component
 * Component component;
 * component.type = Component::Type::ALU;
 * component.id = "alu1";
 * component.graphic = "alu.png";
 * component.location = {100.0, 100.0};
 * Port port;
 * port.id = "input1";
 * port.type = "input";
 * port.bits = 32;
 * port.location = {10.0, 10.0};
 * port.orientation = "left";
 * component.ports.push_back(port);
 * cpuDescription.addComponent(component);
 *
 * // Save to file
 * cpuDescription.saveToFile("cpu_description.json");
 *
 * // Load from file
 * cpuDescription.loadFromFile("cpu_description.json");
 * ```
 */

#include "cpu_description.h"
#include <algorithm>
#include <iostream>
#include "component.h"
#include "connection.h"

// Constructor
CpuDescription::CpuDescription() {}

// Load CPU description from a file
bool CpuDescription::loadFromFile(const std::string &fileName)
{
    components.clear();
    connections.clear();

    std::string content;
    if (!readFile(fileName, content))
    {
        return false;  // Return false if the file cannot be read
    }

    try
    {
        nlohmann::json json = nlohmann::json::parse(content);
        json.at("components").get_to(components);
        json.at("connections").get_to(connections);
    }
    catch (const nlohmann::json::exception &e)
    {
        std::cerr << "Error parsing JSON: " << e.what() << std::endl;
        return false;  // Return false if JSON parsing fails
    }

    return true;  // Return true if loading and parsing are successful
}

// Save CPU description to a file
bool CpuDescription::saveToFile(const std::string &fileName) const
{
    nlohmann::json json;
    json["components"] = components;
    json["connections"] = connections;

    std::string content = json.dump(4); // Pretty print with 4 spaces
    return writeFile(fileName, content);
}

// Helper method to read file content into a string
bool CpuDescription::readFile(const std::string &fileName, std::string &content) const
{
    std::ifstream file(fileName);
    if (!file.is_open())
    {
        std::cerr << "Couldn't open file for reading: " << fileName << std::endl;
        return false;
    }

    std::string line;
    while (std::getline(file, line))
    {
        content += line + "\n";
    }
    file.close();
    return true;
}

// Helper method to write string content to a file
bool CpuDescription::writeFile(const std::string &fileName, const std::string &content) const
{
    std::ofstream file(fileName);
    if (!file.is_open())
    {
        std::cerr << "Couldn't open file for writing: " << fileName << std::endl;
        return false;
    }

    file << content;
    file.close();
    return true;
}

// Add a new component
void CpuDescription::addComponent(const Component &component)
{
    std::shared_ptr<Component> newComponent(Component::create(component.type));
    if (newComponent)
    {
        *newComponent = component;
        components.push_back(newComponent);
    }
}

// Remove a component by ID
bool CpuDescription::removeComponent(const std::string &id)
{
    auto it = std::remove_if(components.begin(), components.end(),
                             [&id](const std::shared_ptr<Component> &component)
                             { return component->id == id; });
    if (it != components.end())
    {
        components.erase(it, components.end());
        return true;
    }
    return false;
}

// Update an existing component by ID
bool CpuDescription::updateComponent(const std::string &id, const Component &component)
{
    for (auto &existingComponent : components)
    {
        if (existingComponent->id == id)
        {
            std::shared_ptr<Component> updatedComponent(Component::create(component.type));
            if (updatedComponent)
            {
                *updatedComponent = component;
                existingComponent = updatedComponent;
                return true;
            }
        }
    }
    return false;
}

// Add a new connection
uint32_t CpuDescription::addConnection(const Connection &connection)
{
    auto conn = std::make_shared<Connection>(connection);
    connections.push_back(conn);
    return conn->id;
}

// Remove a connection by ID
bool CpuDescription::removeConnection(uint32_t id)
{
    auto it = std::remove_if(connections.begin(), connections.end(),
                             [&id](const std::shared_ptr<Connection> &connection)
                             { return connection->id == id; });
    if (it != connections.end())
    {
        connections.erase(it, connections.end());
        return true;
    }
    return false;
}

// Update an existing connection by ID
bool CpuDescription::updateConnection(uint32_t id, const Connection &connection)
{
    for (auto &existingConnection : connections)
    {
        if (existingConnection->id == id)
        {
            *existingConnection = connection;
            return true;
        }
    }
    return false;
}
