#include "cache.h"
#include <iostream>

//
// CacheLine constructor
//
CacheLine::CacheLine() : data({0}), recently_used(false), valid(false), dirty(false) 
{}

//
// Cache constructor
//
Cache::Cache() : Component(), cache(CACHE_LINES / CACHE_SET_ASSOCIATIVE), instr_addr(0), instr_rd_req(false), instr_out(0), instr_rd_ok(false),
                 data_addr(0), data_wr_req(false), data_in(0), data_rd_req(false), data_out(0), data_rd_ok(false)
{
    type = Component::Type::MEMORY;
}

//
// Cache destructor
//
Cache::~Cache() 
{}

//
// Method to update cache inputs
//
void Cache::update_inputs() 
{
    // Go through the input ports and update the corresponding attributes
    for (const auto& port : input_ports)
    {
        if (port.id == "instr_addr")
        {
            instr_addr = port.value;
        } 
        else if (port.id == "instr_rd_req")
        {
            instr_rd_req = port.value;
        } 
        else if (port.id == "data_addr")
        {
            data_addr = port.value;
        } 
        else if (port.id == "data_wr_req")
        {
            data_wr_req = port.value;
        } 
        else if (port.id == "data_in")
        {
            data_in = port.value;
        } 
        else if (port.id == "data_rd_req")
        {
            data_rd_req = port.value;
        }
    }
}

//
// Method to execute cache operation
//
void Cache::execute() 
{
    // Instruction read operation
    if (instr_rd_req) 
    {
        uint32_t index = (instr_addr / CACHE_LINE_SIZE) % (CACHE_LINES / CACHE_SET_ASSOCIATIVE);
        uint32_t tag = instr_addr / (CACHE_LINES / CACHE_SET_ASSOCIATIVE);
        CacheSet& cacheSet = cache[index];
        bool hit = false;

        for (auto& line : cacheSet) 
        {
            if (line.valid && line.data[0] == tag) 
            { // Assume first word holds the tag for simplicity
                hit = true;
                line.recently_used = true;
                instr_out = line.data[(instr_addr % CACHE_LINE_SIZE)];
                instr_rd_ok = true;
                std::cout << "Instruction cache read hit: addr=" << instr_addr << ", data=" << instr_out << std::endl;
                break;
            }
        }

        if (!hit) 
        {
            instr_rd_ok = false;
            std::cout << "Instruction cache read miss: addr=" << instr_addr << std::endl;
        }

        // Reset recently used bit for LRU policy
        for (auto& line : cacheSet) 
        {
            if (&line != &cacheSet[0] || hit) 
            {
                line.recently_used = false;
            }
        }
    }

    // Data read/write operation
    if (data_rd_req || data_wr_req) 
    {
        uint32_t index = (data_addr / CACHE_LINE_SIZE) % (CACHE_LINES / CACHE_SET_ASSOCIATIVE);
        uint32_t tag = data_addr / (CACHE_LINES / CACHE_SET_ASSOCIATIVE);
        CacheSet& cacheSet = cache[index];
        bool hit = false;

        for (auto& line : cacheSet) 
        {
            if (line.valid && line.data[0] == tag) 
            { // Assume first word holds the tag for simplicity
                hit = true;
                line.recently_used = true;

                if (data_wr_req) 
                {
                    // Write operation
                    line.data[(data_addr % CACHE_LINE_SIZE)] = data_in;
                    line.dirty = true;
                    std::cout << "Data cache write hit: addr=" << data_addr << ", data=" << data_in << std::endl;
                } 
                else if (data_rd_req) 
                {
                    // Read operation
                    data_out = line.data[(data_addr % CACHE_LINE_SIZE)];
                    data_rd_ok = true;
                    std::cout << "Data cache read hit: addr=" << data_addr << ", data=" << data_out << std::endl;
                }
                break;
            }
        }

        CacheLine* lineToReplace = nullptr;

        if (!hit)
        {
            data_rd_ok = false;
            std::cout << "Data cache read/write miss: addr=" << data_addr << std::endl;

            // Cache miss, find a line to replace
            for (auto& line : cacheSet) 
            {
                if (!line.valid) 
                {
                    lineToReplace = &line;
                    break;
                }
            }

            if (!lineToReplace) 
            {
                // All lines are valid, replace the least recently used (LRU)
                lineToReplace = &cacheSet[0];
                for (auto& line : cacheSet) 
                {
                    if (!line.recently_used) 
                    {
                        lineToReplace = &line;
                        break;
                    }
                }
            }

            // Update the line to replace
            if (data_wr_req) 
            {
                lineToReplace->data[(data_addr % CACHE_LINE_SIZE)] = data_in;
                lineToReplace->dirty = true;
                std::cout << "Data cache write miss: addr=" << data_addr << ", data=" << data_in << std::endl;
            } 
            else if (data_rd_req) 
            {
                // Read operation
                data_out = 0; // For simplicity, assume data comes from memory (not shown here)
                lineToReplace->data[(data_addr % CACHE_LINE_SIZE)] = data_out;
                std::cout << "Data cache read miss: addr=" << data_addr << ", data=" << data_out << std::endl;
            }

            lineToReplace->data[0] = tag; // Store tag in the first word
            lineToReplace->valid = true;
            lineToReplace->recently_used = true;
        }

        // Reset recently used bit for LRU policy
        for (auto& line : cacheSet) 
        {
            if (&line != lineToReplace) 
            {
                line.recently_used = false;
            }
        }
    }
}

// Method to update cache outputs
void Cache::update_outputs() 
{
    // Update the output ports with the corresponding attributes
    for (auto& port : output_ports)
    {
        if (port.id == "instr_out") 
        {
            port.value = instr_out;
        } 
        else if (port.id == "instr_rd_ok") 
        {
            port.value = instr_rd_ok;
        } 
        else if (port.id == "data_out") 
        {
            port.value = data_out;
        } 
        else if (port.id == "data_rd_ok") 
        {
            port.value = data_rd_ok;
        }
    }
}
