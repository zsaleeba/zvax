#include "alu.h"
#include <iostream>

Alu::Alu() : Component(), func(0), a(0), b(0), out(0)
{
    type = Component::Type::ALU;
}

Alu::~Alu()
{}


// Method to update ALU inputs
void Alu::update_inputs()
{
    // Go through the input ports and update the corresponding attributes
    for (const auto& port : input_ports) {
        if (port.id == "func") {
            func = port.value;
        } else if (port.id == "a") {
            a = port.value;
        } else if (port.id == "b") {
            b = port.value;
        }
    }

    // Print the inputs for debugging
    std::cout << "ALU Update: func=" << func << ", a=" << a << ", b=" << b << std::endl;
}

// Method to execute ALU operation
void Alu::execute()
{
    switch (func) {
        case 0: // ADD
            out = a + b;
            break;
        case 1: // SUBTRACT
            out = a - b;
            break;
        case 2: // AND
            out = a & b;
            break;
        case 3: // OR
            out = a | b;
            break;
        case 4: // XOR
            out = a ^ b;
            break;
        case 5: // SHIFT LEFT
            out = a << b;
            break;
        case 6: // SHIFT RIGHT
            out = a >> b;
            break;
        default:
            throw std::invalid_argument("Invalid ALU function code");
    }

    // Print the result for debugging
    std::cout << "ALU Execute: func=" << func << ", a=" << a << ", b=" << b << ", out=" << out << std::endl;
}

// Update the output ports
void Alu::update_outputs()
{
    // Update the output ports with the result
    for (auto& port : output_ports) {
        if (port.id == "out") {
            port.value = out;
        }
    }

    // Print the output for debugging
    std::cout << "ALU Update Outputs: out=" << out << std::endl;
}