from enum import Enum

class MarkerType(Enum):
    REGISTRATION_MARK = 1
    COMPONENT = 2
    PAD = 3
    PIN = 4

class Marker:
    def __init__(self, marker_type: MarkerType, x, y, xsize = 1, ysize = 1):
        self.marker_type = marker_type
        self.x = x
        self.y = y
        self.xsize = xsize
        self.ysize = ysize
