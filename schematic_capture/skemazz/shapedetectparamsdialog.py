import sys
from PyQt6.QtWidgets import QApplication, QDialog, QVBoxLayout, QFormLayout, QLineEdit, QPushButton, QDialogButtonBox
from shapedetect import ShapeDetectParameters

class ShapeDetectParametersDialog(QDialog):
    def __init__(self, parameters, parent=None):
        super().__init__(parent)
        self.parameters = parameters
        
        self.setWindowTitle("Shape Detection Parameters")

        # Create form layout
        self.form_layout = QFormLayout()
        
        # Create input fields
        self.circle_diameter_edit = QLineEdit(str(self.parameters.circle_diameter))
        self.cross_length_edit = QLineEdit(str(self.parameters.cross_length))
        self.dp_edit = QLineEdit(str(self.parameters.dp))
        self.minDist_edit = QLineEdit(str(self.parameters.minDist))
        self.param1_edit = QLineEdit(str(self.parameters.param1))
        self.param2_edit = QLineEdit(str(self.parameters.param2))
        self.minRadius_edit = QLineEdit(str(self.parameters.minRadius))
        self.maxRadius_edit = QLineEdit(str(self.parameters.maxRadius))
        self.line_threshold_edit = QLineEdit(str(self.parameters.line_threshold))
        self.min_line_length_edit = QLineEdit(str(self.parameters.min_line_length))
        self.max_line_gap_edit = QLineEdit(str(self.parameters.max_line_gap))

        # Add input fields to form layout
        self.form_layout.addRow("Pad Diameter:", self.circle_diameter_edit)
        self.form_layout.addRow("DP:", self.dp_edit)
        self.form_layout.addRow("Min Distance:", self.minDist_edit)
        self.form_layout.addRow("Param1:", self.param1_edit)
        self.form_layout.addRow("Param2:", self.param2_edit)
        self.form_layout.addRow("Min Radius:", self.minRadius_edit)
        self.form_layout.addRow("Max Radius:", self.maxRadius_edit)
        self.form_layout.addRow("Cross Length:", self.cross_length_edit)
        self.form_layout.addRow("Line Threshold:", self.line_threshold_edit)
        self.form_layout.addRow("Min Line Length:", self.min_line_length_edit)
        self.form_layout.addRow("Max Line Gap:", self.max_line_gap_edit)

        # Create OK and Cancel buttons
        self.button_box = QDialogButtonBox(QDialogButtonBox.StandardButton.Ok | QDialogButtonBox.StandardButton.Cancel)
        self.button_box.accepted.connect(self.accept)
        self.button_box.rejected.connect(self.reject)

        # Set the layout
        main_layout = QVBoxLayout()
        main_layout.addLayout(self.form_layout)
        main_layout.addWidget(self.button_box)
        self.setLayout(main_layout)

    def accept(self):
        # Update parameters with the values from the input fields
        self.parameters.circle_diameter = int(self.circle_diameter_edit.text())
        self.parameters.cross_length = int(self.cross_length_edit.text())
        self.parameters.dp = float(self.dp_edit.text())
        self.parameters.minDist = int(self.minDist_edit.text())
        self.parameters.param1 = int(self.param1_edit.text())
        self.parameters.param2 = int(self.param2_edit.text())
        self.parameters.minRadius = int(self.minRadius_edit.text())
        self.parameters.maxRadius = int(self.maxRadius_edit.text())
        self.parameters.line_threshold = int(self.line_threshold_edit.text())
        self.parameters.min_line_length = int(self.min_line_length_edit.text())
        self.parameters.max_line_gap = int(self.max_line_gap_edit.text())

        super().accept()
