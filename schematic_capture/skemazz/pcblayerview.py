import numpy as np
from PyQt6.QtCore import Qt
from PyQt6.QtGui import QColor, QVector3D, QVector4D, QMatrix4x4, QSurfaceFormat
from PyQt6.QtWidgets import QWidget, QVBoxLayout
from PyQt6.Qt3DCore import QEntity, QTransform
from PyQt6.Qt3DExtras import Qt3DWindow, QOrbitCameraController
from PyQt6.Qt3DRender import QPointLight
from PyQt6.Qt3DInput import QMouseDevice, QMouseHandler, QMouseEvent
from pcblayer import PcbLayer

class PcbLayerWidget(QWidget):
    def __init__(self, zoom_slider, handle_view_click):
        super().__init__()
        
        # Create the Qt3D window
        self.view = Qt3DWindow()
        self.container = QWidget.createWindowContainer(self.view, self)
        
        # Set up the surface format for antialiasing
        format = QSurfaceFormat()
        format.setSamples(4)  # Enable 4x multisampling
        self.view.setFormat(format)

        # Set the clear color to white
        self.view.defaultFrameGraph().setClearColor(Qt.GlobalColor.white)
        self.rootEntity = QEntity()
        
        # Camera
        self.camera_max_distance = 20
        self.camera_x = 0
        self.camera_y = 0
        self.zoom_around_point = None
        self.cameraEntity = self.view.camera()
        self.cameraEntity.lens().setPerspectiveProjection(45.0, 16.0 / 9.0, 0.1, 1000.0)
        self.cameraEntity.setPosition(QVector3D(0, 0, self.camera_max_distance))
        self.cameraEntity.setViewCenter(QVector3D(0, 0, 0))

        # Light source
        lightEntity = QEntity(self.rootEntity)
        light = QPointLight(lightEntity)
        light.setColor(QColor(Qt.GlobalColor.white))
        light.setIntensity(1)
        lightTransform = QTransform()
        lightTransform.setTranslation(QVector3D(20, 0, 0))
        lightEntity.addComponent(light)
        lightEntity.addComponent(lightTransform)

        self.view.setRootEntity(self.rootEntity)
        
        self.layers = []

        # Set layout
        layout = QVBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)
        layout.addWidget(self.container)

        # Connect zoom slider
        self.zoom_slider = zoom_slider
        self.zoom_slider.valueChanged.connect(self.adjust_zoom) 
        self.handle_view_click = handle_view_click

        # Setup Mouse Device
        self.mouse_device = QMouseDevice()
        self.mouse_handler = QMouseHandler(self.rootEntity)
        self.mouse_handler.setSourceDevice(self.mouse_device)

        # Connect a signal to handle the mouse input
        self.mouse_handler.pressed.connect(self.handle_mouse_press)
        self.mouse_handler.wheel.connect(self.handle_mouse_scroll)
        self.rootEntity.addComponent(self.mouse_handler)

    def adjust_zoom(self):
        slider_value = self.zoom_slider.value() - 5
        new_distance = self.camera_max_distance * (0.8 ** slider_value)

        zoom_around_point = self.zoom_around_point
        self.zoom_around_point = None

        if zoom_around_point is not None:
            new_point = self.convert_click_to_world(zoom_around_point[0], zoom_around_point[1], new_distance)
            self.camera_x = new_point.x()
            self.camera_y = new_point.y()

        self.cameraEntity.setPosition(QVector3D(self.camera_x, self.camera_y, new_distance))
        self.cameraEntity.setViewCenter(QVector3D(self.camera_x, self.camera_y, 0))

    def addLayer(self, texture, z_order=0, transparency=1.0, color=QColor(Qt.GlobalColor.green)):
        layer = PcbLayer(self.rootEntity, texture, z_order, transparency, color)
        self.layers.append(layer)
        return layer

    def getLayer(self, index):
        print("getLayer(", index, "). number of layers: ", len(self.layers))
        return self.layers[index]

    def setLayerVertices(self, index, vertices):
        self.layers[index].setVertices(vertices)

    def getLayerVertices(self, index):
        return self.layers[index].getVertices()

    def handle_mouse_press(self, event: QMouseEvent):
        # Handle the mouse press event, event contains all the details about the mouse click
        print(f"Mouse clicked at: {event.x()}, {event.y()}, buttons: {event.buttons()}")
        world_coord = self.convert_click_to_world(event.x(), event.y(), 0)
        self.handle_view_click(world_coord)

    def handle_mouse_scroll(self, event: QMouseEvent):
        # Handle the mouse scroll event, event contains all the details about the scroll
        self.zoom_around_point = [event.x(), event.y()]
        if (event.angleDelta().y() > 0):
            self.zoom_slider.setValue(self.zoom_slider.value() + 1)
        else:
            self.zoom_slider.setValue(self.zoom_slider.value() - 1)
    
    def setTilt(self, tilt_angle_degrees):
        """Set the tilt of all the layers in degrees."""
        for layer in self.layers:
            layer.setTilt(tilt_angle_degrees)

    def convert_click_to_world(self, x, y, z_plane):
        # Get the dimensions of the window
        width = self.view.width()
        height = self.view.height()

        # Convert screen coordinates to normalized device coordinates (NDC)
        ndc_x = (2.0 * x) / width - 1.0
        ndc_y = 1.0 - (2.0 * y) / height
        ndc_z = 1.0  # Near plane

        # Create a point in NDC space
        ndc_point = QVector4D(ndc_x, ndc_y, ndc_z, 1.0)

        # Convert the point from NDC space to view space
        projection_matrix = QMatrix4x4(self.cameraEntity.projectionMatrix())
        inverted_projection_matrix = projection_matrix.inverted()[0]

        view_point = inverted_projection_matrix * ndc_point
        view_point /= view_point.w()  # Normalize the point

        # Convert the point from view space to world space
        view_matrix = QMatrix4x4(self.cameraEntity.viewMatrix())
        inverted_view_matrix = view_matrix.inverted()[0]

        world_point = inverted_view_matrix * view_point

        # Compute the ray direction in world space
        ray_direction = QVector3D(world_point.toVector3D())
        ray_direction -= self.cameraEntity.position()
        ray_direction.normalize()

        # Compute the intersection of the ray with the specified z plane
        camera_position = self.cameraEntity.position()
        t = (z_plane - camera_position.z()) / ray_direction.z()
        intersection_point = camera_position + t * ray_direction

        return intersection_point