#!/usr/bin/env python

# Turn pcb layer images into netlists and schematics
import os
import json
import sys
import math
from PyQt6.QtWidgets import (QApplication, QMainWindow, QVBoxLayout, QHBoxLayout,
                             QPushButton, QWidget, QMenu, QLabel, QSlider, QSizePolicy,
                             QFileDialog, QGraphicsScene, QDialog, QScrollArea)
from PyQt6.QtGui import QImage, QColor, QIcon, QAction
from PyQt6.QtCore import Qt, QPointF
import numpy as np
from tooltype import ToolType
from pcblayerview import PcbLayerWidget
from shapedetect import ShapeDetect, ShapeDetectParameters
from shapedetectparamsdialog import ShapeDetectParametersDialog
from marker import Marker, MarkerType

class Skemazz(QMainWindow):
    def __init__(self):
        super().__init__()

        # Get the path to the script directory (following soft links if necessary)
        script_path = os.path.abspath(__file__)
        while os.path.islink(script_path):
            script_path = os.path.abspath(os.path.join(os.path.dirname(script_path), os.readlink(script_path)))
        self.script_dir = os.path.dirname(script_path)

        self.image_names = ["components.tiff", "layer_1.tiff", "layer_2.tiff"]
        self.layer_button_labels = ["Component", "Layer 1", "Layer 2"]
        self.layer_names = ["components", "layer_1", "layer_2"]
        self.layer_colors = [Qt.GlobalColor.darkRed, Qt.GlobalColor.darkGreen, Qt.GlobalColor.blue]
        self.layer_images = [None, None, None]
        self.currently_checked = None  # Track the currently checked button
        self.NAME_KEY = 0
        self.current_tool = ToolType.NONE
        self.shape_detect_params = ShapeDetectParameters()
        self.program_state = self.read_state_file()
        self.registration_mark_items = [ [None, None] for name in self.layer_names ]
        self.image_pixmaps = [None, None, None]
        self.board_folder = None
        self.tilt = 0
        self.initUI()

    def __del__(self):
        self.write_state_file()

    def initUI(self):
        self.setWindowTitle('Skemazz')
        self.setGeometry(100, 100, 1800, 1100)

        # Create a menu bar
        menu_bar = self.menuBar()
        file_menu = QMenu("File", self)
        load_action = QAction("Open folder...", self)
        load_action.triggered.connect(self.open_board)
        file_menu.addAction(load_action)
        save_action = QAction("Save", self)
        save_action.triggered.connect(self.handle_save_board)
        file_menu.addAction(save_action)
        exit_action = QAction("Exit", self)
        exit_action.triggered.connect(self.close)
        file_menu.addAction(exit_action)
        menu_bar.addMenu(file_menu)

        # Process menu
        process_menu = QMenu("Process", self)
        set_detect_params_action = process_menu.addAction("Set detection parameters...")
        set_detect_params_action.triggered.connect(self.set_detection_parameters)
        process_menu.addSeparator()
        detect_components_action = process_menu.addAction("Detect components")
        detect_components_action.triggered.connect(self.detect_components)
        detect_pads_action = process_menu.addAction("Detect pads")
        detect_pads_action.triggered.connect(self.detect_pads)
        detect_traces_action = process_menu.addAction("Detect traces")
        detect_traces_action.triggered.connect(self.detect_traces)
        menu_bar.addMenu(process_menu)

        # Main layout
        main_layout = QHBoxLayout()
        main_layout.setContentsMargins(0, 0, 0, 0)
        main_layout.setSpacing(0)

        # Zoom slider
        self.zoom_factor = 5
        self.zoom_slider = QSlider(Qt.Orientation.Horizontal)
        self.zoom_slider.setMinimum(0)
        self.zoom_slider.setMaximum(20)
        self.zoom_slider.setValue(self.zoom_factor)
        self.zoom_slider.setTickPosition(QSlider.TickPosition.TicksBelow)
        self.zoom_slider.setTickInterval(1)
        self.zoom_slider.previousValue = 0

        # PcbLayerWidget
        self.pcbLayerWidget = PcbLayerWidget(self.zoom_slider, self.handle_view_click)
        self.scene = QGraphicsScene()

        # Main control layout
        controls_layout = QVBoxLayout()
        controls_layout.addWidget(QLabel("Zoom"))
        controls_layout.addWidget(self.zoom_slider)

        # Tilt slider
        self.tilt_slider = QSlider(Qt.Orientation.Horizontal)
        self.tilt_slider.setMinimum(-50)
        self.tilt_slider.setMaximum(50)
        self.tilt_slider.setValue(0)
        self.tilt_slider.setTickPosition(QSlider.TickPosition.TicksBelow)
        self.tilt_slider.setTickInterval(1)
        self.tilt_slider.valueChanged.connect(self.adjust_tilt)
        self.tilt_slider.previousValue = 0
        controls_layout.addWidget(QLabel("Tilt"))
        controls_layout.addWidget(self.tilt_slider)

        # Layer buttons side by side
        layer_layout = QHBoxLayout()
        self.layer_buttons = []
        for label in self.layer_button_labels:
            btn = QPushButton(label)
            btn.setCheckable(True)
            btn.setMinimumWidth(50)
            btn.setMaximumWidth(100)
            btn.setChecked(True)
            btn.clicked.connect(lambda checked, l=label: self.toggle_pixmap_visibility(checked, l))
            layer_layout.addWidget(btn)
            self.layer_buttons.append(btn)
        controls_layout.addWidget(QLabel("Layers"))
        controls_layout.addLayout(layer_layout)
        self.layer_buttons[0].setChecked(True)

        # Registration toggle buttons in a compact horizontal row
        toggle_button_layout = QHBoxLayout()
        toggle_button_layout.setSpacing(5)  # Reduce spacing between buttons
        button_labels = ["Ca", "Cb", "1a", "1b", "2a", "2b"]
        self.toggle_buttons = []
        for i, label in enumerate(button_labels):
            btn = QPushButton(label)
            btn.setCheckable(True)
            btn.setMinimumWidth(30)  # Set a minimal width to keep buttons compact
            btn.setMaximumWidth(50)  # You can adjust these widths as needed
            layer_no = i // 2
            reg_no = i % 2
            btn.clicked.connect(lambda checked, b=btn, l=layer_no, r=reg_no: self.handle_registration_button_click(b, l, r))
            self.toggle_buttons.append(btn)
            toggle_button_layout.addWidget(btn)
        controls_layout.addWidget(QLabel("Registration marks"))
        controls_layout.addLayout(toggle_button_layout)

        controls_layout.addStretch()

        # Limit the controls layout to a fixed width
        controls_widget = QWidget()
        controls_widget.setLayout(controls_layout)
        controls_widget.setSizePolicy(QSizePolicy.Policy.Fixed, QSizePolicy.Policy.Preferred)
        controls_widget.setMaximumWidth(250)

        # Add widgets to the main layout
        main_layout.addWidget(self.pcbLayerWidget)
        main_layout.addWidget(controls_widget)

        # Add a toolbar to the window
        toolbar = self.addToolBar("Tools")
        icon_path = os.path.join(self.script_dir, "images")

        # Add a button with a hand icon for the movement tool
        self.movement_action = QAction("Movement", self)
        # If toggled on select the movement tool. If toggled off, select the NONE tool
        self.movement_action.triggered.connect(self.toggle_movement_tool)
        self.movement_action.setCheckable(True)
        self.movement_action.setIcon(QIcon(os.path.join(icon_path, "hand.png")))
        toolbar.addAction(self.movement_action)

        # Add a button with a pad icon for the "mark pad" tool
        self.mark_pad_action = QAction("Mark pad", self)
        self.mark_pad_action.triggered.connect(self.toggle_mark_pad_tool)
        self.mark_pad_action.setCheckable(True)
        self.mark_pad_action.setIcon(QIcon(os.path.join(icon_path, "pad.png")))
        toolbar.addAction(self.mark_pad_action)

        # Add a button with a crop icon for the "outline component" tool
        self.outline_action = QAction("Outline component", self)
        self.outline_action.triggered.connect(self.toggle_outline_component_tool)
        self.outline_action.setCheckable(True)
        self.outline_action.setIcon(QIcon(os.path.join(icon_path, "crop.png")))
        toolbar.addAction(self.outline_action)

        # Load the images
        if self.program_state and "pwd" in self.program_state:
            self.load_board(self.program_state["pwd"])
        else:
            self.load_board(os.getcwd())

        # Central widget
        central_widget = QWidget()
        central_widget.setLayout(main_layout)
        self.setCentralWidget(central_widget)

        # Show it
        self.show()
        self.fit_image_in_view()

    # Read a JSON state file from $HOME/.config/skemazz/state.json
    def read_state_file(self):
        state_file = os.path.join(os.path.expanduser("~"), ".config", "skemazz", "state.json")
        if os.path.exists(state_file):
            with open(state_file, "r") as f:
                try:
                    state = json.load(f)

                    # Does it have a "pwd" key?
                    if "pwd" in state:
                        # Change the working directory to the one stored in the state file
                        os.chdir(state["pwd"])
                except json.JSONDecodeError:
                    return {}
                
                return state
        else:
            return {}
        
    # Write the current program state to a JSON file
    def write_state_file(self):
        if not self.program_state:
            self.program_state = {}
            
        state_file = os.path.join(os.path.expanduser("~"), ".config", "skemazz", "state.json")
        os.makedirs(os.path.dirname(state_file), exist_ok=True)
        with open(state_file, "w") as f:
            json.dump(self.program_state, f, indent=4)

    def handle_registration_button_click(self, button, layer_no, reg_no):
        print(f"Layer: {layer_no}, Registration: {reg_no}")
        if button.isChecked():
            if self.currently_checked and self.currently_checked != button:
                self.currently_checked.setChecked(False)

            # Turn on the layer corresponding to the button and turn off the others
            for i, btn in enumerate(self.layer_buttons):
                btn.setChecked(i == layer_no)
                layer = self.pcbLayerWidget.getLayer(i)
                if layer:
                    layer.entity.setEnabled(i == layer_no)

            self.currently_checked = button
            self.registration_layer_no = layer_no
            self.registration_mark_no = reg_no
            self.select_tool(ToolType.PLACE_REGISTRATION_MARK)
        else:
            self.currently_checked = None
            self.select_tool(ToolType.NONE)

    def adjust_tilt(self, slider_value):
        """Adjusts the tilt of the view based on a slider value from -20 to 20."""
        self.tilt = slider_value / 10
        self.pcbLayerWidget.setTilt(self.tilt)

    def set_tilt_slider(self, tilt):
        """Sets the tilt slider to a specific value."""
        self.tilt = tilt
        self.tilt_slider.setValue(int(round(tilt * 10)))

    def fit_image_in_view(self):
        """Fits the image within the view."""
        # Placeholder for fitting the image
        pass

    def resizeEvent(self, event):
        """Resizes the view and fits the image within it."""
        super().resizeEvent(event)
        self.fit_image_in_view()

    def toggle_pixmap_visibility(self, checked, label):
        """Toggles the visibility of a pixmap based on the checked state of a button."""
        index = self.layer_button_labels.index(label)
        layer = self.pcbLayerWidget.getLayer(index)
        if layer:
            layer.entity.setEnabled(checked)

    def handle_view_click(self, point):
        """Handle mouse clicks in the view area"""
        print(f"Clicked at: ({point.x()}, {point.y()})")
        if self.current_tool == ToolType.PLACE_REGISTRATION_MARK:
            print(f"Placing registration mark on layer {self.registration_layer_no}, mark {self.registration_mark_no}")
            self.show_registration_mark(self.registration_layer_no, self.registration_mark_no, point)
        else:
            print("Tool not implemented")

    def toggle_movement_tool(self, checked):
        """Toggle between the movement tool and the NONE tool."""
        if checked:
            self.select_tool(ToolType.MOVEMENT)
        else:
            self.select_tool(ToolType.NONE)

    def toggle_mark_pad_tool(self, checked):
        """Toggle between the mark pad tool and the NONE tool."""
        if checked:
            self.select_tool(ToolType.MARK_PAD)
        else:
            self.select_tool(ToolType.NONE)

    def toggle_outline_component_tool(self, checked):
        """Toggle between the outline component tool and the NONE tool."""
        if checked:
            self.select_tool(ToolType.OUTLINE_COMPONENT)
        else:
            self.select_tool(ToolType.NONE)

    def select_tool(self, tool):
        """Selects a tool for the application."""
        self.current_tool = tool
        if tool != ToolType.PLACE_REGISTRATION_MARK:
            for button in self.toggle_buttons:
                button.setChecked(False)

        if tool != ToolType.MOVEMENT:
            self.movement_action.setChecked(False)
        
        if tool != ToolType.MARK_PAD:
            self.mark_pad_action.setChecked(False)
        
        if tool != ToolType.OUTLINE_COMPONENT:
            self.outline_action.setChecked(False)

    def open_board(self):
        """Opens a folder containing the board images and data."""
        folder = QFileDialog.getExistingDirectory(self, "Open folder")
        if folder:
            self.program_state["pwd"] = folder
            self.write_state_file()
            self.load_board(folder)

    def load_board(self, folder):
        """Loads the board images and data from the specified folder."""
        # Delete any existing layers
        self.pcbLayerWidget.layers.clear()

        # Clear the board info
        self.set_tilt_slider(0)
        self.shape_detect_params = ShapeDetectParameters()
        for i in range(len(self.layer_names)):
            for j in range(2):
                self.remove_registration_mark(i, j)

        # Load the images
        for i, image_name in enumerate(self.image_names):
            # Check if the image exists at folder/scans_fixed/image_name
            image_path = os.path.join(folder, "scans_fixed", image_name)
            if not os.path.exists(image_path):
                # Check if the image exists at folder/scans_orig/image_name
                image_path = os.path.join(folder, "scans_orig", image_name)
                if not os.path.exists(image_path):
                    continue

            # Read the image into a QImage
            image = QImage(image_path)
            if image.isNull():
                raise ValueError("Unable to load the image: {}".format(image_path))
            
            # Mirror the image horizontally if it's the bottom layer
            if i == 2:
                image = image.mirrored(True, False)

            self.layer_images[i] = image

            # Colour the image
            coloured_image = self.colour_image(image, self.layer_colors[i])

            # Add layer to the PcbLayerWidget
            self.pcbLayerWidget.addLayer(coloured_image, z_order=i/100, transparency=1.0, color=self.layer_colors[i])

        # Load the board info
        board_info_path = os.path.join(folder, "board.json")
        if os.path.exists(board_info_path):
            with open(board_info_path, "r") as f:
                board_info = json.load(f)

                # Get the tilt, registration marks and shape detect parameters
                if "tilt" in board_info:
                    self.set_tilt_slider(board_info["tilt"])
                
                if "shape_detect_params" in board_info:
                    self.shape_detect_params = ShapeDetectParameters(**board_info["shape_detect_params"])

                if "registration_marks" in board_info:
                    registration_marks = board_info["registration_marks"]
                    for i, layer_name in enumerate(self.layer_names):
                        if layer_name in registration_marks:
                            for j, mark in enumerate(registration_marks[layer_name]):
                                if mark:
                                    center = QPointF(mark["x"], mark["y"])
                                    self.show_registration_mark(i, j, center)
       
        # Do the registration alignment
        self.align_registration_marks()

        # Set the window title to the folder name
        self.setWindowTitle(f"Skemazz - {os.path.basename(folder)}")
        self.board_folder = folder

    def handle_save_board(self):
        """Saves the board images and data to a folder."""
        self.save_board(self.board_folder)
        
        # Save the program state
        self.write_state_file()

    def colour_image(self, image, qt_color):
        # Ensure image is in the correct format to manipulate ARGB data directly
        image = image.convertToFormat(QImage.Format.Format_ARGB32)

        # Access the raw bits of the image
        ptr = image.bits()
        ptr.setsize(image.sizeInBytes())  # Use sizeInBytes() for PyQt6
        array = np.frombuffer(ptr, np.uint32).reshape((image.height(), image.width()))

        # Create masks based on the condition of being white
        white_mask = (array == 0xffffffff)  # ARGB for white
        not_white_mask = ~white_mask

        # Define new colors
        transparent = 0x00000000  # Fully transparent

        # Creating semi-transparent color from Qt.GlobalColor
        color = QColor(qt_color)
        color.setAlpha(127)
        semi_transparent_color = color.rgba()

        # Apply masks to set new colors
        array[white_mask] = transparent
        array[not_white_mask] = semi_transparent_color

        # Return image
        return image
        
    def save_board(self, folder):
        """Saves the board images and data to a folder."""
        # Create the tilt, shape detect parameters, and registration marks into a dictionary
        board_info = {
            "tilt": self.tilt,
            "shape_detect_params": vars(self.shape_detect_params),
            "registration_marks": {}
        }

        # Add the registration marks to the board info
        for i, layer_name in enumerate(self.layer_names):
            layer_marks = []
            for j in range(2):
                layer = self.pcbLayerWidget.getLayer(i)
                mark = layer.getRegistrationMark(j)
                if mark:
                    layer_marks.append(mark)
                else:
                    layer_marks.append(None)

            # transform list of [x,y] coords into a list of dicts with x and y keys
            board_info["registration_marks"][layer_name] = [dict(zip(["x", "y"], mark)) if mark else None for mark in layer_marks]

        # Save the board info
        board_info_path = os.path.join(folder, "board.json")
        with open(board_info_path, "w") as f:
            json.dump(board_info, f, indent=4)

        print(f"Saved to {board_info_path}, content: {board_info}")

    def show_registration_mark(self, layer_no, mark_no, center):
        """Adds a registration mark to the scene."""
        print("Adding registration mark", layer_no, mark_no, center)
        # try:
        layer = self.pcbLayerWidget.getLayer(layer_no)
        layer.setRegistrationMark(mark_no, (center.x(), center.y()))
        # except Exception as e:
        #     print(f"Error adding registration mark: {e}")
        #     pass

    def remove_registration_mark(self, layer_no, mark_no):
        """Removes a registration mark from the scene."""
        try:
            layer = self.pcbLayerWidget.getLayer(layer_no)
            layer.removeRegistrationMark(mark_no)
        except Exception as e:
            pass

    def align_registration_marks(self):
        """Aligns the registration marks of each of the layers so that the
           first marks on all layers are at the same position and the second
           marks on all layers are at the same position.
           Do this by applying pixmap.setPos(), pixmap.setScale() and pixmap.setRotation()
           to the layers"""
        # Check if any of the registration marks are missing
        first_marks = []
        second_marks = []
        for i in range(len(self.layer_names)):
            layer = self.pcbLayerWidget.getLayer(i)
            if not layer:
                return
            
            first_mark = layer.getRegistrationMark(0)
            second_mark = layer.getRegistrationMark(1)
            if not first_mark or not second_mark:
                return
            
            # Add the first and second registration marks to the lists
            first_marks.append(first_mark)
            second_marks.append(second_mark)
 
        # Calculate the distance between the first and second registration marks
        distances = [math.hypot(second_marks[i][0] - first_marks[i][0], second_marks[i][1] - first_marks[i][1]) for i in range(len(first_marks))]
 
        # Calculate the angles of rotation between each pair of first and second registration marks
        rotations = [np.arctan2(second_marks[i][1] - first_marks[i][1], second_marks[i][0] - first_marks[i][0]) for i in range(len(first_marks))]

        # Calculate the midpoints of each of the first and second registration marks
        midpoint_coords = [(first_marks[i][0] + second_marks[i][0]) / 2 for i in range(len(first_marks))], [(first_marks[i][1] + second_marks[i][1]) / 2 for i in range(len(first_marks))]
 
        # Determine the scaling factor for each layer
        apply_scale_factors = [distance / distances[0] for distance in distances]
 
        # Determine the rotation for each layer to align the angles of each layer with the first layer
        apply_rotations = [rotations[0] - rotation for rotation in rotations]
 
        # Apply the scaling and rotation to each layer
        for i, layer_name in enumerate(self.layer_names):
            # Translate the layer so that the mid point between registration marks is at (0, 0)
            layer = self.pcbLayerWidget.getLayer(i)
            layer.translateLayer((-midpoint_coords[i][0], -midpoint_coords[i][1]))

            if i != 0:
                # Apply the scaling and rotation to the layer's texture
                print(f"layer {layer_name}: scale: {apply_scale_factors[i]}, rotation: {rotations[i]}")
                layer.scaleLayer(apply_scale_factors[i], (0, 0))
                layer.rotateLayer(rotations[i], (0, 0))                

    def set_detection_parameters(self):
        """Set the detection parameters for component, pad, and trace detection."""
        dialog = ShapeDetectParametersDialog(self.shape_detect_params)
        
        if dialog.exec() == QDialog.DialogCode.Accepted:
            print("Parameters updated:")
            print(vars(self.shape_detect_params))
            #self.save_board(self.board_folder)
        else:
            print("Dialog canceled")

    def detect_components(self):
        """Detect components in the component layer."""
        # Placeholder for component detection
        pass

    def detect_pads(self):
        """Detect pads in the both pcb layers."""
        # Detect filled circles in the layer images
        for layer_name in self.layer_names:
            i = self.layer_names.index(layer_name)
            shape_detector = ShapeDetect(self.layer_images[i], self.shape_detect_params)
            detected_pads = shape_detector.detect_filled_circles()
            print(f"Detected pads in {layer_name}: {detected_pads}")

    def detect_traces(self):
        """Detect traces in the both pcb layers."""
        # Placeholder for trace detection
        for layer_name in self.layer_names:
            i = self.layer_names.index(layer_name)
            shape_detector = ShapeDetect(self.layer_images[i], self.shape_detect_params)
            detected_traces = shape_detector.detect_lines()
            print(f"Detected traces in {layer_name}: {detected_traces}")

app = QApplication(sys.argv)
ex = Skemazz()
sys.exit(app.exec())
