import numpy as np
from PyQt6.QtGui import QVector3D, QPainter, QColor
from PyQt6.Qt3DCore import QEntity, QTransform, QGeometry, QAttribute, QBuffer
from PyQt6.Qt3DExtras import QTextureMaterial, QPhongMaterial, QCuboidMesh
from PyQt6.Qt3DRender import QTexture2D, QPaintedTextureImage, QGeometryRenderer
from PyQt6.QtCore import Qt
from marker import MarkerType, Marker

class CustomPaintedTextureImage(QPaintedTextureImage):
    def __init__(self, image):
        super().__init__()
        self.image = image
        self.setSize(image.size())

    def paint(self, painter: QPainter):
        painter.drawImage(0, 0, self.image)

class PcbLayer:
    def __init__(self, rootEntity, texture, z_order=0, transparency=1.0, color=QColor(Qt.GlobalColor.green)):
        self.entity = QEntity(rootEntity)
        self.test_entity = QEntity(self.entity)
        self.texture = texture
        self.z_order = z_order
        self.transparency = transparency
        self.markers = []
        self.color = color
        self.registration_mark_size = 0.2
        self.pad_size = 11
        self.pin_size = 10
        self.plane_width = 32
        self.aspect_ratio = self.texture.width() / self.texture.height()
        self.plane_height = self.plane_width / self.aspect_ratio
        self.num_segments = 8
        # print("Plane width x height: ", self.plane_width, "x", self.plane_height, "aspect ratio: ", self.aspect_ratio, "texture width x height: ", self.texture.width(), "x", self.texture.height())

        self.geometry = QGeometry(self.entity)
        self.verticesBuffer = QBuffer()
        self.indicesBuffer = QBuffer()
        self.positionAttribute = QAttribute()
        self.texCoordAttribute = QAttribute()
        self.indexAttribute = QAttribute()
        self.planeRenderer = QGeometryRenderer()
        self.planeTransform = QTransform()
        self.planeMaterial = QTextureMaterial()
        
        self.entity.addComponent(self.planeRenderer)
        self.entity.addComponent(self.planeTransform)
        self.entity.addComponent(self.planeMaterial)

        self.createTexturedPlane()
        self.setTextures(self.texture)
        self.setTransparency(self.transparency)
        self.setZOrder(self.z_order)

        self.registration_marker_entities = {}
        self.registration_marker_transforms = {}
        self.registration_marker_coords = [None, None]
        self.createRegistrationMarkers()

    def createTexturedPlane(self):
        # Create the vertex coordinates and texture coordinates for the plane as a numpy 2d array of floats
        self.vertex_coords = np.zeros((self.num_segments + 1, self.num_segments + 1, 3), dtype=np.float64)
        self.tex_coords = np.zeros((self.num_segments + 1, self.num_segments + 1, 2), dtype=np.float64)

        # Fill out the vertex and texture coordinates
        for i in range(self.num_segments + 1):
            for j in range(self.num_segments + 1):
                x = -self.plane_width/2 + (self.plane_width / self.num_segments) * i
                y = self.plane_height/2 - (self.plane_height / self.num_segments) * j
                z = 0.0
                u = i / self.num_segments
                v = j / self.num_segments
                self.vertex_coords[i, j] = [x, y, z]
                self.tex_coords[i, j] = [u, v]

        self.updateVertexAndTextureCoordinates()

        # Create the indices for the plane
        indices = []
        for i in range(self.num_segments):
            for j in range(self.num_segments):
                bottom_left = (i + 1) * (self.num_segments + 1) + j
                bottom_right = bottom_left + 1
                top_left = i * (self.num_segments + 1) + j
                top_right = top_left + 1

                indices.extend([top_left, top_right, bottom_left, bottom_left, top_right, bottom_right])

        indices_array = np.array(indices, dtype=np.uint16)
        self.indicesBuffer.setData(indices_array.tobytes())

        # Create attributes
        self.positionAttribute.setName(QAttribute.defaultPositionAttributeName())
        self.positionAttribute.setVertexBaseType(QAttribute.VertexBaseType.Float)
        self.positionAttribute.setVertexSize(3)
        self.positionAttribute.setAttributeType(QAttribute.AttributeType.VertexAttribute)
        self.positionAttribute.setBuffer(self.verticesBuffer)
        self.positionAttribute.setByteStride(5 * 4)
        self.positionAttribute.setCount(self.vertexCount)

        self.texCoordAttribute.setName(QAttribute.defaultTextureCoordinateAttributeName())
        self.texCoordAttribute.setVertexBaseType(QAttribute.VertexBaseType.Float)
        self.texCoordAttribute.setVertexSize(2)
        self.texCoordAttribute.setAttributeType(QAttribute.AttributeType.VertexAttribute)
        self.texCoordAttribute.setBuffer(self.verticesBuffer)
        self.texCoordAttribute.setByteOffset(3 * 4)
        self.texCoordAttribute.setByteStride(5 * 4)
        self.texCoordAttribute.setCount(self.vertexCount)

        self.indexAttribute.setVertexBaseType(QAttribute.VertexBaseType.UnsignedShort)
        self.indexAttribute.setAttributeType(QAttribute.AttributeType.IndexAttribute)
        self.indexAttribute.setBuffer(self.indicesBuffer)
        self.indexAttribute.setCount(len(indices))

        self.geometry.addAttribute(self.positionAttribute)
        self.geometry.addAttribute(self.texCoordAttribute)
        self.geometry.addAttribute(self.indexAttribute)

        self.planeRenderer.setGeometry(self.geometry)
        self.planeRenderer.setPrimitiveType(QGeometryRenderer.PrimitiveType.Triangles)

    def updateVertexAndTextureCoordinates(self):
        """Update the vertex and texture coordinates of the plane."""
        vertices_array = np.zeros((self.num_segments + 1) * (self.num_segments + 1) * 5, dtype=np.float32)
        for i in range(self.num_segments + 1):
            for j in range(self.num_segments + 1):
                offset = (i * (self.num_segments + 1) + j) * 5
                vertices_array[offset: offset + 3] = self.vertex_coords[i, j]
                vertices_array[offset + 3: offset + 5] = self.tex_coords[i, j]

        self.verticesBuffer.setData(vertices_array.tobytes())
        self.vertexCount = len(vertices_array) // 5
    
    def setTextures(self, image):
        # Create a texture
        texture = QTexture2D()
        texture.setMinificationFilter(QTexture2D.Filter.Linear)
        texture.setMagnificationFilter(QTexture2D.Filter.Linear)
        
        # Create a custom texture image
        textureImage = CustomPaintedTextureImage(image)
        texture.setFormat(QTexture2D.TextureFormat.RGBA8_UNorm)  # Set appropriate format if needed

        # Add the image to the texture
        texture.addTextureImage(textureImage)

        # Set the material's texture
        self.planeMaterial.setTexture(texture)
        
    def setTransparency(self, transparency):
        self.transparency = transparency
        self.planeMaterial.setAlphaBlendingEnabled(True)
        # self.planeMaterial.setAlphaBlendingSourceFactor(QTextureMaterial.Blending.SourceAlpha)
        # self.planeMaterial.setAlphaBlendingDestinationFactor(QTextureMaterial.Blending.OneMinusSourceAlpha)
        # self.planeMaterial.setAlpha(transparency)

    def setZOrder(self, z_order):
        self.z_order = z_order
        self.planeTransform.setTranslation(QVector3D(0, 0, z_order))

    def getVertices(self):
        return np.frombuffer(self.verticesBuffer.data(), dtype=np.float32).reshape(-1, 5)

    def setVertices(self, vertices):
        self.verticesBuffer.setData(vertices.tobytes())

    def setTilt(self, tilt_angle_degrees):
        self.planeTransform.setRotationZ(tilt_angle_degrees)
    
    def addMarker(self, marker):
        """Add a marker to the layer."""
        self.markers.append(marker)
        self.drawMarker(marker)
    
    def showMarkers(self, marker_type, visible):
        """Show/hide markers of a specific type."""
        for marker in self.markers:
            if marker.marker_type == marker_type:
                marker.entity.setEnabled(visible)

    def drawMarker(self, marker):
        """Draw a marker on the layer.
           Markers are drawn differently depending on their type.
           Registration marks are drawn as diamonds with a cross through them, components as rectangles, pads as circles, and pins as crosses.
        """
        if marker.marker_type == MarkerType.REGISTRATION_MARK:
            return self.drawRegistrationMark(marker.x, marker.y, self.registration_mark_size, self.color)

        elif marker.marker_type == MarkerType.COMPONENT:
            return self.drawComponent(marker.x, marker.y, marker.xsize, marker.ysize, self.color)

        elif marker.marker_type == MarkerType.PAD:
            return self.drawPad(marker.x, marker.y, self.pad_size, self.color)

        elif marker.marker_type == MarkerType.PIN:
            return self.drawPin(marker.x, marker.y, self.pin_size, self.color)

    def drawRegistrationMark(self, x, y, size, color):
        """Draw a diamond shape with a cross through it at position (x, y) with the specified size."""
        half_size = size / 2

        # Define vertices for the diamond and cross
        z = self.z_order + 0.001
        vertices = [
            0, half_size, z,   0.0, -0.5,   # Top vertex
            -half_size, 0, z, -0.5,  0.0,   # Left vertex
            0, -half_size, z,  0.0,  0.5,   # Bottom vertex
            half_size, 0, z,   0.5,  0.0,   # Right vertex
            0, 0, z,           0.0,  0.0,   # Center vertex
        ]

        vertices_array = np.array(vertices, dtype=np.float32)
        vertices_buffer = QBuffer()
        vertices_buffer.setData(vertices_array.tobytes())

        # Define indices to draw the lines
        indices = [
            0, 1,  # Top to Left
            1, 2,  # Left to Bottom
            2, 3,  # Bottom to Right
            3, 0,  # Right to Top
            0, 2,  # Top to bottom
            1, 3   # Left to right
        ]
        indices_array = np.array(indices, dtype=np.uint16)
        indices_buffer = QBuffer()
        indices_buffer.setData(indices_array.tobytes())

        # Create attributes
        position_attribute = QAttribute()
        position_attribute.setName(QAttribute.defaultPositionAttributeName())
        position_attribute.setVertexBaseType(QAttribute.VertexBaseType.Float)
        position_attribute.setVertexSize(3)
        position_attribute.setAttributeType(QAttribute.AttributeType.VertexAttribute)
        position_attribute.setBuffer(vertices_buffer)
        position_attribute.setByteStride(5 * 4)      # Five 32 bit floats per vertex
        position_attribute.setCount(len(vertices) // 5)

        index_attribute = QAttribute()
        index_attribute.setVertexBaseType(QAttribute.VertexBaseType.UnsignedShort)
        index_attribute.setAttributeType(QAttribute.AttributeType.IndexAttribute)
        index_attribute.setBuffer(indices_buffer)
        index_attribute.setCount(len(indices))

        # Create geometry
        geometry = QGeometry(self.entity)
        geometry.addAttribute(position_attribute)
        geometry.addAttribute(index_attribute)

        # Create geometry renderer
        geometry_renderer = QGeometryRenderer()
        geometry_renderer.setGeometry(geometry)
        geometry_renderer.setPrimitiveType(QGeometryRenderer.PrimitiveType.Lines)

        # Create material
        material = QPhongMaterial()
        material.setAmbient(color)

        # Create a transform
        transform = QTransform()
        transform.setTranslation(QVector3D(x, y, 0))
        
        # Create entity
        marker_entity = QEntity(self.entity)
        marker_entity.addComponent(geometry_renderer)
        marker_entity.addComponent(material)
        marker_entity.addComponent(transform)

        return (marker_entity, transform)

    def drawComponent(self, x, y, width, height, color):
        """Draw a rectangular component at position (x, y) with the specified width and height."""
        # Define vertices for the rectangle
        half_width = width / 2
        half_height = height / 2
        vertices = [
            QVector3D(x - half_width, y - half_height, self.z_order),  # Bottom-left vertex
            QVector3D(x + half_width, y - half_height, self.z_order),  # Bottom-right vertex
            QVector3D(x + half_width, y + half_height, self.z_order),  # Top-right vertex
            QVector3D(x - half_width, y + half_height, self.z_order)   # Top-left vertex
        ]

        # Define indices to draw the rectangle using two triangles
        indices = [
            0, 1, 2,  # First triangle
            2, 3, 0   # Second triangle
        ]

        # Create buffers
        vertices_buffer = QBuffer()
        indices_buffer = QBuffer()

        # Set buffer data
        vertices_array = np.array([vertex.toTuple() for vertex in vertices], dtype=np.float32).flatten()
        vertices_buffer.setData(vertices_array.tobytes())
        indices_array = np.array(indices, dtype=np.uint16)
        indices_buffer.setData(indices_array.tobytes())

        # Create attributes
        position_attribute = QAttribute()
        position_attribute.setName(QAttribute.defaultPositionAttributeName())
        position_attribute.setVertexBaseType(QAttribute.VertexBaseType.Float)
        position_attribute.setVertexSize(3)
        position_attribute.setAttributeType(QAttribute.AttributeType.VertexAttribute)
        position_attribute.setBuffer(vertices_buffer)
        position_attribute.setByteStride(3 * 4)
        position_attribute.setCount(len(vertices))

        index_attribute = QAttribute()
        index_attribute.setVertexBaseType(QAttribute.VertexBaseType.UnsignedShort)
        index_attribute.setAttributeType(QAttribute.AttributeType.IndexAttribute)
        index_attribute.setBuffer(indices_buffer)
        index_attribute.setCount(len(indices))

        # Create geometry
        geometry = QGeometry()
        geometry.addAttribute(position_attribute)
        geometry.addAttribute(index_attribute)

        # Create geometry renderer
        geometry_renderer = QGeometryRenderer()
        geometry_renderer.setGeometry(geometry)
        geometry_renderer.setPrimitiveType(QGeometryRenderer.PrimitiveType.Triangles)

        # Create material
        material = QPhongMaterial()
        material.setAmbient(color)  # Set component color to green

        # Create entity
        component_entity = QEntity(self.entity)
        component_entity.addComponent(geometry_renderer)
        component_entity.addComponent(material)

    def drawPad(self, x, y, radius, color):
        """Draw a filled circle (pad) at position (x, y) with the specified radius and color."""
        num_segments = 32  # Number of segments to approximate the circle

        # Define vertices for the circle
        vertices = [QVector3D(x, y, self.z_order)]  # Center vertex
        for i in range(num_segments + 1):
            angle = 2.0 * np.pi * i / num_segments
            vx = x + radius * np.cos(angle)
            vy = y + radius * np.sin(angle)
            vertices.append(QVector3D(vx, vy, self.z_order))

        # Define indices to draw the triangles that make up the circle
        indices = []
        for i in range(1, num_segments + 1):
            indices.extend([0, i, i + 1])

        # Create buffers
        vertices_buffer = QBuffer()
        indices_buffer = QBuffer()

        # Set buffer data
        vertices_array = np.array([vertex.toTuple() for vertex in vertices], dtype=np.float32).flatten()
        vertices_buffer.setData(vertices_array.tobytes())
        indices_array = np.array(indices, dtype=np.uint16)
        indices_buffer.setData(indices_array.tobytes())

        # Create attributes
        position_attribute = QAttribute()
        position_attribute.setName(QAttribute.defaultPositionAttributeName())
        position_attribute.setVertexBaseType(QAttribute.VertexBaseType.Float)
        position_attribute.setVertexSize(3)
        position_attribute.setAttributeType(QAttribute.AttributeType.VertexAttribute)
        position_attribute.setBuffer(vertices_buffer)
        position_attribute.setByteStride(3 * 4)
        position_attribute.setCount(len(vertices))

        index_attribute = QAttribute()
        index_attribute.setVertexBaseType(QAttribute.VertexBaseType.UnsignedShort)
        index_attribute.setAttributeType(QAttribute.AttributeType.IndexAttribute)
        index_attribute.setBuffer(indices_buffer)
        index_attribute.setCount(len(indices))

        # Create geometry
        geometry = QGeometry()
        geometry.addAttribute(position_attribute)
        geometry.addAttribute(index_attribute)

        # Create geometry renderer
        geometry_renderer = QGeometryRenderer()
        geometry_renderer.setGeometry(geometry)
        geometry_renderer.setPrimitiveType(QGeometryRenderer.PrimitiveType.Triangles)

        # Create material
        material = QPhongMaterial()
        material.setAmbient(color)  # Set pad color

        # Create entity
        pad_entity = QEntity(self.entity)
        pad_entity.addComponent(geometry_renderer)
        pad_entity.addComponent(material)

    def drawPin(self, x, y, size, color):
        """Draw a cross (pin) at position (x, y) with the specified size and color."""
        half_size = size / 2

        # Define vertices for the cross (vertical and horizontal bars)
        vertices = [
            QVector3D(x - half_size, y, self.z_order),  # Left end of horizontal bar
            QVector3D(x + half_size, y, self.z_order),  # Right end of horizontal bar
            QVector3D(x, y + half_size, self.z_order),  # Top end of vertical bar
            QVector3D(x, y - half_size, self.z_order)   # Bottom end of vertical bar
        ]

        # Define indices to draw the lines for the cross
        indices = [
            0, 1,  # Horizontal bar
            2, 3   # Vertical bar
        ]

        # Create buffers
        vertices_buffer = QBuffer()
        indices_buffer = QBuffer()

        # Set buffer data
        vertices_array = np.array([vertex.toTuple() for vertex in vertices], dtype=np.float32).flatten()
        vertices_buffer.setData(vertices_array.tobytes())
        indices_array = np.array(indices, dtype=np.uint16)
        indices_buffer.setData(indices_array.tobytes())

        # Create attributes
        position_attribute = QAttribute()
        position_attribute.setName(QAttribute.defaultPositionAttributeName())
        position_attribute.setVertexBaseType(QAttribute.VertexBaseType.Float)
        position_attribute.setVertexSize(3)
        position_attribute.setAttributeType(QAttribute.AttributeType.VertexAttribute)
        position_attribute.setBuffer(vertices_buffer)
        position_attribute.setByteStride(3 * 4)
        position_attribute.setCount(len(vertices))

        index_attribute = QAttribute()
        index_attribute.setVertexBaseType(QAttribute.VertexBaseType.UnsignedShort)
        index_attribute.setAttributeType(QAttribute.AttributeType.IndexAttribute)
        index_attribute.setBuffer(indices_buffer)
        index_attribute.setCount(len(indices))

        # Create geometry
        geometry = QGeometry()
        geometry.addAttribute(position_attribute)
        geometry.addAttribute(index_attribute)

        # Create geometry renderer
        geometry_renderer = QGeometryRenderer()
        geometry_renderer.setGeometry(geometry)
        geometry_renderer.setPrimitiveType(QGeometryRenderer.PrimitiveType.Lines)

        # Create material
        material = QPhongMaterial()
        material.setAmbient(color)  # Set pin color

        # Create entity
        pin_entity = QEntity(self.entity)
        pin_entity.addComponent(geometry_renderer)
        pin_entity.addComponent(material)

    def createRegistrationMarkers(self):
        for i in range(2):
            marker_entity, transform = self.drawRegistrationMark(0, 0, self.registration_mark_size, self.color)
            marker_entity.setEnabled(False)  # Default to not visible
            self.registration_marker_entities[i] = marker_entity
            self.registration_marker_transforms[i] = transform
            self.registration_marker_coords[i] = None

    def setRegistrationMark(self, id, coord, visible=True):
        print(f"Setting registration mark {id} at ({coord[0]}, {coord[1]})")
        if id in self.registration_marker_entities:
            self.registration_marker_transforms[id].setTranslation(QVector3D(coord[0], coord[1], 0))
            self.registration_marker_entities[id].setEnabled(visible)
            self.registration_marker_coords[id] = coord
            print(f"Registration mark {id} set at ({coord[0]}, {coord[1]})")
        else:
            raise ValueError(f"Invalid marker ID: {id}")

    def removeRegistrationMark(self, id, visible=False):
        print(f"Removing registration mark {id}")
        if id in self.registration_marker_entities:
            marker_entity = self.registration_markers[id]
            marker_entity.setEnabled(visible)
            self.registration_marker_coords[id] = None
        else:
            raise ValueError(f"Invalid marker ID: {id}")

    def getRegistrationMark(self, id):
        print(f"Getting registration mark {id} coordinates.")
        if id in self.registration_marker_entities:
            print(f"Registration mark found: {self.registration_marker_coords[id]}")
            return self.registration_marker_coords[id]
        else:
            print("Registration mark not found.")
            return None
        
    def createTransformationMatrix(self, scale_factor, rotation_in_radians, x_translation, y_translation):
        """Create a 3x3 transformation matrix using the specified scale factor, rotation in radians, and translation."""
        transformation_matrix = np.array([
            [scale_factor * np.cos(rotation_in_radians), -scale_factor * np.sin(rotation_in_radians), x_translation],
            [scale_factor * np.sin(rotation_in_radians),  scale_factor * np.cos(rotation_in_radians), y_translation],
            [0, 0, 1]
        ])
        return transformation_matrix
    
    def createRotateAroundCenterMatrix(self, rotation_in_radians, center):
        """Create a 3x3 transformation matrix to rotate around the specified center."""
        # Translate the center to the origin, rotate, and then translate back
        center_matrix = np.array([
            [1, 0, -center[0]],
            [0, 1, -center[1]],
            [0, 0, 1]
        ])
        rotation_matrix = np.array([
            [np.cos(rotation_in_radians), -np.sin(rotation_in_radians), 0],
            [np.sin(rotation_in_radians),  np.cos(rotation_in_radians), 0],
            [0, 0, 1]
        ])
        inverse_center_matrix = np.array([
            [1, 0, center[0]],
            [0, 1, center[1]],
            [0, 0, 1]
        ])
        transformation_matrix = inverse_center_matrix @ rotation_matrix @ center_matrix
        return transformation_matrix
    
    def createScaleAroundCenterMatrix(self, scale_factor, center):
        """Create a 3x3 transformation matrix to scale around the specified center."""
        # Translate the center to the origin, scale, and then translate back
        center_matrix = np.array([
            [1, 0, -center[0]],
            [0, 1, -center[1]],
            [0, 0, 1]
        ])
        scale_matrix = np.array([
            [scale_factor, 0, 0],
            [0, scale_factor, 0],
            [0, 0, 1]
        ])
        inverse_center_matrix = np.array([
            [1, 0, center[0]],
            [0, 1, center[1]],
            [0, 0, 1]
        ])
        transformation_matrix = inverse_center_matrix @ scale_matrix @ center_matrix
        return transformation_matrix
        
    def remapCoordinateByMatrix(self, transformation_matrix, coord):
        """Remap the coordinate (x, y) using the specified scale factor and rotation in radians."""
        # Apply the transformation matrix to the coordinate
        new_x = transformation_matrix[0, 0] * coord[0] + transformation_matrix[0, 1] * coord[1] + transformation_matrix[0, 2]
        new_y = transformation_matrix[1, 0] * coord[0] + transformation_matrix[1, 1] * coord[1] + transformation_matrix[1, 2]

        return (new_x, new_y)
    
    def remapCoordinate(self, scale_factor, rotation_in_radians, x_translation, y_translation, coord):
        """Remap the coordinate (x, y) using the specified scale factor, rotation in radians, and translation."""
        # Create a single matrix to apply to the coordinate which combines all the transformations
        transformation_matrix = self.createTransformationMatrix(scale_factor, rotation_in_radians, x_translation, y_translation)
        
        # Apply the transformation matrix to the coordinate
        return self.remapCoordinateByMatrix(transformation_matrix, coord)
    
    def remapTexture(self, scale_factor, rotation_in_radians, x_translation, y_translation):
        """Remap the texture coordinates of the layer.
           The scale factor is a single linear value.
           The rotation is in radians.
           The translation is a 2D vector.
           Texture coordinates are in the self.verticesBuffer 
        """
        # Create a single matrix to apply to the texture coordinates which combines all the transformations
        transformation_matrix = self.createTransformationMatrix(scale_factor, rotation_in_radians, x_translation, y_translation)
        
        # Loop through the vertices and update the texture coordinates
        for i in range(self.num_segments + 1):
            for j in range(self.num_segments + 1):
                u = self.tex_coords[i, j, 0]
                v = self.tex_coords[i, j, 1]
                # Apply the transformation matrix to the texture coordinates
                new_u, new_v = self.remapCoordinateByMatrix(transformation_matrix, (u, v))
                self.tex_coords[i, j, 0] = new_u
                self.tex_coords[i, j, 1] = new_v

        # Update the texture coordinates in the vertices buffer
        self.updateVertexAndTextureCoordinates()
        self.texCoordAttribute.setBuffer(self.verticesBuffer)

    def translateTexture(self, x_translation, y_translation):
        """Translate the texture coordinates of the layer by the specified amount."""
        # Loop through the vertices and update the texture coordinates
        for i in range(self.num_segments + 1):
            for j in range(self.num_segments + 1):
                u = self.tex_coords[i, j, 0]
                v = self.tex_coords[i, j, 1]
                new_u = u + x_translation
                new_v = v + y_translation
                self.tex_coords[i, j] = (new_u, new_v)

        # Update the texture coordinates in the vertices buffer
        self.updateVertexAndTextureCoordinates()
        self.texCoordAttribute.setBuffer(self.verticesBuffer)

    def scaleTexture(self, scale_factor, center):
        """Scale the texture coordinates of the layer by the specified scale factor around the specified center."""
        # Create a single matrix to apply to the coordinate which combines all the transformations
        transformation_matrix = self.createScaleAroundCenterMatrix(scale_factor, center)

        # Loop through the vertices and update the texture coordinates
        for i in range(self.num_segments + 1):
            for j in range(self.num_segments + 1):
                u = self.tex_coords[i, j, 0]
                v = self.tex_coords[i, j, 1]
                new_u, new_v = self.remapCoordinateByMatrix(transformation_matrix, (u, v))
                self.tex_coords[i, j] = (new_u, new_v)

        # Update the texture coordinates in the vertices buffer
        self.updateVertexAndTextureCoordinates()
        self.texCoordAttribute.setBuffer(self.verticesBuffer)
    
    def rotateTexture(self, rotation_in_radians, center):
        """Rotate the texture coordinates of the layer by the specified rotation in radians around the specified center."""
        # Create a single matrix to apply to the coordinate which combines all the transformations
        transformation_matrix = self.createRotateAroundCenterMatrix(rotation_in_radians, center)

        # Loop through the vertices and update the texture coordinates
        for i in range(self.num_segments + 1):
            for j in range(self.num_segments + 1):
                u = self.tex_coords[i, j, 0]
                v = self.tex_coords[i, j, 1]
                new_u, new_v = self.remapCoordinateByMatrix(transformation_matrix, (u, v))
                self.tex_coords[i, j] = (new_u, new_v)

        # Update the texture coordinates in the vertices buffer
        self.updateVertexAndTextureCoordinates()
        self.texCoordAttribute.setBuffer(self.verticesBuffer)

    def translateLayer(self, offset):
        """Translate the layer by the specified offset."""
        # Translate the texture coordinates in the opposite direction of the offset
        texture_offset = self.convertWorldCoordToTextureCoord(offset)
        self.translateTexture(-texture_offset[0], -texture_offset[1])

        # Translate the registration markers in the positive direction of the offset
        for i in range(2):
            if self.registration_marker_coords[i] is not None:
                new_coord = (self.registration_marker_coords[i][0] + offset[0], self.registration_marker_coords[i][1] + offset[1])
                self.setRegistrationMark(i, new_coord)

    def scaleLayer(self, scale_factor, center):
        """Scale the layer by the specified scale factor around the specified center."""
        # Scale the texture coordinates around the center
        texture_center = self.convertWorldCoordToTextureCoord(center)
        self.remapTexture(scale_factor, 0, texture_center[0], texture_center[1])

        # Scale the registration markers around the center
        for i in range(2):
            if self.registration_marker_coords[i] is not None:
                new_coord = self.remapCoordinate(scale_factor, 0, texture_center[0], texture_center[1], self.registration_marker_coords[i])
                self.setRegistrationMark(i, new_coord)

    def rotateLayer(self, rotation_in_radians, center):
        """Rotate the layer by the specified rotation in radians around the specified center."""
        # Rotate the texture coordinates around the center
        texture_center = self.convertWorldCoordToTextureCoord(center)
        self.remapTexture(1, rotation_in_radians, texture_center[0], texture_center[1])

        # Rotate the registration markers around the center
        for i in range(2):
            if self.registration_marker_coords[i] is not None:
                new_coord = self.remapCoordinate(1, rotation_in_radians, texture_center[0], texture_center[1], self.registration_marker_coords[i])
                self.setRegistrationMark(i, new_coord)
    
    def convertWorldCoordToTextureCoord(self, world_coord):
        """Convert a world coordinate to a texture coordinate."""
        # Get the dimensions of the plane
        plane_width = self.plane_width
        plane_height = self.plane_height

        # If the coordinate is outside the plane, return None
        if world_coord[0] < -plane_width / 2 or world_coord[0] > plane_width / 2 or world_coord[1] < -plane_height / 2 or world_coord[1] > plane_height / 2:
            return None

        # Find the two closest x indices in the self.vertex_coords surrounding the given world coordinate
        x_index = 0
        for i in range(self.num_segments + 1):
            if self.vertex_coords[i, 0, 0] > world_coord[0]:
                x_index = i
                break

        # Same for y
        y_index = 0
        for j in range(self.num_segments + 1):
            if self.vertex_coords[0, j, 1] < world_coord[1]:
                y_index = j
                break
        
        # Get the texture coordinates of the four vertices surrounding the world coordinate
        u0 = self.tex_coords[x_index, y_index, 0]
        v0 = self.tex_coords[x_index, y_index, 1]
        u1 = self.tex_coords[x_index + 1, y_index, 0]
        v1 = self.tex_coords[x_index + 1, y_index, 1]
        u2 = self.tex_coords[x_index, y_index + 1, 0]
        v2 = self.tex_coords[x_index, y_index + 1, 1]
        u3 = self.tex_coords[x_index + 1, y_index + 1, 0]
        v3 = self.tex_coords[x_index + 1, y_index + 1, 1]

        # How close is the world coordinate to the four vertices
        dist_x1_x0 = self.vertex_coords[x_index + 1, y_index, 0] - self.vertex_coords[x_index, y_index, 0]
        closeness_x1 = (world_coord[0] - self.vertex_coords[x_index, y_index, 0]) / dist_x1_x0
        closeness_x0 = 1 - closeness_x1
        dist_y1_y0 = self.vertex_coords[x_index, y_index + 1, 1] - self.vertex_coords[x_index, y_index, 1]
        closeness_y1 = (self.vertex_coords[x_index, y_index + 1, 1] - world_coord[1]) / dist_y1_y0
        closeness_y0 = 1 - closeness_y1
        
        # Interpolate the texture coordinates to find the texture coordinate of the world coordinate
        u = closeness_x0 * (closeness_y0 * u0 + closeness_y1 * u2) + closeness_x1 * (closeness_y0 * u1 + closeness_y1 * u3)
        v = closeness_x0 * (closeness_y0 * v0 + closeness_y1 * v2) + closeness_x1 * (closeness_y0 * v1 + closeness_y1 * v3)

        return (u, v)

    def convertTextureCoordToWorldCoord(self, texture_coord):
        """Convert a texture coordinate to a world coordinate."""
        # Loop through all the texture coordinates to find the closest match
        closest_x = 0
        closest_y = 0
        closest_dist = float('inf')
        for i in range(self.num_segments + 1):
            for j in range(self.num_segments + 1):
                u = self.tex_coords[i, j, 0]
                v = self.tex_coords[i, j, 1]
                dist = np.sqrt((u - texture_coord[0]) ** 2 + (v - texture_coord[1]) ** 2)
                if dist < closest_dist:
                    closest_dist = dist
                    closest_x = i
                    closest_y = j
        
        # Examine the four surrounding squares to see which one the texture coordinate is in
        world_x = 0
        world_y = 0
        if self.isTextureCoordInsideQuadrilateral(texture_coord, closest_x, closest_y):
            return self.interpolateWorldCoord(texture_coord, closest_x, closest_y)
        elif self.isTextureCoordInsideQuadrilateral(texture_coord, closest_x - 1, closest_y):
            return self.interpolateWorldCoord(texture_coord, closest_x - 1, closest_y)
        elif self.isTextureCoordInsideQuadrilateral(texture_coord, closest_x, closest_y - 1):
            return self.interpolateWorldCoord(texture_coord, closest_x, closest_y - 1)
        elif self.isTextureCoordInsideQuadrilateral(texture_coord, closest_x - 1, closest_y - 1):
            return self.interpolateWorldCoord(texture_coord, closest_x - 1, closest_y - 1)
        else:
            return None
    
    def isTextureCoordInsideQuadrilateral(self, texture_coord, x_index_from, y_index_from):
        """Check if a texture coordinate is inside a quadrilateral defined by the four corners.
           Note that the quadrilateral is assumed to be convex and the vertices must be 
           given in clockwise order."""
        x_index = x_index_from
        y_index = y_index_from
        u0 = self.tex_coords[x_index, y_index, 0]
        v0 = self.tex_coords[x_index, y_index, 1]
        u1 = self.tex_coords[x_index + 1, y_index, 0]
        v1 = self.tex_coords[x_index + 1, y_index, 1]
        u2 = self.tex_coords[x_index + 1, y_index + 1, 0]
        v2 = self.tex_coords[x_index + 1, y_index + 1, 1]
        u3 = self.tex_coords[x_index, y_index + 1, 0]
        v3 = self.tex_coords[x_index, y_index + 1, 1]
        
        return self.isCoordInsideQuadrilateral(texture_coord[0], texture_coord[1], u0, v0, u1, v1, u2, v2, u3, v3)
    
    def isCoordInsideQuadrilateral(self, u, v, u0, v0, u1, v1, u2, v2, u3, v3):
        """Check if a coordinate is inside a quadrilateral defined by the four corners.
           Note that the quadrilateral is assumed to be convex and the vertices must be 
           given in clockwise order."""
        def is_point_left_of_line(px, py, x0, y0, x1, y1):
            # Check if the point (px, py) is to the left of the line from (x0, y0) to (x1, y1)
            return (x1 - x0) * (py - y0) - (y1 - y0) * (px - x0) > 0
        
        # Check if the point is to the left of all edges of the quadrilateral
        is_left_of_all_edges = (
            is_point_left_of_line(u, v, u0, v0, u1, v1) and
            is_point_left_of_line(u, v, u1, v1, u2, v2) and
            is_point_left_of_line(u, v, u2, v2, u3, v3) and
            is_point_left_of_line(u, v, u3, v3, u0, v0)
        )
        
        return is_left_of_all_edges

    def interpolateWorldCoord(self, texture_coord, x_index_from, y_index_from):
        """Interpolate the world coordinate from the texture coordinate and the four surrounding texture coordinates."""
        xi = x_index_from
        yi = y_index_from
        t0x = self.tex_coords[xi, yi, 0]
        t0y = self.tex_coords[xi, yi, 1]
        t1x = self.tex_coords[xi + 1, yi, 0]
        t1y = self.tex_coords[xi + 1, yi, 1]
        t2x = self.tex_coords[xi, yi + 1, 0]
        t2y = self.tex_coords[xi, yi + 1, 1]
        w0x = self.vertex_coords[xi, yi, 0]
        w0y = self.vertex_coords[xi, yi, 1]
        w1x = self.vertex_coords[xi + 1, yi, 0]
        w1y = self.vertex_coords[xi + 1, yi, 1]
        w2x = self.vertex_coords[xi, yi + 1, 0]
        w2y = self.vertex_coords[xi, yi + 1, 1]
        w3x = self.vertex_coords[xi + 1, yi + 1, 0]
        w3y = self.vertex_coords[xi + 1, yi + 1, 1]

        def find_weights(t_coord):
            xt, yt = t_coord

            denom = (t1x - t0x) * (t2y - t0y) - (t2x - t0x) * (t1y - t0y)
            if denom == 0:
                return None

            a = ((t1x - xt) * (t2y - yt) - (t2x - xt) * (t1y - yt)) / denom
            b = ((t2x - xt) * (t0y - yt) - (t0x - xt) * (t2y - yt)) / denom
            c = ((t0x - xt) * (t1y - yt) - (t1x - xt) * (t0y - yt)) / denom

            return a, b, c

        weights = find_weights(texture_coord)
        if weights is None:
            return None
        
        a, b, c = weights

        xw = a * w0x + b * w1x + c * w2x + (1 - a - b - c) * w3x
        yw = a * w0y + b * w1y + c * w2y + (1 - a - b - c) * w3y

        return (xw, yw)