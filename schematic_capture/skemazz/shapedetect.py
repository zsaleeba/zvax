import cv2
import numpy as np
import math
from PyQt6.QtGui import QImage

class ShapeDetectParameters:
    def __init__(self, circle_diameter=11, cross_length=15, dp=1.2, minDist=15, param1=50, param2=30, 
                 minRadius=4, maxRadius=7, line_threshold=80, min_line_length=10, max_line_gap=10):
        """
        Initialize the shape detection parameters.
        
        Parameters:
        - circle_diameter: int
          Default diameter of the circles to be detected. Used to set minRadius and maxRadius.
          
        - cross_length: int
          Length of each line in the cross mark. Crosses are expected to have lines of this length.
          
        - dp: float
          Inverse ratio of the accumulator resolution to the image resolution in the Hough Circle Transform.
          A dp of 1.2 means the accumulator has a resolution of 1.2 times the image resolution.
          
        - minDist: int
          Minimum distance between the centers of the detected circles. This helps avoid multiple nearby circles.
          
        - param1: int
          Higher threshold for the internal Canny edge detector used in the Hough Circle Transform.
          
        - param2: int
          Accumulator threshold for the circle centers in the Hough Circle Transform. The smaller it is, 
          the more false circles may be detected.
          
        - minRadius: int
          Minimum radius of the circles to be detected. This is calculated based on circle_diameter.
          
        - maxRadius: int
          Maximum radius of the circles to be detected. This is calculated based on circle_diameter.
          
        - line_threshold: int
          Threshold for the Hough Line Transform to decide if a detected line is significant enough.
          
        - min_line_length: int
          Minimum length of a line to be detected by the Hough Line Transform. Shorter lines will be discarded.
          
        - max_line_gap: int
          Maximum allowed gap between line segments to treat them as a single line in the Hough Line Transform.
        """
        self.circle_diameter = circle_diameter
        self.cross_length = cross_length
        self.dp = dp
        self.minDist = minDist
        self.param1 = param1
        self.param2 = param2
        self.minRadius = minRadius
        self.maxRadius = maxRadius
        self.line_threshold = line_threshold
        self.min_line_length = min_line_length
        self.max_line_gap = max_line_gap

        # Ensure minRadius and maxRadius are set based on circle_diameter if not explicitly provided
        if minRadius == 4 and maxRadius == 7:  # Default values are used, so adjust based on circle_diameter
            self.minRadius = int(math.floor(circle_diameter / 2 * 0.8))
            self.maxRadius = int(math.ceil(circle_diameter / 2 * 1.2))

class ShapeDetect:
    def __init__(self, qimage, params):
        """
        Initialize the ShapeDetect object with a QImage and ShapeDetectParameters.
        
        Parameters:
        - qimage: QImage
          The image to be processed for shape detection.
          
        - params: ShapeDetectParameters
          The parameters for shape detection.
        """
        self.qimage = qimage
        self.params = params
        self.img = self.qimage_to_mat(qimage)
        
    def qimage_to_mat(self, image):
        """Convert QImage to OpenCV mat format."""
        image = image.convertToFormat(QImage.Format.Format_RGB32)
        width = image.width()
        height = image.height()
        ptr = image.bits()
        ptr.setsize(image.byteCount())
        arr = np.array(ptr).reshape(height, width, 4)  # Copies the data
        return cv2.cvtColor(arr, cv2.COLOR_BGRA2GRAY)

    def detect_filled_circles(self):
        """Detect filled circles in the provided QImage."""
        circles = cv2.HoughCircles(self.img, cv2.HOUGH_GRADIENT, dp=self.params.dp, minDist=self.params.minDist,
                                   param1=self.params.param1, param2=self.params.param2,
                                   minRadius=self.params.minRadius, maxRadius=self.params.maxRadius)
        detected_filled_circles = []
        if circles is not None:
            circles = np.round(circles[0, :]).astype("int")
            for (x, y, r) in circles:
                if self.is_filled_circle(x, y, r):
                    detected_filled_circles.append((x, y, r))
        return detected_filled_circles

    def is_filled_circle(self, x, y, r):
        """Check if a circle is filled by analyzing the pixel intensity inside the circle."""
        mask = np.zeros_like(self.img)
        cv2.circle(mask, (x, y), r, 255, thickness=-1)
        mean_val = cv2.mean(self.img, mask=mask)[0]
        return mean_val > 127  # Assuming a binary image, this threshold can be adjusted

    def detect_lines(self):
        """Detect lines and return a list of connected line segments."""
        edges = cv2.Canny(self.img, 50, 150, apertureSize=3)
        lines = cv2.HoughLinesP(edges, 1, np.pi/180, threshold=self.params.line_threshold, 
                                minLineLength=self.params.min_line_length, maxLineGap=self.params.max_line_gap)
        detected_lines = []
        if lines is not None:
            for line in lines:
                x1, y1, x2, y2 = line[0]
                detected_lines.append([(x1, y1), (x2, y2)])
        return detected_lines

    def detect_cross_marks(self):
        """Detect cross marks in the provided QImage."""
        detected_crosses = []
        lines = self.detect_lines()
        for line1 in lines:
            for line2 in lines:
                if line1 != line2:
                    if self.is_cross(line1, line2):
                        detected_crosses.append((line1, line2))
        return detected_crosses

    def detect_cross_marks_with_circles(self):
        """Detect cross marks with circles around them in the provided QImage."""
        crosses = self.detect_cross_marks()
        detected_crosses_with_circles = []
        circles = self.detect_filled_circles()
        for cross in crosses:
            for circle in circles:
                if self.is_circle_around_cross(cross, circle):
                    detected_crosses_with_circles.append((cross, circle))
        return detected_crosses_with_circles

    def is_cross(self, line1, line2):
        """Check if two lines form a cross."""
        (x1, y1), (x2, y2) = line1
        (x3, y3), (x4, y4) = line2
        return (x1 == x2 and y3 == y4 and abs(y1 - y2) == self.params.cross_length and 
                abs(x3 - x4) == self.params.cross_length and abs(x1 - x3) < self.params.cross_length and 
                abs(y1 - y3) < self.params.cross_length)

    def is_circle_around_cross(self, cross, circle):
        """Check if a circle surrounds a cross mark."""
        (line1, line2) = cross
        (cx, cy, cr) = circle
        (x1, y1), (x2, y2) = line1
        return (abs(cx - x1) <= cr and abs(cy - y1) <= cr and abs(cx - x2) <= cr and abs(cy - y2) <= cr)
    