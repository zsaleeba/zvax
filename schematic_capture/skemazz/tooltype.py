from enum import Enum

class ToolType(Enum):
    NONE = 0
    PLACE_REGISTRATION_MARK = 1
    MOVEMENT = 2
    MARK_PAD = 3
    OUTLINE_COMPONENT = 4
    DRAW_PCB_TRACE = 5
