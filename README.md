# zvax


# VAX-11/780 clone
ZVAX is a clone of the DEC VAX-11/780 computer in a single FPGA. In the early 1980s a VAX took up a few refrigerator-sized cabinets and now with this project you can have one in a single small chip.

This is a real VAX, not a simulation. It's a gate-for-gate replica of the original VAX-11/780 CPU, with minimal changes to make it work with modern technology.

# The VAX
The VAX-11/780 is the machine which pioneered many aspects of modern processors, programming languages and virtual memory design. It still has a huge influence on processors and operating systems today.

## Authors and acknowledgment
This is currently a one-man project by zik@zikzak.net

## License
This project is BSD licensed.

## Project status
The project is in a very early pre-release stage. It doesn't do anything yet.
