/**
 * @module combined_cache
 * @brief Combined Instruction/Data Cache with Arbitration
 *
 * This module represents a combined instruction and data cache that
 * mediates access between an instruction unit and an execution unit.
 * The arbitration is handled by an arbiter module which ensures that 
 * only one unit can access the cache at a given time, based on a request
 * and grant mechanism.
 *
 * The cache itself is a 2-way set associative cache with a write-through
 * policy and random replacement. The combined cache interface ensures
 * proper communication between the cache and the two units, managing
 * read and write operations, address routing, and data output.
 *
 * Parameters:
 *  - None
 *
 * Ports:
 *  - clk: Input clock signal
 *  - reset: Input reset signal
 *  - req_instr: Request signal from the instruction unit
 *  - req_exec: Request signal from the execution unit
 *  - addr_instr: Address signal from the instruction unit
 *  - addr_exec: Address signal from the execution unit
 *  - data_exec: Data input signal from the execution unit (for writes)
 *  - we_exec: Write enable signal from the execution unit
 *  - data_out_instr: Data output signal to the instruction unit
 *  - data_out_exec: Data output signal to the execution unit
 *  - grant_instr: Grant signal to the instruction unit, indicating access to the cache
 *  - grant_exec: Grant signal to the execution unit, indicating access to the cache
 *  - hit_instr: Cache hit signal for the instruction unit
 *  - hit_exec: Cache hit signal for the execution unit
 *
 * The combined cache module comprises the following components:
 * 1. Arbiter: Manages access requests from both the instruction and execution units.
 *    It grants access based on a defined policy, ensuring that only one unit accesses
 *    the cache at any given time.
 * 2. Cache Interface: Connects to the arbiter and routes the appropriate address, data,
 *    and control signals to the cache based on the grant signals from the arbiter.
 * 3. Cache Module: A 2-way set associative cache with a write-through policy. It performs
 *    read and write operations based on the signals provided by the cache interface.
 *
 * The combined cache ensures synchronized access to a shared cache resource by the 
 * instruction and execution units, facilitating efficient data retrieval and storage
 * while avoiding conflicts and ensuring data integrity.
 */
module combined_cache(
    input logic clk,
    input logic reset,
    input logic req_instr,
    input logic req_exec,
    input logic [31:0] addr_instr,
    input logic [31:0] addr_exec,
    input logic [31:0] data_exec,
    input logic we_exec, // Write enable for execution unit
    output logic [31:0] data_out_instr,
    output logic [31:0] data_out_exec,
    output logic grant_instr,
    output logic grant_exec,
    output logic hit_instr,
    output logic hit_exec
);
    // Instantiate the arbiter
    arbiter arb (
        .clk(clk),
        .reset(reset),
        .req_instr(req_instr),
        .req_exec(req_exec),
        .grant_instr(grant_instr),
        .grant_exec(grant_exec)
    );

    // Cache signals
    logic [31:0] cache_addr;
    logic [31:0] cache_data_in;
    logic [31:0] cache_data_out;
    logic cache_read;
    logic cache_write;
    logic cache_hit;

    // Connect cache based on grants
    assign cache_addr = (grant_instr) ? addr_instr : addr_exec;
    assign cache_data_in = data_exec;
    assign cache_read = (grant_instr) ? req_instr : 1'b0;
    assign cache_write = (grant_exec) ? we_exec : 1'b0;

    // Cache instance
    cache #(
        .CACHE_SIZE(1024),
        .BLOCK_SIZE(16),
        .ADDR_WIDTH(32),
        .DATA_WIDTH(32)
    ) my_cache (
        .clk(clk),
        .rst(reset),
        .read(cache_read),
        .write(cache_write),
        .addr(cache_addr),
        .wdata(cache_data_in),
        .rdata(cache_data_out),
        .hit(cache_hit)
    );

    // Route cache data to the appropriate output
    assign data_out_instr = (grant_instr && cache_hit) ? cache_data_out : 32'b0;
    assign data_out_exec = (grant_exec && cache_hit) ? cache_data_out : 32'b0;
    assign hit_instr = (grant_instr) ? cache_hit : 1'b0;
    assign hit_exec = (grant_exec) ? cache_hit : 1'b0;
endmodule
