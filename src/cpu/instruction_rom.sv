module instruction_rom (
    input logic [8:0] opcode,
    output logic [2:0] num_operands,
    output logic [3:0] base_data_type,
    output logic [3:0] operand_data_type [5:0],
    output logic [2:0] access_mode [5:0]
);

    always_comb begin
        // Default outputs
        num_operands = 3'b000;
        base_data_type = 4'b0000;
        operand_data_type = '{default: 4'b0000};
        access_mode = '{default: 3'b000};

        // Lookup table based on opcode
        case (opcode)
            9'h00: begin
                num_operands = 3'b010;
                base_data_type = 4'h2; // Longword
                operand_data_type = '{4'h2, 4'h2, 4'h0, 4'h0, 4'h0, 4'h0};
                access_mode = '{3'h1, 3'h2, 3'h0, 3'h0, 3'h0, 3'h0};
            end
            9'h01: begin
                num_operands = 3'b001;
                base_data_type = 4'h3; // Quadword
                operand_data_type = '{4'h3, 4'h0, 4'h0, 4'h0, 4'h0, 4'h0};
                access_mode = '{3'h1, 3'h0, 3'h0, 3'h0, 3'h0, 3'h0};
            end
            // Add more cases for each opcode
            default: begin
                // Defaults already set
            end
        endcase
    end
endmodule
