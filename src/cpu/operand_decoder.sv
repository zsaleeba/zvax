module operand_decoder (
    input  logic [5:0] start_offset,          // Starting offset into the instruction buffer
    input  logic [2:0] expected_data_size,    // Expected data size of the operand
    input  logic       is_branch,             // Whether it's a branch offset operand
    input  logic       branch_offset_size,    // Size of branch offset (1 or 2 bytes)
    input  logic [2:0] operands_remaining,    // Number of operands remaining to decode
    input  logic [7:0] instruction_buffer [0:63], // Instruction buffer (64 bytes)
    output logic [4:0] operand_bytes,         // Number of bytes this operand occupies
    output logic [5:0] next_offset,           // Offset for the next operand
    output logic [2:0] operands_remaining_out,// Number of operands remaining to decode
    output logic       reserved_fault,        // Reserved addressing mode fault
    output logic [3:0] addressing_mode,       // Addressing mode used
    output logic [3:0] reg_number,            // Register number
    output logic [5:0] data_offset,           // Operand's data offset in instruction buffer
    output logic       indexed_mode,          // Whether indexed addressing is used
    output logic [3:0] index_reg,             // Index register if indexed addressing
    output logic       operand_present,       // Whether an operand is present
    output logic [5:0] literal_value          // Literal value for literal mode
);

    // Internal variables
    logic [7:0] addr_mode_byte;
    logic [4:0] size_table [4:0];
    logic       fault;
    logic [4:0] operand_length;

    // Precompute size table for different expected data sizes
    // Size table: 1, 2, 4, 8, 16 (Byte, Word, Longword, Quadword, Octaword)
    initial begin
        size_table[3'd0] = 5'd1;
        size_table[3'd1] = 5'd2;
        size_table[3'd2] = 5'd4;
        size_table[3'd3] = 5'd8;
        size_table[3'd4] = 5'd16;
    end

    always_comb begin
        // Default outputs
        operand_bytes = 5'd0;
        next_offset = start_offset;
        operands_remaining_out = operands_remaining;
        reserved_fault = 1'b0;
        addressing_mode = 4'h0;
        reg_number = 4'h0;
        data_offset = 6'h0;
        indexed_mode = 1'b0;
        index_reg = 4'h0;
        operand_present = 1'b0;
        literal_value = 6'd0;
        operand_length = 5'd1; // Default operand length

        // Check for zero operands remaining
        if (operands_remaining == 3'b000) begin
            operand_present = 1'b0;
            operands_remaining_out = 3'b000;
            return;
        end

        // Operand is present
        operand_present = 1'b1;
        addr_mode_byte = instruction_buffer[start_offset];
        fault = 1'b0;

        // Handle branch operand first for quick exit
        if (is_branch) begin
            addressing_mode = 4'h1;  // Branch mode
            operand_bytes = branch_offset_size ? 5'd2 : 5'd1;
            data_offset = start_offset;
            next_offset = start_offset + operand_bytes;
            operands_remaining_out = operands_remaining - 3'b001;
            return;
        end

        // Check for indexed mode prefix
        if (addr_mode_byte[7:4] == 4'h4) begin
            indexed_mode = 1'b1;
            index_reg = addr_mode_byte[3:0];
            addr_mode_byte = instruction_buffer[start_offset + 1];
            operand_length = 5'd2; // Account for index byte
        end

        // Determine addressing mode and operand length
        case (addr_mode_byte[7:4])
            4'h0, 4'h1, 4'h2, 4'h3: begin
                // Literal mode
                addressing_mode = 4'h0;
                operand_bytes = 5'd1;
                literal_value = addr_mode_byte[5:0];
                if (indexed_mode) fault = 1'b1;
            end
            4'h5: begin
                // Register mode
                addressing_mode = 4'h5;
                operand_bytes = 5'd1;
                reg_number = addr_mode_byte[3:0];
                if (indexed_mode) fault = 1'b1;
            end
            4'h6: begin
                // Register deferred mode
                addressing_mode = 4'h6;
                operand_bytes = 5'd1;
                reg_number = addr_mode_byte[3:0];
                if (indexed_mode) fault = 1'b1;
            end
            4'h7: begin
                // Autodecrement mode
                addressing_mode = 4'h7;
                operand_bytes = 5'd1;
                reg_number = addr_mode_byte[3:0];
                if (indexed_mode) fault = 1'b1;
            end
            4'h8: begin
                if (addr_mode_byte == 8'h8F) begin
                    // Immediate mode
                    addressing_mode = 4'h2;
                    operand_bytes = 5'd1 + size_table[expected_data_size];
                    data_offset = start_offset + operand_length;
                end else begin
                    // Autoincrement mode
                    addressing_mode = 4'h8;
                    operand_bytes = 5'd1;
                    reg_number = addr_mode_byte[3:0];
                    if (indexed_mode) fault = 1'b1;
                end
            end
            4'h9: begin
                if (addr_mode_byte == 8'h9F) begin
                    // Absolute mode
                    addressing_mode = 4'h3;
                    operand_bytes = 5'd5;
                    data_offset = start_offset + operand_length;
                end else begin
                    // Autoincrement deferred mode
                    addressing_mode = 4'h9;
                    operand_bytes = 5'd1;
                    reg_number = addr_mode_byte[3:0];
                    if (indexed_mode) fault = 1'b1;
                end
            end
            4'hA, 4'hB: begin
                // Byte displacement modes
                addressing_mode = (addr_mode_byte[7:4] == 4'hA) ? 4'hA : 4'hB;
                operand_bytes = 5'd2;
                reg_number = addr_mode_byte[3:0];
                data_offset = start_offset + operand_length;
            end
            4'hC, 4'hD: begin
                // Word displacement modes
                addressing_mode = (addr_mode_byte[7:4] == 4'hC) ? 4'hC : 4'hD;
                operand_bytes = 5'd3;
                reg_number = addr_mode_byte[3:0];
                data_offset = start_offset + operand_length;
            end
            4'hE, 4'hF: begin
                // Longword displacement modes
                addressing_mode = (addr_mode_byte[7:4] == 4'hE) ? 4'hE : 4'hF;
                operand_bytes = 5'd5;
                reg_number = addr_mode_byte[3:0];
                data_offset = start_offset + operand_length;
            end
            default: begin
                // Reserved or invalid mode
                operand_bytes = 5'd1;
                fault = 1'b1;
            end
        endcase

        // Adjust operand length for indexed mode
        if (indexed_mode) operand_length += operand_bytes;

        // Check reserved fault for invalid indexed mode combinations
        if (indexed_mode && (addressing_mode <= 4'h3 || addressing_mode == 4'h5 ||
                             addressing_mode == 4'h6 || addressing_mode == 4'h7 ||
                             addressing_mode == 4'h8 || addressing_mode == 4'h9)) begin
            fault = 1'b1;
        end

        // Calculate next offset
        next_offset = start_offset + operand_length;

        // Set outputs
        reserved_fault = fault;
        operands_remaining_out = operands_remaining - 3'b001;
    end
endmodule
