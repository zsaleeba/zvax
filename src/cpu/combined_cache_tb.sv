module combined_cache_tb;

    // Testbench clock and reset signals
    logic clk;
    logic reset;

    // Instruction and execution unit request and address/data signals
    logic req_instr;
    logic req_exec;
    logic [31:0] addr_instr;
    logic [31:0] addr_exec;
    logic [31:0] data_exec;
    logic we_exec;

    // Combined cache output signals
    logic [31:0] data_out_instr;
    logic [31:0] data_out_exec;
    logic grant_instr;
    logic grant_exec;
    logic hit_instr;
    logic hit_exec;

    // Instantiate the combined cache module
    combined_cache uut (
        .clk(clk),
        .reset(reset),
        .req_instr(req_instr),
        .req_exec(req_exec),
        .addr_instr(addr_instr),
        .addr_exec(addr_exec),
        .data_exec(data_exec),
        .we_exec(we_exec),
        .data_out_instr(data_out_instr),
        .data_out_exec(data_out_exec),
        .grant_instr(grant_instr),
        .grant_exec(grant_exec),
        .hit_instr(hit_instr),
        .hit_exec(hit_exec)
    );

    // Clock generation
    always #5 clk = ~clk;

    // Initial block to initialize signals and apply test vectors
    initial begin
        // Initialize clock and reset
        clk = 0;
        reset = 0;

        // Initialize request and address/data signals
        req_instr = 0;
        req_exec = 0;
        addr_instr = 0;
        addr_exec = 0;
        data_exec = 0;
        we_exec = 0;

        // Apply reset
        reset = 1;
        #10;
        reset = 0;

        // Test 1: No requests
        #10;
        assert(grant_instr == 0) else $error("Test 1 failed: grant_instr should be 0");
        assert(grant_exec == 0) else $error("Test 1 failed: grant_exec should be 0");

        // Test 2: Instruction unit read request (address 0x00000010)
        addr_instr = 32'h00000010;
        req_instr = 1;
        #10;
        assert(grant_instr == 1) else $error("Test 2 failed: grant_instr should be 1");
        assert(hit_instr == 0) else $error("Test 2 failed: hit_instr should be 0");
        req_instr = 0;

        // Test 3: Execution unit write request (address 0x00000020, data 0xDEADBEEF)
        addr_exec = 32'h00000020;
        data_exec = 32'hDEADBEEF;
        we_exec = 1;
        req_exec = 1;
        #10;
        assert(grant_exec == 1) else $error("Test 3 failed: grant_exec should be 1");
        req_exec = 0;
        we_exec = 0;

        // Test 4: Instruction unit read request (address 0x00000020) after execution unit write
        addr_instr = 32'h00000020;
        req_instr = 1;
        #10;
        assert(grant_instr == 1) else $error("Test 4 failed: grant_instr should be 1");
        assert(hit_instr == 1) else $error("Test 4 failed: hit_instr should be 1");
        assert(data_out_instr == 32'hDEADBEEF) else $error("Test 4 failed: data_out_instr should be 0xDEADBEEF");
        req_instr = 0;

        // Test 5: Both units request simultaneously (instruction unit read, execution unit write)
        addr_instr = 32'h00000030;
        req_instr = 1;
        addr_exec = 32'h00000040;
        data_exec = 32'hCAFEBABE;
        we_exec = 1;
        req_exec = 1;
        #10;
        assert(grant_instr == 1) else $error("Test 5 failed: grant_instr should be 1");
        assert(hit_instr == 0) else $error("Test 5 failed: hit_instr should be 0");
        req_instr = 0;
        #10;
        assert(grant_exec == 1) else $error("Test 5 failed: grant_exec should be 1");
        req_exec = 0;
        we_exec = 0;

        // Test 6: Reset while requests are active
        addr_instr = 32'h00000050;
        req_instr = 1;
        addr_exec = 32'h00000060;
        data_exec = 32'hBEEFCAFE;
        we_exec = 1;
        req_exec = 1;
        #10;
        reset = 1;
        #10;
        reset = 0;
        assert(grant_instr == 0) else $error("Test 6 failed: grant_instr should be 0 after reset");
        assert(grant_exec == 0) else $error("Test 6 failed: grant_exec should be 0 after reset");
        req_instr = 0;
        req_exec = 0;
        we_exec = 0;

        // Test 7: Behavior after reset (instruction unit read request)
        addr_instr = 32'h00000010;
        req_instr = 1;
        #10;
        assert(grant_instr == 1) else $error("Test 7 failed: grant_instr should be 1");
        assert(hit_instr == 0) else $error("Test 7 failed: hit_instr should be 0");
        req_instr = 0;

        $display("All tests passed!");
        $finish;
    end

    // Monitor signals for debugging
    initial begin
        $monitor("Time: %0t | clk: %b | reset: %b | req_instr: %b | req_exec: %b | addr_instr: %h | addr_exec: %h | data_exec: %h | we_exec: %b | grant_instr: %b | grant_exec: %b | data_out_instr: %h | data_out_exec: %h | hit_instr: %b | hit_exec: %b",
                 $time, clk, reset, req_instr, req_exec, addr_instr, addr_exec, data_exec, we_exec, grant_instr, grant_exec, data_out_instr, data_out_exec, hit_instr, hit_exec);
    end

endmodule
