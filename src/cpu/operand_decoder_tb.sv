module operand_decoder_tb;

    // Parameters
    parameter INSTR_BUF_SIZE = 64;

    // Inputs
    logic [5:0] start_offset;
    logic [2:0] expected_data_size;
    logic       is_branch;
    logic       branch_offset_size;
    logic [2:0] operands_remaining;
    logic [7:0] instruction_buffer [0:INSTR_BUF_SIZE-1];

    // Outputs
    logic [4:0] operand_bytes;
    logic [5:0] next_offset;
    logic [2:0] operands_remaining_out;
    logic       reserved_fault;
    logic [3:0] addressing_mode;
    logic [3:0] reg_number;
    logic [5:0] data_offset;
    logic       indexed_mode;
    logic [3:0] index_reg;
    logic       operand_present;
    logic [5:0] literal_value;

    // Instantiate the operand decoder
    operand_decoder uut (
        .start_offset(start_offset),
        .expected_data_size(expected_data_size),
        .is_branch(is_branch),
        .branch_offset_size(branch_offset_size),
        .operands_remaining(operands_remaining),
        .instruction_buffer(instruction_buffer),
        .operand_bytes(operand_bytes),
        .next_offset(next_offset),
        .operands_remaining_out(operands_remaining_out),
        .reserved_fault(reserved_fault),
        .addressing_mode(addressing_mode),
        .reg_number(reg_number),
        .data_offset(data_offset),
        .indexed_mode(indexed_mode),
        .index_reg(index_reg),
        .operand_present(operand_present),
        .literal_value(literal_value)
    );

    // Task to display output values
    task display_outputs();
        $display("operand_bytes: %0d", operand_bytes);
        $display("next_offset: %0d", next_offset);
        $display("operands_remaining_out: %0d", operands_remaining_out);
        $display("reserved_fault: %0d", reserved_fault);
        $display("addressing_mode: %0d", addressing_mode);
        $display("reg_number: %0d", reg_number);
        $display("data_offset: %0d", data_offset);
        $display("indexed_mode: %0d", indexed_mode);
        $display("index_reg: %0d", index_reg);
        $display("operand_present: %0d", operand_present);
        $display("literal_value: %0d", literal_value);
        $display("--------------------------------------------------");
    endtask

    // Initialize test
    initial begin
        // Test Case 1: No operands remaining
        $display("Test Case 1: No operands remaining");
        start_offset = 6'd0;
        expected_data_size = 3'd0;
        is_branch = 1'b0;
        branch_offset_size = 1'b0;
        operands_remaining = 3'd0;
        instruction_buffer = '{default: 8'h00};
        #1;
        display_outputs();
        assert(operand_present == 1'b0);
        assert(operand_bytes == 5'd0);
        assert(next_offset == start_offset);
        assert(operands_remaining_out == 3'd0);
        assert(reserved_fault == 1'b0);

        // Test Case 2: Branch operand with 1-byte offset
        $display("Test Case 2: Branch operand with 1-byte offset");
        start_offset = 6'd0;
        expected_data_size = 3'd0;
        is_branch = 1'b1;
        branch_offset_size = 1'b0;
        operands_remaining = 3'd1;
        instruction_buffer = '{default: 8'h00};
        instruction_buffer[0] = 8'hAA; // Arbitrary byte
        #1;
        display_outputs();
        assert(operand_present == 1'b1);
        assert(operand_bytes == 5'd1);
        assert(next_offset == 6'd1);
        assert(operands_remaining_out == 3'd0);
        assert(reserved_fault == 1'b0);
        assert(addressing_mode == 4'h1);
        assert(data_offset == 6'd0);

        // Test Case 3: Branch operand with 2-byte offset
        $display("Test Case 3: Branch operand with 2-byte offset");
        start_offset = 6'd0;
        expected_data_size = 3'd0;
        is_branch = 1'b1;
        branch_offset_size = 1'b1;
        operands_remaining = 3'd1;
        instruction_buffer = '{default: 8'h00};
        instruction_buffer[0] = 8'hBB; // Arbitrary byte
        instruction_buffer[1] = 8'hCC; // Arbitrary byte
        #1;
        display_outputs();
        assert(operand_present == 1'b1);
        assert(operand_bytes == 5'd2);
        assert(next_offset == 6'd2);
        assert(operands_remaining_out == 3'd0);
        assert(reserved_fault == 1'b0);
        assert(addressing_mode == 4'h1);
        assert(data_offset == 6'd0);

        // Test Case 4: Indexed mode with immediate addressing (1-byte data)
        $display("Test Case 4: Indexed mode with immediate addressing (1-byte data)");
        start_offset = 6'd0;
        expected_data_size = 3'd0; // Byte
        is_branch = 1'b0;
        branch_offset_size = 1'b0;
        operands_remaining = 3'd1;
        instruction_buffer = '{default: 8'h00};
        instruction_buffer[0] = 8'h4F; // Indexed mode, index reg = 0xF
        instruction_buffer[1] = 8'h8F; // Immediate mode
        instruction_buffer[2] = 8'h12; // Data byte
        #1;
        display_outputs();
        assert(operand_present == 1'b1);
        assert(operand_bytes == 5'd2); // 1 for indexed + 1 for immediate byte
        assert(next_offset == 6'd2);
        assert(operands_remaining_out == 3'd0);
        assert(reserved_fault == 1'b0);
        assert(addressing_mode == 4'h2);
        assert(indexed_mode == 1'b1);
        assert(index_reg == 4'hF);
        assert(data_offset == 6'd1);

        // Test Case 5: Literal addressing mode
        $display("Test Case 5: Literal addressing mode");
        start_offset = 6'd0;
        expected_data_size = 3'd0;
        is_branch = 1'b0;
        branch_offset_size = 1'b0;
        operands_remaining = 3'd1;
        instruction_buffer = '{default: 8'h00};
        instruction_buffer[0] = 8'h31; // Literal mode, value = 0x31
        #1;
        display_outputs();
        assert(operand_present == 1'b1);
        assert(operand_bytes == 5'd1);
        assert(next_offset == 6'd1);
        assert(operands_remaining_out == 3'd0);
        assert(reserved_fault == 1'b0);
        assert(addressing_mode == 4'h0);
        assert(data_offset == 6'd0);
        assert(literal_value == 6'd49); // 0x31 in decimal

        // Test Case 6: Register mode
        $display("Test Case 6: Register mode");
        start_offset = 6'd0;
        expected_data_size = 3'd0;
        is_branch = 1'b0;
        branch_offset_size = 1'b0;
        operands_remaining = 3'd1;
        instruction_buffer = '{default: 8'h00};
        instruction_buffer[0] = 8'h58; // Register mode, register = 0x8
        #1;
        display_outputs();
        assert(operand_present == 1'b1);
        assert(operand_bytes == 5'd1);
        assert(next_offset == 6'd1);
        assert(operands_remaining_out == 3'd0);
        assert(reserved_fault == 1'b0);
        assert(addressing_mode == 4'h5);
        assert(data_offset == 6'd0);
        assert(reg_number == 4'h8);

        // Test Case 7: Absolute mode
        $display("Test Case 7: Absolute mode");
        start_offset = 6'd0;
        expected_data_size = 3'd0;
        is_branch = 1'b0;
        branch_offset_size = 1'b0;
        operands_remaining = 3'd1;
        instruction_buffer = '{default: 8'h00};
        instruction_buffer[0] = 8'h9F; // Absolute mode
        instruction_buffer[1] = 8'h11; // Data byte 1
        instruction_buffer[2] = 8'h22; // Data byte 2
        instruction_buffer[3] = 8'h33; // Data byte 3
        instruction_buffer[4] = 8'h44; // Data byte 4
        #1;
        display_outputs();
        assert(operand_present == 1'b1);
        assert(operand_bytes == 5'd5); // 1 for address mode + 4 for data
        assert(next_offset == 6'd5);
        assert(operands_remaining_out == 3'd0);
        assert(reserved_fault == 1'b0);
        assert(addressing_mode == 4'h3);
        assert(data_offset == 6'd1);

        // Test Case 8: Indexed mode with a reserved fault
        $display("Test Case 8: Indexed mode with reserved fault");
        start_offset = 6'd0;
        expected_data_size = 3'd0;
        is_branch = 1'b0;
        branch_offset_size = 1'b0;
        operands_remaining = 3'd1;
        instruction_buffer = '{default: 8'h00};
        instruction_buffer[0] = 8'h4F; // Indexed mode, index reg = 0xF
        instruction_buffer[1] = 8'h4A; // Invalid nested indexed mode
        #1;
        display_outputs();
        assert(operand_present == 1'b1);
        assert(operand_bytes == 5'd2);
        assert(next_offset == 6'd2);
        assert(operands_remaining_out == 3'd0);
        assert(reserved_fault == 1'b1);
        assert(indexed_mode == 1'b1);
        assert(index_reg == 4'hF);

        // Test Case 9: Word displacement mode
        $display("Test Case 9: Word displacement mode");
        start_offset = 6'd0;
        expected_data_size = 3'd0;
        is_branch = 1'b0;
        branch_offset_size = 1'b0;
        operands_remaining = 3'd1;
        instruction_buffer = '{default: 8'h00};
        instruction_buffer[0] = 8'hC7; // Word displacement mode, register = 0x7
        instruction_buffer[1] = 8'hDE; // Displacement byte 1
        instruction_buffer[2] = 8'hAD; // Displacement byte 2
        #1;
        display_outputs();
        assert(operand_present == 1'b1);
        assert(operand_bytes == 5'd3); // 1 for address mode + 2 for displacement
        assert(next_offset == 6'd3);
        assert(operands_remaining_out == 3'd0);
        assert(reserved_fault == 1'b0);
        assert(addressing_mode == 4'hC);
        assert(data_offset == 6'd1);

        // Test Case 10: Autoincrement mode
        $display("Test Case 10: Autoincrement mode");
        start_offset = 6'd0;
        expected_data_size = 3'd0;
        is_branch = 1'b0;
        branch_offset_size = 1'b0;
        operands_remaining = 3'd1;
        instruction_buffer = '{default: 8'h00};
        instruction_buffer[0] = 8'h85; // Autoincrement mode, register = 0x5
        #1;
        display_outputs();
        assert(operand_present == 1'b1);
        assert(operand_bytes == 5'd1);
        assert(next_offset == 6'd1);
        assert(operands_remaining_out == 3'd0);
        assert(reserved_fault == 1'b0);
        assert(addressing_mode == 4'h8);
        assert(reg_number == 4'h5);
        assert(data_offset == 6'd0);

        // Test Case 11: Longword displacement deferred mode
        $display("Test Case 11: Longword displacement deferred mode");
        start_offset = 6'd0;
        expected_data_size = 3'd0;
        is_branch = 1'b0;
        branch_offset_size = 1'b0;
        operands_remaining = 3'd1;
        instruction_buffer = '{default: 8'h00};
        instruction_buffer[0] = 8'hF7; // Longword displacement deferred mode, register = 0x7
        instruction_buffer[1] = 8'hBA; // Displacement byte 1
        instruction_buffer[2] = 8'hBE; // Displacement byte 2
        instruction_buffer[3] = 8'hCA; // Displacement byte 3
        instruction_buffer[4] = 8'hFE; // Displacement byte 4
        #1;
        display_outputs();
        assert(operand_present == 1'b1);
        assert(operand_bytes == 5'd5); // 1 for address mode + 4 for displacement
        assert(next_offset == 6'd5);
        assert(operands_remaining_out == 3'd0);
        assert(reserved_fault == 1'b0);
        assert(addressing_mode == 4'hF);
        assert(data_offset == 6'd1);

        // Test Case 12: Indexed mode with word displacement mode
        $display("Test Case 12: Indexed mode with word displacement mode");
        start_offset = 6'd0;
        expected_data_size = 3'd0;
        is_branch = 1'b0;
        branch_offset_size = 1'b0;
        operands_remaining = 3'd1;
        instruction_buffer = '{default: 8'h00};
        instruction_buffer[0] = 8'h4E; // Indexed mode, index reg = 0xE
        instruction_buffer[1] = 8'hC6; // Word displacement mode, register = 0x6
        instruction_buffer[2] = 8'h01; // Displacement byte 1
        instruction_buffer[3] = 8'h02; // Displacement byte 2
        #1;
        display_outputs();
        assert(operand_present == 1'b1);
        assert(operand_bytes == 5'd4); // 1 for index + 1 for address mode + 2 for displacement
        assert(next_offset == 6'd4);
        assert(operands_remaining_out == 3'd0);
        assert(reserved_fault == 1'b0);
        assert(addressing_mode == 4'hC);
        assert(indexed_mode == 1'b1);
        assert(index_reg == 4'hE);
        assert(data_offset == 6'd2);

        // Test Case 13: Zero operand size
        $display("Test Case 13: Zero operand size");
        start_offset = 6'd0;
        expected_data_size = 3'd0;
        is_branch = 1'b0;
        branch_offset_size = 1'b0;
        operands_remaining = 3'd0; // No operands remaining
        instruction_buffer = '{default: 8'h00};
        #1;
        display_outputs();
        assert(operand_present == 1'b0);
        assert(operand_bytes == 5'd0);
        assert(next_offset == 6'd0);
        assert(operands_remaining_out == 3'd0);

        // Test Case 14: Incorrect mode leading to reserved fault
        $display("Test Case 14: Incorrect mode leading to reserved fault");
        start_offset = 6'd0;
        expected_data_size = 3'd0;
        is_branch = 1'b0;
        branch_offset_size = 1'b0;
        operands_remaining = 3'd1;
        instruction_buffer = '{default: 8'h00};
        instruction_buffer[0] = 8'hFF; // Invalid mode
        #1;
        display_outputs();
        assert(operand_present == 1'b1);
        assert(operand_bytes == 5'd1);
        assert(next_offset == 6'd1);
        assert(operands_remaining_out == 3'd0);
        assert(reserved_fault == 1'b1);

        // Test Case 15: Maximum displacement mode
        $display("Test Case 15: Maximum displacement mode");
        start_offset = 6'd0;
        expected_data_size = 3'd0;
        is_branch = 1'b0;
        branch_offset_size = 1'b0;
        operands_remaining = 3'd1;
        instruction_buffer = '{default: 8'h00};
        instruction_buffer[0] = 8'hEF; // Longword displacement mode
        instruction_buffer[1] = 8'h10; // Displacement byte 1
        instruction_buffer[2] = 8'h20; // Displacement byte 2
        instruction_buffer[3] = 8'h30; // Displacement byte 3
        instruction_buffer[4] = 8'h40; // Displacement byte 4
        #1;
        display_outputs();
        assert(operand_present == 1'b1);
        assert(operand_bytes == 5'd5); // 1 for address mode + 4 for displacement
        assert(next_offset == 6'd5);
        assert(operands_remaining_out == 3'd0);
        assert(reserved_fault == 1'b0);
        assert(addressing_mode == 4'hE);
        assert(data_offset == 6'd1);

        // Test Case 16: Indexed mode with branch operand
        $display("Test Case 16: Indexed mode with branch operand (should fault)");
        start_offset = 6'd0;
        expected_data_size = 3'd0;
        is_branch = 1'b1;
        branch_offset_size = 1'b1;
        operands_remaining = 3'd1;
        instruction_buffer = '{default: 8'h00};
        instruction_buffer[0] = 8'h4F; // Indexed mode
        instruction_buffer[1] = 8'hBB; // Branch offset
        #1;
        display_outputs();
        assert(operand_present == 1'b1);
        assert(operand_bytes == 5'd2);
        assert(next_offset == 6'd2);
        assert(operands_remaining_out == 3'd0);
        assert(reserved_fault == 1'b1);
        assert(indexed_mode == 1'b1);

        // Test Case 17: Maximum buffer offset
        $display("Test Case 17: Maximum buffer offset");
        start_offset = 6'd63; // Start at the last byte
        expected_data_size = 3'd0;
        is_branch = 1'b0;
        branch_offset_size = 1'b0;
        operands_remaining = 3'd1;
        instruction_buffer = '{default: 8'h00};
        instruction_buffer[63] = 8'h90; // Arbitrary mode
        #1;
        display_outputs();
        assert(operand_present == 1'b1);
        assert(operand_bytes == 5'd1);
        assert(next_offset == 6'd0); // Wrap around
        assert(operands_remaining_out == 3'd0);
        assert(reserved_fault == 1'b0);
        
        // Test Case 18: Quadword immediate mode
        $display("Test Case 18: Quadword immediate mode");
        start_offset = 6'd0;
        expected_data_size = 3'd3; // Quadword
        is_branch = 1'b0;
        branch_offset_size = 1'b0;
        operands_remaining = 3'd1;
        instruction_buffer = '{default: 8'h00};
        instruction_buffer[0] = 8'h8F; // Immediate mode
        instruction_buffer[1] = 8'hAA; // Data byte 1
        instruction_buffer[2] = 8'hBB; // Data byte 2
        instruction_buffer[3] = 8'hCC; // Data byte 3
        instruction_buffer[4] = 8'hDD; // Data byte 4
        instruction_buffer[5] = 8'hEE; // Data byte 5
        instruction_buffer[6] = 8'hFF; // Data byte 6
        instruction_buffer[7] = 8'h11; // Data byte 7
        instruction_buffer[8] = 8'h22; // Data byte 8
        #1;
        display_outputs();
        assert(operand_present == 1'b1);
        assert(operand_bytes == 5'd9); // 1 for address mode + 8 for data
        assert(next_offset == 6'd9);
        assert(operands_remaining_out == 3'd0);
        assert(reserved_fault == 1'b0);
        assert(addressing_mode == 4'h2);
        assert(data_offset == 6'd1);

        // End of test
        $display("All test cases completed.");
        $stop;
    end

endmodule
