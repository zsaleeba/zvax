module instruction_decoder_tb;

    // Parameters
    parameter INSTR_BUF_SIZE = 64;
    parameter MAX_OPERANDS = 6;

    // Inputs
    logic [7:0] instruction_buffer [0:INSTR_BUF_SIZE-1];
    logic [5:0] start_offset;

    // Outputs
    logic [8:0] opcode;
    logic [5:0] total_bytes;
    logic [2:0] num_operands;
    logic [4:0] addressing_mode [MAX_OPERANDS-1:0];
    logic [2:0] data_size [MAX_OPERANDS-1:0];
    logic [3:0] data_type [MAX_OPERANDS-1:0];
    logic [3:0] reg_number [MAX_OPERANDS-1:0];
    logic [5:0] literal_value [MAX_OPERANDS-1:0];
    logic [5:0] data_offset [MAX_OPERANDS-1:0];
    logic       indexed_mode [MAX_OPERANDS-1:0];
    logic [3:0] index_reg [MAX_OPERANDS-1:0];
    logic       operand_present [MAX_OPERANDS-1:0];
    logic       reserved_fault;

    // Instantiate the instruction decoder
    instruction_decoder uut (
        .instruction_buffer(instruction_buffer),
        .start_offset(start_offset),
        .opcode(opcode),
        .total_bytes(total_bytes),
        .num_operands(num_operands),
        .addressing_mode(addressing_mode),
        .data_size(data_size),
        .data_type(data_type),
        .reg_number(reg_number),
        .literal_value(literal_value),
        .data_offset(data_offset),
        .indexed_mode(indexed_mode),
        .index_reg(index_reg),
        .operand_present(operand_present),
        .reserved_fault(reserved_fault)
    );

    // Task to display output values for debugging
    task display_outputs();
        $display("Opcode: %h", opcode);
        $display("Total Bytes: %0d", total_bytes);
        $display("Number of Operands: %0d", num_operands);
        $display("Reserved Fault: %b", reserved_fault);
        for (int i = 0; i < MAX_OPERANDS; i++) begin
            if (operand_present[i]) begin
                $display("Operand %0d:", i);
                $display("  Addressing Mode: %0d", addressing_mode[i]);
                $display("  Data Size: %0d", data_size[i]);
                $display("  Data Type: %0d", data_type[i]);
                $display("  Register Number: %0d", reg_number[i]);
                $display("  Literal Value: %0d", literal_value[i]);
                $display("  Data Offset: %0d", data_offset[i]);
                $display("  Indexed Mode: %b", indexed_mode[i]);
                $display("  Index Register: %0d", index_reg[i]);
            end
        end
        $display("--------------------------------------------------");
    endtask

    // Initial block for simulation
    initial begin
        $display("Starting instruction_decoder testbench...");

        // Initialize instruction buffer with default values
        instruction_buffer = '{default: 8'h00};

        // Test Case 1: Single-byte opcode, no operands
        $display("Test Case 1: Single-byte opcode, no operands");
        start_offset = 6'd0;
        instruction_buffer[0] = 8'h00; // Single-byte opcode with no operands
        #1;
        display_outputs();
        assert(opcode == 9'h000);
        assert(total_bytes == 6'd1);
        assert(num_operands == 3'd0);
        assert(reserved_fault == 1'b0);

        // Test Case 2: Single-byte opcode, two operands
        $display("Test Case 2: Single-byte opcode, two operands");
        start_offset = 6'd0;
        instruction_buffer[0] = 8'h01; // Single-byte opcode
        instruction_buffer[1] = 8'h5F; // Operand 1: Register mode
        instruction_buffer[2] = 8'h8F; // Operand 2: Immediate mode
        #1;
        display_outputs();
        assert(opcode == 9'h001);
        assert(total_bytes == 6'd4); // 1 for opcode + 1 for each operand byte
        assert(num_operands == 3'd2);
        assert(reserved_fault == 1'b0);
        assert(operand_present[0] == 1'b1);
        assert(operand_present[1] == 1'b1);
        assert(addressing_mode[0] == 4'h5);
        assert(addressing_mode[1] == 4'h2);

        // Test Case 3: Two-byte opcode, one operand
        $display("Test Case 3: Two-byte opcode, one operand");
        start_offset = 6'd0;
        instruction_buffer[0] = 8'hFD; // Opcode prefix
        instruction_buffer[1] = 8'h02; // Opcode value
        instruction_buffer[2] = 8'h7E; // Operand 1: Autodecrement mode
        #1;
        display_outputs();
        assert(opcode == 9'h102);
        assert(total_bytes == 6'd3); // 2 for opcode + 1 for operand byte
        assert(num_operands == 3'd1);
        assert(reserved_fault == 1'b0);
        assert(operand_present[0] == 1'b1);
        assert(addressing_mode[0] == 4'h7);

        // Test Case 4: Two-byte opcode, three operands, reserved fault
        $display("Test Case 4: Two-byte opcode, three operands, reserved fault");
        start_offset = 6'd0;
        instruction_buffer[0] = 8'hFD; // Opcode prefix
        instruction_buffer[1] = 8'h03; // Opcode value
        instruction_buffer[2] = 8'h4F; // Operand 1: Indexed mode (invalid combination)
        instruction_buffer[3] = 8'h5E; // Operand 2: Register mode
        instruction_buffer[4] = 8'hA0; // Operand 3: Byte displacement mode
        #1;
        display_outputs();
        assert(opcode == 9'h103);
        assert(total_bytes == 6'd5); // 2 for opcode + 3 for operand bytes
        assert(num_operands == 3'd3);
        assert(reserved_fault == 1'b1);
        assert(operand_present[0] == 1'b1);
        assert(operand_present[1] == 1'b1);
        assert(operand_present[2] == 1'b1);
        assert(addressing_mode[0] == 4'hA);

        // Test Case 5: Single-byte opcode, variable data types
        $display("Test Case 5: Single-byte opcode, variable data types");
        start_offset = 6'd0;
        instruction_buffer[0] = 8'h04; // Single-byte opcode
        instruction_buffer[1] = 8'h8F; // Operand 1: Immediate mode
        instruction_buffer[2] = 8'h9F; // Operand 2: Absolute mode
        instruction_buffer[3] = 8'hC3; // Operand 3: Word displacement mode
        #1;
        display_outputs();
        assert(opcode == 9'h004);
        assert(total_bytes == 6'd7); // 1 for opcode + 6 for operands
        assert(num_operands == 3'd3);
        assert(reserved_fault == 1'b0);
        assert(operand_present[0] == 1'b1);
        assert(operand_present[1] == 1'b1);
        assert(operand_present[2] == 1'b1);
        assert(addressing_mode[0] == 4'h2);
        assert(addressing_mode[1] == 4'h3);
        assert(addressing_mode[2] == 4'hC);

        // Additional test cases can be added here for more thorough testing

        $display("All test cases completed.");
        $finish;
    end

endmodule
