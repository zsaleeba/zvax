module instruction_buffer_tb;

    // Parameters
    parameter MAX_BYTES = 12;

    // Testbench clock and reset signals
    logic clk;
    logic reset;

    // Inputs
    logic reload;
    logic [31:0] load_address;
    logic advance;
    logic [$clog2(MAX_BYTES):0] advance_byte_count;

    // Outputs
    logic [MAX_BYTES*8-1:0] instr_buffer;
    logic [$clog2(MAX_BYTES+1):0] full_count;

    // Cache interface
    logic cache_hit;
    logic [31:0] cache_data;
    logic [31:0] cache_address;
    logic cache_read;

    // Instantiate the instruction buffer module
    instruction_buffer #(.MAX_BYTES(MAX_BYTES)) uut (
        .clk(clk),
        .reset(reset),
        .reload(reload),
        .load_address(load_address),
        .advance(advance),
        .advance_byte_count(advance_byte_count),
        .instr_buffer(instr_buffer),
        .full_count(full_count),
        .cache_hit(cache_hit),
        .cache_data(cache_data),
        .cache_address(cache_address),
        .cache_read(cache_read)
    );

    // Clock generation
    always #5 clk = ~clk;

    // Initial block to initialize signals and apply test vectors
    initial begin
        // Initialize clock and reset
        clk = 0;
        reset = 0;

        // Initialize input signals
        reload = 0;
        load_address = 0;
        advance = 0;
        advance_byte_count = 0;
        cache_hit = 0;
        cache_data = 0;

        // Apply reset
        reset = 1;
        #10;
        reset = 0;

        // Test 1: Reload the buffer with address 0x1000
        load_address = 32'h1000;
        reload = 1;
        #10;
        reload = 0;
        // Simulate cache hits and data loading
        #10;
        cache_hit = 1;
        cache_data = 32'h12345678;
        #10;
        cache_hit = 1;
        cache_data = 32'h9ABCDEF0;
        #10;
        cache_hit = 1;
        cache_data = 32'h11223344;
        #10;
        cache_hit = 0;
        assert(instr_buffer == 96'h112233449ABCDEF012345678) else $error("Test 1 failed: Incorrect instruction buffer content");
        assert(full_count == 12) else $error("Test 1 failed: Incorrect full count");

        // Test 2: Advance the buffer by 4 bytes
        advance_byte_count = 4;
        advance = 1;
        #10;
        advance = 0;
        // Simulate cache hit and data loading
        #10;
        cache_hit = 1;
        cache_data = 32'h55667788;
        #10;
        cache_hit = 0;
        assert(instr_buffer == 96'h55667788112233449ABCDEF0) else $error("Test 2 failed: Incorrect instruction buffer content");
        assert(full_count == 12) else $error("Test 2 failed: Incorrect full count");

        // Test 3: Advance the buffer by 6 bytes
        advance_byte_count = 6;
        advance = 1;
        #10;
        advance = 0;
        // Simulate cache hit and data loading
        #10;
        cache_hit = 1;
        cache_data = 32'hAABBCCDD;
        #10;
        cache_hit = 0;
        assert(instr_buffer == 96'hAABBCCDD5566778811223344) else $error("Test 3 failed: Incorrect instruction buffer content");
        assert(full_count == 12) else $error("Test 3 failed: Incorrect full count");

        // Test 4: Reload the buffer with address 0x2000
        load_address = 32'h2000;
        reload = 1;
        #10;
        reload = 0;
        // Simulate cache hits and data loading
        #10;
        cache_hit = 1;
        cache_data = 32'hFFEEDDCC;
        #10;
        cache_hit = 1;
        cache_data = 32'hBBAA9988;
        #10;
        cache_hit = 1;
        cache_data = 32'h77665544;
        #10;
        cache_hit = 0;
        assert(instr_buffer == 96'h77665544BBAA9988FFEEDDCC) else $error("Test 4 failed: Incorrect instruction buffer content");
        assert(full_count == 12) else $error("Test 4 failed: Incorrect full count");

        $display("All tests passed!");
        $finish;
    end

    // Monitor signals for debugging
    initial begin
        $monitor("Time: %0t | clk: %b | reset: %b | reload: %b | load_address: %h | advance: %b | advance_byte_count: %0d | instr_buffer: %h | full_count: %0d | cache_hit: %b | cache_data: %h | cache_address: %h | cache_read: %b",
                 $time, clk, reset, reload, load_address, advance, advance_byte_count, instr_buffer, full_count, cache_hit, cache_data, cache_address, cache_read);
    end

endmodule
