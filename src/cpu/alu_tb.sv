module alu_tb;

    // Parameters
    localparam WIDTH = 4;

    // Signals
    logic [WIDTH-1:0] A;
    logic [WIDTH-1:0] B;
    logic [3:0]       S;
    logic             M;
    logic             Cin;
    logic [WIDTH-1:0] F;
    logic             Cout;

    // Instantiate the ALU
    alu #(
        .WIDTH(WIDTH)
    ) uut (
        .A(A),
        .B(B),
        .S(S),
        .M(M),
        .Cin(Cin),
        .F(F),
        .Cout(Cout)
    );

    // Test sequence
    initial begin
        // Initialize signals
        A = 0;
        B = 0;
        S = 0;
        M = 0;
        Cin = 0;

        // Test logical operations
        // Test 1: NOT A
        A = 4'b1100;
        S = 4'b0000;
        M = 0;
        #10;
        $display("Test 1: NOT A, A = %b, F = %b", A, F);

        // Test 2: NOT (A OR B)
        A = 4'b1100;
        B = 4'b1010;
        S = 4'b0001;
        M = 0;
        #10;
        $display("Test 2: NOT (A OR B), A = %b, B = %b, F = %b", A, B, F);

        // Test 3: (NOT A) AND B
        A = 4'b1100;
        B = 4'b1010;
        S = 4'b0010;
        M = 0;
        #10;
        $display("Test 3: (NOT A) AND B, A = %b, B = %b, F = %b", A, B, F);

        // Test 4: ALL ZEROS
        S = 4'b0011;
        M = 0;
        #10;
        $display("Test 4: ALL ZEROS, F = %b", F);

        // Test arithmetic operations
        // Test 5: A + (NOT Cin)
        A = 4'b0101;
        Cin = 0;
        S = 4'b0000;
        M = 1;
        #10;
        $display("Test 5: A + (NOT Cin), A = %b, Cin = %b, F = %b, Cout = %b", A, Cin, F, Cout);

        // Test 6: A + B + (NOT Cin)
        A = 4'b0011;
        B = 4'b0101;
        Cin = 0;
        S = 4'b1001;
        M = 1;
        #10;
        $display("Test 6: A + B + (NOT Cin), A = %b, B = %b, Cin = %b, F = %b, Cout = %b", A, B, Cin, F, Cout);

        // Test 7: A - B - 1 + (NOT Cin)
        A = 4'b1001;
        B = 4'b0011;
        Cin = 0;
        S = 4'b0110;
        M = 1;
        #10;
        $display("Test 7: A - B - 1 + (NOT Cin), A = %b, B = %b, Cin = %b, F = %b, Cout = %b", A, B, Cin, F, Cout);

        // Add more tests as needed

        // Test 8: A XOR B
        A = 4'b1100;
        B = 4'b1010;
        S = 4'b0110;
        M = 0;
        #10;
        $display("Test 8: A XOR B, A = %b, B = %b, F = %b", A, B, F);

        // Test 9: A AND (NOT B)
        A = 4'b1100;
        B = 4'b1010;
        S = 4'b0111;
        M = 0;
        #10;
        $display("Test 9: A AND (NOT B), A = %b, B = %b, F = %b", A, B, F);

        // Test 10: (NOT A) OR B
        A = 4'b1100;
        B = 4'b1010;
        S = 4'b1000;
        M = 0;
        #10;
        $display("Test 10: (NOT A) OR B, A = %b, B = %b, F = %b", A, B, F);

        // Test 11: A AND B
        A = 4'b1100;
        B = 4'b1010;
        S = 4'b1011;
        M = 0;
        #10;
        $display("Test 11: A AND B, A = %b, B = %b, F = %b", A, B, F);

        // Test 12: A OR B
        A = 4'b1100;
        B = 4'b1010;
        S = 4'b1110;
        M = 0;
        #10;
        $display("Test 12: A OR B, A = %b, B = %b, F = %b", A, B, F);

        // Test 13: Arithmetic: A - 1 + (NOT Cin)
        A = 4'b0101;
        Cin = 0;
        S = 4'b1111;
        M = 1;
        #10;
        $display("Test 13: A - 1 + (NOT Cin), A = %b, Cin = %b, F = %b, Cout = %b", A, Cin, F, Cout);

        // Add more tests for thorough verification

        // Finish the simulation
        $finish;
    end

endmodule
