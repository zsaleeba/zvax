module operand_info (
    input logic [3:0] data_type,        // Data type of the operand
    input logic [2:0] access_mode,      // Access mode of the operand
    output logic [2:0] expected_data_size, // Expected data size of the operand
    output logic       is_branch,       // Whether it's a branch operand
    output logic       branch_offset_size // Size of the branch offset (1 or 2 bytes)
);

    always_comb begin
        // Default outputs
        expected_data_size = 3'b000; // Byte
        is_branch = 1'b0;
        branch_offset_size = 1'b0;

        // Determine data size and branch properties
        case (access_mode)
            3'h0: begin
                // UNUSED / CUSTOM - defaults to Byte
                expected_data_size = 3'b000;
            end
            3'h1, 3'h2, 3'h3, 3'h6, 3'h7: begin
                // READ, WRITE, MODIFY, ADDRESS, V_ADDRESS
                // Set expected data size based on data type
                if (data_type >= 4'h9 && data_type <= 4'hC) begin
                    expected_data_size = 3'h2; // Treat 0x9 to 0xC as Longword
                end else begin
                    expected_data_size = data_type[2:0];
                end
            end
            3'h4: begin
                // BRANCH (byte size)
                is_branch = 1'b1;
                branch_offset_size = 1'b0; // 1 byte offset
            end
            3'h5: begin
                // BRANCH (word size)
                is_branch = 1'b1;
                branch_offset_size = 1'b1; // 2 bytes offset
            end
            default: begin
                // Default case for any undefined modes
                expected_data_size = 3'b000;
            end
        endcase
    end

endmodule
