module alu #(
    parameter WIDTH = 4  // Default bit width is now 4
)(
    input  logic [WIDTH-1:0]     A,      // Input A
    input  logic [WIDTH-1:0]     B,      // Input B
    input  logic [3:0]           S,      // Function select
    input  logic                 M,      // Mode select (0 for logic operations, 1 for arithmetic operations)
    input  logic                 Cin,    // Carry-in
    output logic [WIDTH-1:0]     F,      // Output
    output logic                 Cout    // Carry-out
);

    // Helper signals
    logic [WIDTH-1:0] extended_not_cin;
    logic [WIDTH-1:0] zero;
    logic [WIDTH:0] extended_A, extended_B;

    always_comb begin
        extended_not_cin = {{(WIDTH-1){1'b0}}, ~Cin};
        zero = {WIDTH{1'b0}};
        extended_A = {1'b0, A};
        extended_B = {1'b0, B};

        if (M == 0) begin
            // Logical operations
            case (S)
                4'b0000: F = ~A;          // NOT A
                4'b0001: F = ~(A | B);    // NOT (A OR B)
                4'b0010: F = (~A) & B;    // (NOT A) AND B
                4'b0011: F = zero;        // ALL ZEROS
                4'b0100: F = ~(A & B);    // NOT (A AND B)
                4'b0101: F = ~B;          // NOT B
                4'b0110: F = A ^ B;       // A XOR B
                4'b0111: F = A & (~B);    // A AND (NOT B)
                4'b1000: F = (~A) | B;    // (NOT A) OR B
                4'b1001: F = ~(A | B);    // NOT (A OR B)
                4'b1010: F = B;           // B
                4'b1011: F = A & B;       // A AND B
                4'b1100: F = ~zero;       // ALL ONES
                4'b1101: F = A | (~B);    // A OR (NOT B)
                4'b1110: F = A | B;       // A OR B
                4'b1111: F = A;           // A
                default: F = zero;        // Default output is zero
            endcase
            Cout = 1'b0;                  // For logic operations, no carry-out
        end else begin
            // Arithmetic operations
            case (S)
                4'b0000: {Cout, F} = extended_A + extended_not_cin;                  // A + (NOT Cin)
                4'b0001: {Cout, F} = (extended_A | extended_B) + extended_not_cin;   // (A OR B) + (NOT Cin)
                4'b0010: {Cout, F} = (extended_A | ~extended_B) + extended_not_cin;  // (A OR NOT B) + (NOT Cin)
                4'b0011: {Cout, F} = {1'b0, {WIDTH{1'b1}}} + extended_not_cin;       // -1 + (NOT Cin)
                4'b0100: {Cout, F} = extended_A + (extended_A & ~extended_B) + extended_not_cin; // A + (A AND (NOT B)) + (NOT Cin)
                4'b0101: {Cout, F} = (extended_A | extended_B) + (extended_A & ~extended_B) + extended_not_cin; // (A OR B) + (A AND (NOT B)) + (NOT Cin)
                4'b0110: {Cout, F} = extended_A - extended_B - 1 + extended_not_cin; // A - B - 1 + (NOT Cin)
                4'b0111: {Cout, F} = (extended_A & ~extended_B) - 1 + extended_not_cin; // (A AND (NOT B)) - 1 + (NOT Cin)
                4'b1000: {Cout, F} = extended_A + (extended_A & extended_B) + extended_not_cin; // A + (A AND B) + (NOT Cin)
                4'b1001: {Cout, F} = extended_A + extended_B + extended_not_cin;     // A + B + (NOT Cin)
                4'b1010: {Cout, F} = (extended_A | ~extended_B) + (extended_A & extended_B) + extended_not_cin; // (A OR (NOT B)) + (A AND B) + (NOT Cin)
                4'b1011: {Cout, F} = (extended_A & extended_B) - 1 + extended_not_cin; // (A AND B) - 1 + (NOT Cin)
                4'b1100: {Cout, F} = extended_A + extended_A + extended_not_cin;     // A + A + (NOT Cin)
                4'b1101: {Cout, F} = (extended_A | extended_B) + extended_A + extended_not_cin; // (A OR B) + A + (NOT Cin)
                4'b1110: {Cout, F} = (extended_A | ~extended_B) + extended_A + extended_not_cin; // (A OR (NOT B)) + A + (NOT Cin)
                4'b1111: {Cout, F} = extended_A - 1 + extended_not_cin;              // A - 1 + (NOT Cin)
                default: {Cout, F} = {1'b0, zero};                                   // Default output is zero
            endcase
        end
    end

endmodule
