#include "verilated.h"
#include "verilated_vcd_c.h"
#include "Vzvax.h"
#include "Vzvax__Syms.h"

#include <iostream>

int main(int argc, char **argv, char **env) {
    Verilated::commandArgs(argc, argv);
    Vzvax* top = new Vzvax;

    VerilatedVcdC* tfp = new VerilatedVcdC;
    Verilated::traceEverOn(true);
    top->trace(tfp, 99);
    tfp->open("obj/zvax.vcd");

    // Initialize inputs
    top->clk = 0;
    top->rst = 1;

    // Run for a few clock cycles
    for (int i = 0; i < 10; ++i) {
        top->clk = !top->clk;
        top->eval();
        tfp->dump(i);
    }

    // Deassert reset
    top->rst = 0;
    for (int i = 10; i < 100; ++i) {
        top->clk = !top->clk;
        top->eval();
        tfp->dump(i);
    }

    tfp->close();
    delete top;
    delete tfp;
    return 0;
}
