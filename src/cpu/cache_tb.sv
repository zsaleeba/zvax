module cache_tb;

    // Parameters
    localparam CACHE_SIZE = 1024;
    localparam BLOCK_SIZE = 16;
    localparam ADDR_WIDTH = 32;
    localparam DATA_WIDTH = 32;

    // Signals
    logic clk;
    logic rst;
    logic read;
    logic write;
    logic [ADDR_WIDTH-1:0] addr;
    logic [DATA_WIDTH-1:0] wdata;
    logic [DATA_WIDTH-1:0] rdata;
    logic hit;

    // Instantiate the cache
    cache #(
        .CACHE_SIZE(CACHE_SIZE),
        .BLOCK_SIZE(BLOCK_SIZE),
        .ADDR_WIDTH(ADDR_WIDTH),
        .DATA_WIDTH(DATA_WIDTH)
    ) uut (
        .clk(clk),
        .rst(rst),
        .read(read),
        .write(write),
        .addr(addr),
        .wdata(wdata),
        .rdata(rdata),
        .hit(hit)
    );

    // Clock generation
    always #5 clk = ~clk;

    // Test sequence
    initial begin
        // Initialize signals
        clk = 0;
        rst = 1;
        read = 0;
        write = 0;
        addr = 0;
        wdata = 0;

        // Reset the cache
        #10;
        rst = 0;

        // Test 1: Write to cache
        write = 1;
        addr = 32'h0000_0000;
        wdata = 32'hAAAA_AAAA;
        #10;
        write = 0;

        // Test 2: Read from cache (expect hit)
        read = 1;
        addr = 32'h0000_0000;
        #10;
        read = 0;

        // Test 3: Write to a different address
        write = 1;
        addr = 32'h0000_0010;
        wdata = 32'h5555_5555;
        #10;
        write = 0;

        // Test 4: Read from the new address (expect hit)
        read = 1;
        addr = 32'h0000_0010;
        #10;
        read = 0;

        // Test 5: Read from the first address again (expect hit)
        read = 1;
        addr = 32'h0000_0000;
        #10;
        read = 0;

        // Test 6: Read from an address that causes a miss
        read = 1;
        addr = 32'h0000_0100;
        #10;
        read = 0;

        // Test 7: Write to the same address to see if data updates correctly
        write = 1;
        addr = 32'h0000_0000;
        wdata = 32'hBBBB_BBBB;
        #10;
        write = 0;

        // Test 8: Read the updated address (expect hit and new data)
        read = 1;
        addr = 32'h0000_0000;
        #10;
        read = 0;

        // Test 9: Test random replacement by writing multiple addresses
        for (int i = 0; i < 10; i++) begin
            write = 1;
            addr = $random;
            wdata = $random;
            #10;
            write = 0;
        end

        // Test 10: Reading from those addresses to check cache behavior
        for (int i = 0; i < 10; i++) begin
            read = 1;
            addr = $random;
            #10;
            read = 0;
        end

        // Test 11: Reset the cache and check if all entries are invalidated
        rst = 1;
        #10;
        rst = 0;
        read = 1;
        addr = 32'h0000_0000;
        #10;
        read = 0;

        // Test 12: Check behavior for sequential access
        for (int i = 0; i < 10; i++) begin
            write = 1;
            addr = i * BLOCK_SIZE;
            wdata = i;
            #10;
            write = 0;
        end

        for (int i = 0; i < 10; i++) begin
            read = 1;
            addr = i * BLOCK_SIZE;
            #10;
            read = 0;
        end

        // Add more tests as needed

        $finish;
    end

    // Display results
    initial begin
        $monitor("Time: %0t | Read: %0b | Write: %0b | Addr: %h | Wdata: %h | Rdata: %h | Hit: %0b", 
                  $time, read, write, addr, wdata, rdata, hit);
    end

endmodule
