module instruction_decoder (
    input  logic [7:0] instruction_buffer [0:63], // Instruction buffer (64 bytes)
    input  logic [5:0] start_offset,              // Starting offset into the instruction buffer
    output logic [8:0] opcode,                    // Opcode (9 bits)
    output logic [5:0] total_bytes,               // Total number of bytes the instruction occupies
    output logic [2:0] num_operands,              // Number of operands
    output logic [4:0] addressing_mode [5:0],     // Addressing mode for up to 6 operands
    output logic [2:0] data_size [5:0],           // Data size of operands
    output logic [3:0] data_type [5:0],           // Data type of operands
    output logic [3:0] reg_number [5:0],          // Register number for operands
    output logic [5:0] literal_value [5:0],       // Literal value for operands
    output logic [5:0] data_offset [5:0],         // Data offset for operands
    output logic       indexed_mode [5:0],        // Whether indexed addressing is used for operands
    output logic [3:0] index_reg [5:0],           // Index register for operands
    output logic       operand_present [5:0],     // Whether an operand is present
    output logic       reserved_fault             // Whether a reserved addressing mode fault occurred
);

    // Internal variables
    logic [8:0] opcode_internal;
    logic [5:0] offsets [5:0];
    logic       reserved_faults [5:0];

    // Instances of instruction ROM
    instruction_rom rom (
        .opcode(opcode_internal),
        .num_operands(num_operands),
        .base_data_type(),
        .operand_data_type(data_type),
        .access_mode()
    );

    // Instances of operand info and operand decoders
    genvar i;
    generate
        for (i = 0; i < 6; i++) begin: operand_loop
            logic [2:0] expected_data_size;
            logic       is_branch;
            logic       branch_offset_size;

            operand_info info (
                .data_type(data_type[i]),
                .access_mode(),
                .expected_data_size(expected_data_size),
                .is_branch(is_branch),
                .branch_offset_size(branch_offset_size)
            );

            operand_decoder decoder (
                .start_offset(offsets[i]),
                .expected_data_size(expected_data_size),
                .is_branch(is_branch),
                .branch_offset_size(branch_offset_size),
                .operands_remaining(num_operands - i),
                .instruction_buffer(instruction_buffer),
                .operand_bytes(operand_bytes[i]),
                .next_offset(offsets[i + 1]),
                .operands_remaining_out(),
                .reserved_fault(reserved_faults[i]),
                .addressing_mode(addressing_mode[i]),
                .reg_number(reg_number[i]),
                .data_offset(data_offset[i]),
                .indexed_mode(indexed_mode[i]),
                .index_reg(index_reg[i]),
                .operand_present(operand_present[i]),
                .literal_value(literal_value[i])
            );
        end
    endgenerate

    always_comb begin
        // Determine opcode
        if (instruction_buffer[start_offset] == 8'hFD) begin
            // Two-byte opcode
            opcode_internal = {1'b1, instruction_buffer[start_offset + 1]};
        end else begin
            // One-byte opcode
            opcode_internal = {1'b0, instruction_buffer[start_offset]};
        end
        opcode = opcode_internal;

        // Initialize offsets
        offsets[0] = start_offset + (opcode_internal[8] ? 2 : 1); // Account for opcode length

        // Compute total bytes and check for reserved faults
        total_bytes = offsets[num_operands] - start_offset;
        reserved_fault = |reserved_faults;
    end
endmodule
