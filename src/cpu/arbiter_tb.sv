module arbiter_tb;

    // Testbench clock and reset signals
    logic clk;
    logic reset;

    // Request and grant signals
    logic req_instr;
    logic req_exec;
    logic grant_instr;
    logic grant_exec;

    // Instantiate the arbiter module
    arbiter uut (
        .clk(clk),
        .reset(reset),
        .req_instr(req_instr),
        .req_exec(req_exec),
        .grant_instr(grant_instr),
        .grant_exec(grant_exec)
    );

    // Clock generation
    always #5 clk = ~clk;

    // Initial block to initialize signals and apply test vectors
    initial begin
        // Initialize clock and reset
        clk = 0;
        reset = 0;

        // Initialize request signals
        req_instr = 0;
        req_exec = 0;

        // Apply reset
        reset = 1;
        #10;
        reset = 0;

        // Test 1: No requests
        #10;
        assert(grant_instr == 0) else $error("Test 1 failed: grant_instr should be 0");
        assert(grant_exec == 0) else $error("Test 1 failed: grant_exec should be 0");

        // Test 2: Instruction unit request
        req_instr = 1;
        #10;
        assert(grant_instr == 1) else $error("Test 2 failed: grant_instr should be 1");
        assert(grant_exec == 0) else $error("Test 2 failed: grant_exec should be 0");

        // Test 3: Execution unit request while instruction unit is granted
        req_exec = 1;
        #10;
        assert(grant_instr == 1) else $error("Test 3 failed: grant_instr should be 1");
        assert(grant_exec == 0) else $error("Test 3 failed: grant_exec should be 0");

        // Test 4: Instruction unit releases request
        req_instr = 0;
        #10;
        assert(grant_instr == 0) else $error("Test 4 failed: grant_instr should be 0");
        assert(grant_exec == 1) else $error("Test 4 failed: grant_exec should be 1");

        // Test 5: Both units request simultaneously
        req_instr = 1;
        req_exec = 1;
        #10;
        assert(grant_instr == 1) else $error("Test 5 failed: grant_instr should be 1");
        assert(grant_exec == 0) else $error("Test 5 failed: grant_exec should be 0");

        // Test 6: Instruction unit releases request, execution unit granted
        req_instr = 0;
        #10;
        assert(grant_instr == 0) else $error("Test 6 failed: grant_instr should be 0");
        assert(grant_exec == 1) else $error("Test 6 failed: grant_exec should be 1");

        // Test 7: Both units release request
        req_exec = 0;
        #10;
        assert(grant_instr == 0) else $error("Test 7 failed: grant_instr should be 0");
        assert(grant_exec == 0) else $error("Test 7 failed: grant_exec should be 0");

        // Test 8: Reset while requests are active
        req_instr = 1;
        req_exec = 1;
        #10;
        reset = 1;
        #10;
        reset = 0;
        assert(grant_instr == 0) else $error("Test 8 failed: grant_instr should be 0 after reset");
        assert(grant_exec == 0) else $error("Test 8 failed: grant_exec should be 0 after reset");

        // Test 9: Behavior after reset
        #10;
        assert(grant_instr == 0) else $error("Test 9 failed: grant_instr should be 0 after reset");
        assert(grant_exec == 0) else $error("Test 9 failed: grant_exec should be 0 after reset");

        $display("All tests passed!");
        $finish;
    end

    // Monitor signals for debugging
    initial begin
        $monitor("Time: %0t | clk: %b | reset: %b | req_instr: %b | req_exec: %b | grant_instr: %b | grant_exec: %b",
                 $time, clk, reset, req_instr, req_exec, grant_instr, grant_exec);
    end

endmodule
