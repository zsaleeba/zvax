/**
 * @module arbiter
 * @brief Access Arbiter for Combined Instruction/Data Cache
 *
 * This module serves as an arbiter that controls access to a shared cache 
 * resource between an instruction unit and an execution unit. The arbiter 
 * ensures that only one unit can access the cache at any given time by 
 * managing request and grant signals.
 *
 * The arbiter operates based on a state machine with the following states:
 *  - IDLE: No access granted, waiting for requests.
 *  - GRANT_INSTR: Access granted to the instruction unit.
 *  - GRANT_EXEC: Access granted to the execution unit.
 *
 * The arbiter transitions between these states based on the request signals 
 * from the instruction and execution units and grants access accordingly.
 *
 * Parameters:
 *  - None
 *
 * Ports:
 *  - clk: Input clock signal
 *  - reset: Input reset signal
 *  - req_instr: Request signal from the instruction unit
 *  - req_exec: Request signal from the execution unit
 *  - grant_instr: Grant signal to the instruction unit, indicating access to the cache
 *  - grant_exec: Grant signal to the execution unit, indicating access to the cache
 *
 * The arbiter module comprises:
 * 1. State Machine: Controls the state transitions based on the request signals.
 * 2. Grant Logic: Asserts the appropriate grant signal based on the current state.
 *
 * The arbiter ensures fair and orderly access to the shared cache resource, 
 * preventing conflicts and ensuring data integrity.
 */
module arbiter(
    input logic clk,
    input logic reset,
    input logic req_instr,
    input logic req_exec,
    output logic grant_instr,
    output logic grant_exec
);
    // Arbiter state definition
    typedef enum logic [1:0] {
        IDLE,
        GRANT_INSTR,
        GRANT_EXEC
    } arbiter_state_t;

    // State registers
    arbiter_state_t state, next_state;

    // State transition logic
    always_ff @(posedge clk or posedge reset) begin
        if (reset) begin
            state <= IDLE;
        end else begin
            state <= next_state;
        end
    end

    // Next state logic and grant signal assignments
    always_comb begin
        // Default values
        grant_instr = 0;
        grant_exec = 0;
        next_state = state;

        case (state)
            IDLE: begin
                if (req_instr) begin
                    next_state = GRANT_INSTR;
                end else if (req_exec) begin
                    next_state = GRANT_EXEC;
                end
            end
            GRANT_INSTR: begin
                grant_instr = 1;
                if (!req_instr) begin
                    next_state = IDLE;
                end
            end
            GRANT_EXEC: begin
                grant_exec = 1;
                if (!req_exec) begin
                    next_state = IDLE;
                end
            end
        endcase
    end
endmodule
