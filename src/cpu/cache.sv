/* verilator lint_off UNUSED */
/**
 * @module cache
 * @brief 2-Way Set Associative Cache with Write-Through Policy
 *
 * This module implements a 2-way set associative cache with a write-through
 * policy and random replacement. It supports both read and write operations,
 * and is designed to be used as a combined instruction and data cache.
 *
 * The cache uses a Least Frequently Used (LFU) algorithm for replacement,
 * with the help of a Linear Feedback Shift Register (LFSR) to determine
 * the random way for replacement on a cache miss.
 *
 * Parameters:
 *  - CACHE_SIZE: Total cache size in bytes (default: 1024)
 *  - BLOCK_SIZE: Block size in bytes (default: 16)
 *  - ADDR_WIDTH: Address width (default: 32)
 *  - DATA_WIDTH: Data width (default: 32)
 *
 * Ports:
 *  - clk: Input clock signal
 *  - rst: Input reset signal
 *  - read: Input read signal (active high)
 *  - write: Input write signal (active high)
 *  - addr: Input address signal (ADDR_WIDTH bits)
 *  - wdata: Input write data signal (DATA_WIDTH bits)
 *  - rdata: Output read data signal (DATA_WIDTH bits)
 *  - hit: Output cache hit signal (active high)
 *
 * The cache module comprises:
 * 1. Tag Memory: Stores the tags for each cache block.
 * 2. Data Memory: Stores the data for each cache block.
 * 3. Valid Memory: Stores the validity of each cache block.
 * 4. LFSR: Generates a random number for random replacement.
 * 5. Read/Write Logic: Handles read and write operations based on the input signals.
 *
 * The cache operates as follows:
 * - On a read operation, it checks both ways for a valid tag match. If a match is found,
 *   it outputs the corresponding data and asserts the hit signal.
 * - On a write operation, it writes the data to the appropriate way if the tag matches.
 *   If there is no tag match, it performs a write-through operation to the random way.
 *
 * This cache module is suitable for use in a variety of applications where a small,
 * efficient, and fast cache is needed.
 */

module cache #(
    parameter CACHE_SIZE = 1024,  // Total cache size in bytes
    parameter BLOCK_SIZE = 16,    // Block size in bytes
    parameter ADDR_WIDTH = 32,    // Address width
    parameter DATA_WIDTH = 32     // Data width
) (
    input  logic                      clk,
    input  logic                      rst,
    input  logic                      read,
    input  logic                      write,
    input  logic [ADDR_WIDTH-1:0]     addr,
    input  logic [DATA_WIDTH-1:0]     wdata,
    output logic [DATA_WIDTH-1:0]     rdata,
    output logic                      hit
);

    // Derived parameters
    localparam NUM_BLOCKS = CACHE_SIZE / BLOCK_SIZE; // Total number of blocks in the cache
    localparam NUM_SETS = NUM_BLOCKS / 2;            // Number of sets in the cache (2-way associative)
    localparam INDEX_WIDTH = $clog2(NUM_SETS);       // Width of the index
    localparam TAG_WIDTH = ADDR_WIDTH - INDEX_WIDTH - $clog2(BLOCK_SIZE); // Width of the tag

    // Cache memory arrays
    logic [TAG_WIDTH-1:0] tag_mem[2][NUM_SETS];            // Tag memory array
    logic [DATA_WIDTH-1:0] data_mem[2][NUM_SETS][BLOCK_SIZE/4];  // Data memory array (assuming 4 bytes per word)
    logic valid_mem[2][NUM_SETS];                         // Valid bit memory array

    // Index and tag extraction
    logic [INDEX_WIDTH-1:0] index;  // Extracted index
    logic [TAG_WIDTH-1:0]   tag;    // Extracted tag

    // Compute the index and tag from the address
    assign index = addr[INDEX_WIDTH + $clog2(BLOCK_SIZE) - 1 : $clog2(BLOCK_SIZE)];
    assign tag = addr[ADDR_WIDTH-1:INDEX_WIDTH + $clog2(BLOCK_SIZE)];

    // LFSR for random replacement logic
    logic [3:0] lfsr;    // Linear Feedback Shift Register
    logic rand_way;      // Random way for replacement

    // LFSR logic for random number generation
    always_ff @(posedge clk or posedge rst) begin
        if (rst)
            lfsr <= 4'b0001;  // Non-zero seed
        else
            lfsr <= {lfsr[2:0], lfsr[3] ^ lfsr[2]}; // LFSR feedback logic
    end

    // Determine the random way based on the LFSR output
    assign rand_way = lfsr[0];

    // Cache read/write logic
    integer i;  // Loop variable

    always_ff @(posedge clk or posedge rst) begin
        if (rst) begin
            // Reset cache: Clear valid bits for both ways in all sets
            for (i = 0; i < NUM_SETS; i++) begin
                valid_mem[0][i] <= 0;  // Clear valid bit for way 0
                valid_mem[1][i] <= 0;  // Clear valid bit for way 1
            end
            hit <= 0;  // Clear the hit signal
        end else if (read) begin
            // Cache read operation
            if (valid_mem[0][index] && tag_mem[0][index] == tag) begin
                rdata <= data_mem[0][index][addr[$clog2(BLOCK_SIZE)-1:2]]; // Output data for way 0
                hit <= 1;  // Set hit signal
            end else if (valid_mem[1][index] && tag_mem[1][index] == tag) begin
                rdata <= data_mem[1][index][addr[$clog2(BLOCK_SIZE)-1:2]]; // Output data for way 1
                hit <= 1;  // Set hit signal
            end else begin
                hit <= 0;  // Clear hit signal if no valid tag match
            end
        end else if (write) begin
            // Cache write-through operation
            if (valid_mem[0][index] && tag_mem[0][index] == tag) begin
                data_mem[0][index][addr[$clog2(BLOCK_SIZE)-1:2]] <= wdata; // Write data to way 0
                hit <= 1;  // Set hit signal
            end else if (valid_mem[1][index] && tag_mem[1][index] == tag) begin
                data_mem[1][index][addr[$clog2(BLOCK_SIZE)-1:2]] <= wdata; // Write data to way 1
                hit <= 1;  // Set hit signal
            end else begin
                // Write miss: Replace a random block
                if (rand_way) begin
                    tag_mem[0][index] <= tag;  // Update tag for way 0
                    data_mem[0][index][addr[$clog2(BLOCK_SIZE)-1:2]] <= wdata; // Write data to way 0
                    valid_mem[0][index] <= 1;  // Set valid bit for way 0
                end else begin
                    tag_mem[1][index] <= tag;  // Update tag for way 1
                    data_mem[1][index][addr[$clog2(BLOCK_SIZE)-1:2]] <= wdata; // Write data to way 1
                    valid_mem[1][index] <= 1;  // Set valid bit for way 1
                end
                hit <= 0;  // Clear hit signal
            end
        end
    end

endmodule
