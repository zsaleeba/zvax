/**
 * @module instruction_buffer
 * @brief Instruction Buffer with Cache Interface
 *
 * This module implements an instruction buffer that can be parameterized
 * with the maximum number of bytes it can contain. The buffer supports
 * reloading from a specified address, advancing by a given number of bytes,
 * and interacting with a combined cache to fetch instruction data.
 *
 * Parameters:
 *  - MAX_BYTES: Maximum number of bytes the buffer can hold (default: 12)
 *
 * Inputs:
 *  - clk: Clock signal
 *  - reset: Reset signal (active high)
 *  - reload: Signal to clear the buffer and start reloading from load_address
 *  - load_address: Address from which to start loading instructions
 *  - advance: Signal to advance the buffer by the specified number of bytes
 *  - advance_byte_count: Number of bytes to advance the buffer by
 *
 * Outputs:
 *  - instr_buffer: Up to MAX_BYTES next bytes of an instruction
 *  - full_count: Number of bytes currently filled in the instruction buffer
 *
 * Cache Interface:
 *  - cache_hit: Signal indicating a successful cache read
 *  - cache_data: Data read from the cache
 *  - cache_address: Address to read from the cache
 *  - cache_read: Signal to initiate a cache read
 *
 * Internal Operation:
 *  - When reload is asserted, the buffer is cleared, and reloading starts
 *    from the specified load_address.
 *  - When advance is asserted, the buffer is shifted down by the specified
 *    number of bytes, and more data is fetched from the cache if needed.
 *  - The buffer interacts with the cache to load instruction data, handling
 *    cache hits and misses appropriately.
 *
 * The instruction buffer ensures that up to MAX_BYTES of instruction data
 * are available for decoding, handling reloading and advancing operations
 * as needed.
 */

module instruction_buffer #(
    parameter MAX_BYTES = 12
)(
    input logic clk,
    input logic reset,
    input logic reload,
    input logic [31:0] load_address,
    input logic advance,
    input logic [$clog2(MAX_BYTES):0] advance_byte_count,

    output logic [MAX_BYTES*8-1:0] instr_buffer,
    output logic [$clog2(MAX_BYTES+1):0] full_count,

    // Interface to combined cache
    input logic cache_hit,
    input logic [31:0] cache_data,
    output logic [31:0] cache_address,
    output logic cache_read
);

    // Internal registers
    logic [MAX_BYTES*8-1:0] buffer;
    logic [$clog2(MAX_BYTES+1):0] bytes_filled;
    logic [31:0] current_address;
    logic reloading;

    // Initialize registers
    always_ff @(posedge clk or posedge reset) begin
        if (reset) begin
            buffer <= 0;
            bytes_filled <= 0;
            current_address <= 0;
            reloading <= 0;
            cache_read <= 0;
            cache_address <= 0;
        end else begin
            if (reload) begin
                // Start reloading from the specified load_address
                buffer <= 0;
                bytes_filled <= 0;
                current_address <= load_address;
                reloading <= 1;
                cache_read <= 1;
                cache_address <= load_address;
            end else if (reloading) begin
                // Continue reloading data from the cache
                if (cache_hit) begin
                    buffer <= (buffer << 32) | cache_data;
                    bytes_filled <= bytes_filled + 4;
                    current_address <= current_address + 4;
                    cache_address <= current_address + 4;
                    if (bytes_filled + 4 >= MAX_BYTES) begin
                        reloading <= 0;
                        cache_read <= 0;
                    end
                end
            end else if (advance) begin
                // Advance the buffer by the specified number of bytes
                if (advance_byte_count <= bytes_filled) begin
                    buffer <= buffer >> (advance_byte_count * 8);
                    bytes_filled <= bytes_filled - advance_byte_count;
                end else begin
                    buffer <= 0;
                    bytes_filled <= 0;
                end

                // Request more data from the cache if needed
                if (bytes_filled < MAX_BYTES) begin
                    cache_read <= 1;
                    cache_address <= current_address;
                    reloading <= 1;
                end
            end
        end
    end

    // Output assignments
    assign instr_buffer = buffer;
    assign full_count = bytes_filled;

endmodule
