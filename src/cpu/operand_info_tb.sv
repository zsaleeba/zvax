module operand_info_tb;

    // Test bench inputs
    logic [3:0] data_type;
    logic [2:0] access_mode;

    // Test bench outputs
    logic [2:0] expected_data_size;
    logic       is_branch;
    logic       branch_offset_size;

    // Instantiate the operand_info module
    operand_info uut (
        .data_type(data_type),
        .access_mode(access_mode),
        .expected_data_size(expected_data_size),
        .is_branch(is_branch),
        .branch_offset_size(branch_offset_size)
    );

    // Task to display output values
    task display_outputs();
        $display("data_type: %h, access_mode: %h", data_type, access_mode);
        $display("expected_data_size: %h, is_branch: %b, branch_offset_size: %b",
                 expected_data_size, is_branch, branch_offset_size);
        $display("--------------------------------------------------");
    endtask

    // Initial block for simulation
    initial begin
        $display("Starting operand_info testbench...");

        // Test Case 1: Byte data type, UNUSED access mode
        $display("Test Case 1: Byte data type, UNUSED access mode");
        data_type = 4'h0;
        access_mode = 3'h0;
        #1;
        display_outputs();
        assert(expected_data_size == 3'h0);
        assert(is_branch == 1'b0);
        assert(branch_offset_size == 1'b0);

        // Test Case 2: Word data type, READ access mode
        $display("Test Case 2: Word data type, READ access mode");
        data_type = 4'h1;
        access_mode = 3'h1;
        #1;
        display_outputs();
        assert(expected_data_size == 3'h1);
        assert(is_branch == 1'b0);
        assert(branch_offset_size == 1'b0);

        // Test Case 3: Longword data type, WRITE access mode
        $display("Test Case 3: Longword data type, WRITE access mode");
        data_type = 4'h2;
        access_mode = 3'h2;
        #1;
        display_outputs();
        assert(expected_data_size == 3'h2);
        assert(is_branch == 1'b0);
        assert(branch_offset_size == 1'b0);

        // Test Case 4: Quadword data type, MODIFY access mode
        $display("Test Case 4: Quadword data type, MODIFY access mode");
        data_type = 4'h3;
        access_mode = 3'h3;
        #1;
        display_outputs();
        assert(expected_data_size == 3'h3);
        assert(is_branch == 1'b0);
        assert(branch_offset_size == 1'b0);

        // Test Case 5: Octaword data type, ADDRESS access mode
        $display("Test Case 5: Octaword data type, ADDRESS access mode");
        data_type = 4'h4;
        access_mode = 3'h6;
        #1;
        display_outputs();
        assert(expected_data_size == 3'h4);
        assert(is_branch == 1'b0);
        assert(branch_offset_size == 1'b0);

        // Test Case 6: F_floating data type, BRANCH (byte size) access mode
        $display("Test Case 6: F_floating data type, BRANCH (byte size) access mode");
        data_type = 4'h5;
        access_mode = 3'h4;
        #1;
        display_outputs();
        assert(expected_data_size == 3'h0); // Size irrelevant for branch
        assert(is_branch == 1'b1);
        assert(branch_offset_size == 1'b0);

        // Test Case 7: D_floating data type, BRANCH (word size) access mode
        $display("Test Case 7: D_floating data type, BRANCH (word size) access mode");
        data_type = 4'h6;
        access_mode = 3'h5;
        #1;
        display_outputs();
        assert(expected_data_size == 3'h0); // Size irrelevant for branch
        assert(is_branch == 1'b1);
        assert(branch_offset_size == 1'b1);

        // Test Case 8: G_floating data type, V_ADDRESS access mode
        $display("Test Case 8: G_floating data type, V_ADDRESS access mode");
        data_type = 4'h7;
        access_mode = 3'h7;
        #1;
        display_outputs();
        assert(expected_data_size == 3'h3);
        assert(is_branch == 1'b0);
        assert(branch_offset_size == 1'b0);

        // Test Case 9: Variable length bitfield data type, READ access mode
        $display("Test Case 9: Variable length bitfield data type, READ access mode");
        data_type = 4'h9;
        access_mode = 3'h1;
        #1;
        display_outputs();
        assert(expected_data_size == 3'h2); // Treated as Longword
        assert(is_branch == 1'b0);
        assert(branch_offset_size == 1'b0);

        // Test Case 10: Trailing numeric string data type, MODIFY access mode
        $display("Test Case 10: Trailing numeric string data type, MODIFY access mode");
        data_type = 4'hA;
        access_mode = 3'h3;
        #1;
        display_outputs();
        assert(expected_data_size == 3'h2); // Treated as Longword
        assert(is_branch == 1'b0);
        assert(branch_offset_size == 1'b0);

        // Test Case 11: Leading separate numeric string data type, ADDRESS access mode
        $display("Test Case 11: Leading separate numeric string data type, ADDRESS access mode");
        data_type = 4'hB;
        access_mode = 3'h6;
        #1;
        display_outputs();
        assert(expected_data_size == 3'h2); // Treated as Longword
        assert(is_branch == 1'b0);
        assert(branch_offset_size == 1'b0);

        // Test Case 12: Packed decimal string data type, WRITE access mode
        $display("Test Case 12: Packed decimal string data type, WRITE access mode");
        data_type = 4'hC;
        access_mode = 3'h2;
        #1;
        display_outputs();
        assert(expected_data_size == 3'h2); // Treated as Longword
        assert(is_branch == 1'b0);
        assert(branch_offset_size == 1'b0);

        // Test Case 13: Custom data type and access mode
        $display("Test Case 13: Custom data type and access mode");
        data_type = 4'hF; // Custom data type
        access_mode = 3'h0; // CUSTOM access mode
        #1;
        display_outputs();
        assert(expected_data_size == 3'h0); // Defaults to Byte
        assert(is_branch == 1'b0);
        assert(branch_offset_size == 1'b0);

        $display("All test cases completed.");
        $finish;
    end

endmodule
