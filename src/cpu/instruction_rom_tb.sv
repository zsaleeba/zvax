module instruction_rom_tb;

    // Inputs
    logic [8:0] opcode;

    // Outputs
    logic [2:0] num_operands;
    logic [3:0] base_data_type;
    logic [3:0] operand_data_type [5:0];
    logic [2:0] access_mode [5:0];

    // Instantiate the instruction_rom module
    instruction_rom uut (
        .opcode(opcode),
        .num_operands(num_operands),
        .base_data_type(base_data_type),
        .operand_data_type(operand_data_type),
        .access_mode(access_mode)
    );

    // Task to display output values
    task display_outputs();
        $display("Opcode: %h", opcode);
        $display("Number of Operands: %0d", num_operands);
        $display("Base Data Type: %0d", base_data_type);
        for (int i = 0; i < 6; i++) begin
            $display("Operand %0d Data Type: %0d", i, operand_data_type[i]);
            $display("Operand %0d Access Mode: %0d", i, access_mode[i]);
        end
        $display("--------------------------------------------------");
    endtask

    // Initial block for simulation
    initial begin
        $display("Starting instruction_rom testbench...");

        // Test Case 1: Opcode 0x000
        $display("Test Case 1: Opcode 0x000");
        opcode = 9'h000;
        #1;
        display_outputs();
        assert(num_operands == 3'd2);
        assert(base_data_type == 4'h2); // Longword
        assert(operand_data_type[0] == 4'h2);
        assert(access_mode[0] == 3'h1); // READ
        assert(operand_data_type[1] == 4'h2);
        assert(access_mode[1] == 3'h2); // WRITE

        // Test Case 2: Opcode 0x001
        $display("Test Case 2: Opcode 0x001");
        opcode = 9'h001;
        #1;
        display_outputs();
        assert(num_operands == 3'd1);
        assert(base_data_type == 4'h3); // Quadword
        assert(operand_data_type[0] == 4'h3);
        assert(access_mode[0] == 3'h1); // READ

        // Test Case 3: Opcode 0x002 (default case)
        $display("Test Case 3: Opcode 0x002");
        opcode = 9'h002;
        #1;
        display_outputs();
        assert(num_operands == 3'd0); // Default case
        assert(base_data_type == 4'h0); // Default
        assert(operand_data_type[0] == 4'h0); // Default
        assert(access_mode[0] == 3'h0); // Default

        // Test Case 4: Opcode 0x100 (Two-byte opcode)
        $display("Test Case 4: Opcode 0x100");
        opcode = 9'h100;
        #1;
        display_outputs();
        // Fill in expected values based on the configuration in instruction_rom

        // Test Case 5: Opcode 0x1FF (boundary check)
        $display("Test Case 5: Opcode 0x1FF");
        opcode = 9'h1FF;
        #1;
        display_outputs();
        // Fill in expected values based on the configuration in instruction_rom

        // Additional test cases can be added here to cover all expected opcodes

        $display("All test cases completed.");
        $finish;
    end

endmodule
