/*
================================================================================
                                UART Transmitter Module
================================================================================
Description:
------------
This module implements a Universal Asynchronous Receiver/Transmitter (UART) 
transmitter with parameterizable word width, buffer length, clock frequency, 
and baud rate. The transmitter operates synchronously with a single clock 
signal and supports independent append operations for queuing data to be 
transmitted. The module uses an external FIFO module for buffering data before 
transmission. The module also provides status flags to indicate when the FIFO 
is full or empty and outputs the serialized data stream through the `uart_tx` 
signal.

Parameters:
-----------
- WORD_SIZE (default: 8 bits):
    Defines the width of the data word to be transmitted. The width of the 
    input data (`din`) is determined by this parameter.

- BUFFER_LEN (default: 2048 words):
    Defines the depth of the FIFO buffer used for storing data before it is 
    transmitted. The buffer can hold up to `BUFFER_LEN` elements.

- CLOCK_HZ (default: 50_000_000 Hz):
    Defines the frequency of the system clock in Hertz. This is used to 
    calculate the required timing for the specified baud rate.

- BPS (default: 115200 bits per second):
    Defines the baud rate for the UART transmission, specifying the number of 
    bits transmitted per second.

Inputs:
-------
- clk (1 bit):
    The clock signal. All operations (append and transmission) are synchronized 
    to the rising edge of this clock.

- reset (1 bit):
    Active-high reset signal. When asserted, this signal resets the UART 
    transmitter, clearing all stored data, and resetting the state machine.

- din (WORD_SIZE bits):
    Data input bus. This signal provides the data to be stored in the FIFO 
    buffer for transmission.

- append (1 bit):
    Append control signal. When this signal is asserted (high) and the FIFO is 
    not full, the data on the `din` bus is written into the FIFO.

Outputs:
--------
- uart_tx (1 bit):
    UART transmit data signal. This output carries the serialized data stream, 
    which includes the start bit, data bits, and stop bit.

- full (1 bit):
    Full status flag. This signal is asserted (high) when the FIFO is full, 
    indicating that no more data can be written until some data is transmitted.

- empty (1 bit):
    Empty status flag. This signal is asserted (high) when the FIFO is empty, 
    indicating that no data is available for transmission.

Operation:
----------
The UART transmitter operates by using an external FIFO buffer to store data 
words for transmission. The data is serialized and transmitted bit by bit, 
including a start bit, the data bits, and a stop bit. The module includes a 
state machine that controls the transmission timing according to the specified 
baud rate.

- FIFO Buffer:
  The FIFO buffer is instantiated as a separate module. The buffer uses 
  standard FIFO read and write operations to manage the data to be transmitted.

- Transmission Logic:
  The transmission logic includes a state machine that manages the 
  serialization of data and the timing of each bit according to the specified 
  baud rate. The `uart_tx` signal is asserted low for the start bit, then 
  outputs the data bits, and finally asserts high for the stop bit.

- Full and Empty Flags:
  The `full` and `empty` flags are connected to the corresponding flags of the 
  FIFO module, providing status information about the buffer.

This UART transmitter module is suitable for applications requiring serial 
communication with configurable data word length, buffer depth, and baud rate.
*/

module uart_tx #(
    parameter WORD_SIZE = 8,
    parameter BUFFER_LEN = 2048,
    parameter CLOCK_HZ = 50_000_000,
    parameter BPS = 115200
)(
    input logic clk,
    input logic reset,
    input logic [WORD_SIZE-1:0] din,
    input logic append,
    output logic uart_tx,
    output logic full,
    output logic empty
);

    // Internal parameters
    localparam TICKS_PER_BIT = CLOCK_HZ / BPS;

    // Internal signals
    logic [WORD_SIZE:0] shift_reg;
    logic [15:0] bit_counter;
    logic sending;
    logic [3:0] bit_pos;

    // Instantiate the FIFO module
    fifo #(
        .WORD_SIZE(WORD_SIZE),
        .BUFFER_LEN(BUFFER_LEN)
    ) fifo_inst (
        .clk(clk),
        .reset(reset),
        .din(din),
        .append(append),
        .remove(remove),
        .dout(fifo_dout),
        .full(full),
        .empty(empty)
    );

    // Read and transmission logic
    always_ff @(posedge clk or posedge reset) begin
        if (reset) begin
            shift_reg <= {WORD_SIZE+1{1'b1}};  // idle (stop bit)
            bit_counter <= 0;
            bit_pos <= 0;
            sending <= 0;
            uart_tx <= 1;
        end else begin
            if (!sending && !empty) begin
                // Load the shift register with start bit, data bits, stop bit
                shift_reg <= {1'b1, fifo_dout, 1'b0};  // LSB first
                bit_counter <= TICKS_PER_BIT;
                bit_pos <= 0;
                sending <= 1;
                remove <= 1;  // Remove the item from FIFO
            end else begin
                remove <= 0;
            end

            if (sending) begin
                if (bit_counter == 0) begin
                    uart_tx <= shift_reg[0];
                    shift_reg <= {1'b1, shift_reg[WORD_SIZE:1]};  // Shift right
                    bit_counter <= TICKS_PER_BIT;
                    bit_pos <= bit_pos + 1;

                    if (bit_pos == WORD_SIZE + 1) begin  // Stop bit sent
                        sending <= 0;
                    end
                end else begin
                    bit_counter <= bit_counter - 1;
                end
            end else begin
                uart_tx <= 1;  // Idle state
            end
        end
    end

endmodule
