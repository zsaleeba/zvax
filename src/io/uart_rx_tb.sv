module uart_rx_tb;

    // Parameters
    parameter WORD_SIZE = 8;
    parameter BUFFER_LEN = 64;
    parameter CLOCK_HZ = 50000000;
    parameter BPS = 115200;
    parameter OVERSAMPLE = 16;

    // Signals
    logic clk;
    logic reset;
    logic uart_rx;
    logic remove;
    logic [WORD_SIZE-1:0] dout;
    logic full;
    logic empty;

    // Instantiate the UART receiver
    uart_rx #(
        .WORD_SIZE(WORD_SIZE),
        .BUFFER_LEN(BUFFER_LEN),
        .CLOCK_HZ(CLOCK_HZ),
        .BPS(BPS),
        .OVERSAMPLE(OVERSAMPLE)
    ) uut (
        .clk(clk),
        .reset(reset),
        .uart_rx(uart_rx),
        .remove(remove),
        .dout(dout),
        .full(full),
        .empty(empty)
    );

    // Clock generation
    always #5 clk = ~clk;  // 10ns clock period (100 MHz)

    // Test vectors
    initial begin
        // Initialize signals
        clk = 0;
        reset = 0;
        uart_rx = 1;  // UART idle state is high
        remove = 0;

        // Apply reset
        $display("Applying reset...");
        reset = 1;
        #20;
        reset = 0;
        #20;
        assert(empty == 1) else $fatal("FIFO should be empty after reset");
        assert(full == 0) else $fatal("FIFO should not be full after reset");
        $display("Reset test passed");

        // Send a single byte (0x55) via UART
        $display("Sending byte 0x55...");
        uart_send_byte(8'h55);  // 0x55 = 0b01010101
        #((10 * OVERSAMPLE * CLOCK_HZ) / BPS);
        assert(dout == 8'h55) else $fatal("Received data mismatch for byte 0x55");
        assert(empty == 0) else $fatal("FIFO should not be empty after receiving data");
        remove = 1;
        #10 remove = 0;
        #10;
        assert(empty == 1) else $fatal("FIFO should be empty after removing data");
        $display("Single byte test passed");

        // Send multiple bytes (0xA5, 0x3C) via UART
        $display("Sending multiple bytes 0xA5, 0x3C...");
        uart_send_byte(8'hA5);  // 0xA5 = 0b10100101
        uart_send_byte(8'h3C);  // 0x3C = 0b00111100
        #((20 * OVERSAMPLE * CLOCK_HZ) / BPS);
        assert(dout == 8'hA5) else $fatal("Received data mismatch for byte 0xA5");
        remove = 1;
        #10 remove = 0;
        #10;
        assert(dout == 8'h3C) else $fatal("Received data mismatch for byte 0x3C");
        remove = 1;
        #10 remove = 0;
        #10;
        assert(empty == 1) else $fatal("FIFO should be empty after receiving data");
        $display("Multiple bytes test passed");

        // Test with noise in the middle of the data bit
        $display("Testing noise handling...");
        uart_rx = 0;  // Start bit
        #(SAMPLE_THRESHOLD * (OVERSAMPLE/2));
        uart_rx = 1;  // Introduce noise (incorrect bit value)
        #(SAMPLE_THRESHOLD * (OVERSAMPLE/2));
        uart_rx = 0;  // Return to correct bit value
        uart_send_bits(8'b11001100);  // Data bits
        uart_rx = 1;  // Stop bit
        #((10 * OVERSAMPLE * CLOCK_HZ) / BPS);
        assert(dout == 8'hCC) else $fatal("Received data mismatch with noise handling");
        remove = 1;
        #10 remove = 0;
        #10;
        assert(empty == 1) else $fatal("FIFO should be empty after noise test");
        $display("Noise handling test passed");

        // Overflow test
        $display("Testing FIFO overflow...");
        for (int i = 0; i < BUFFER_LEN; i++) begin
            uart_send_byte(i);
        end
        #((BUFFER_LEN * 10 * OVERSAMPLE * CLOCK_HZ) / BPS);
        assert(full == 1) else $fatal("FIFO should be full after overflow test");
        $display("Overflow test passed");

        $display("All tests passed successfully!");
        $finish;
    end

    // Task to send a byte via UART (including start, data, and stop bits)
    task uart_send_byte(input [7:0] byte);
        uart_rx = 0;  // Start bit
        #(SAMPLE_THRESHOLD * OVERSAMPLE);
        for (int i = 0; i < 8; i++) begin
            uart_rx = byte[i];
            #(SAMPLE_THRESHOLD * OVERSAMPLE);
        end
        uart_rx = 1;  // Stop bit
        #(SAMPLE_THRESHOLD * OVERSAMPLE);
    endtask

    // Task to send bits for custom data transmission
    task uart_send_bits(input [7:0] bits);
        for (int i = 0; i < 8; i++) begin
            uart_rx = bits[i];
            #(SAMPLE_THRESHOLD * OVERSAMPLE);
        end
    endtask

endmodule
