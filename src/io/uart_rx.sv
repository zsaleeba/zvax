/*
================================================================================
                                UART Receiver Module
================================================================================
Description:
------------
This module implements a Universal Asynchronous Receiver/Transmitter (UART) 
receiver with parameterizable word width, clock frequency, and baud rate. The 
receiver operates synchronously with a single clock signal and employs 16x 
sampling per bit to minimize the effects of noise and ensure accurate data 
reception. The received bytes are buffered in a FIFO, allowing for flexible 
data retrieval. The module also provides status flags to indicate when data is 
available in the FIFO and when an error occurs.

Parameters:
-----------
- WORD_SIZE (default: 8 bits):
    Defines the width of the data word to be received. The width of the output 
    data (`dout`) is determined by this parameter.

- BUFFER_LEN (default: 2048 words):
    Defines the depth of the FIFO buffer used for storing received data.

- CLOCK_HZ (default: 50_000_000 Hz):
    Defines the frequency of the system clock in Hertz. This is used to 
    calculate the required timing for the specified baud rate.

- BPS (default: 115200 bits per second):
    Defines the baud rate for the UART reception, specifying the number of 
    bits received per second.

Inputs:
-------
- clk (1 bit):
    The clock signal. All operations (sampling and data retrieval) are 
    synchronized to the rising edge of this clock.

- reset (1 bit):
    Active-high reset signal. When asserted, this signal resets the UART 
    receiver, clearing all stored data and resetting the state machine.

- uart_rx (1 bit):
    UART receive data signal. This input carries the serialized data stream, 
    which includes the start bit, data bits, and stop bit.

Outputs:
--------
- dout (WORD_SIZE bits):
    Data output bus. This signal carries the data word retrieved from the FIFO.

- data_ready (1 bit):
    Data ready flag. This signal is asserted (high) when there is data 
    available in the FIFO, ready to be read via the `dout` bus.

- error (1 bit):
    Error flag. This signal is asserted (high) if a framing error is detected, 
    indicating that the stop bit was not correctly received.

- full (1 bit):
    Full status flag. This signal is asserted (high) when the FIFO is full, 
    indicating that no more data can be written until some data is read.

- empty (1 bit):
    Empty status flag. This signal is asserted (high) when the FIFO is empty, 
    indicating that no data is available for reading.

Operation:
----------
The UART receiver operates by sampling the incoming `uart_rx` signal 16 times 
per bit to ensure accurate detection of the start bit, data bits, and stop bit. 
A state machine controls the reception process, including the detection of the 
start bit, sampling of each data bit, and verification of the stop bit. 
Received bytes are stored in a FIFO for subsequent retrieval.

- Sampling Logic:
  The receiver samples the `uart_rx` signal at 16x the baud rate. The majority 
  of the samples within each bit period determines the actual value of the bit 
  to minimize the impact of noise.

- State Machine:
  The state machine controls the reception process, starting with the 
  detection of the start bit, followed by the sequential reception of the data 
  bits, and ending with the verification of the stop bit. If a framing error 
  is detected (i.e., the stop bit is not correctly received), the `error` flag 
  is asserted.

- FIFO Buffer:
  The received bytes are stored in a FIFO buffer to allow for flexible data 
  retrieval. The `full` and `empty` flags are used to monitor the status of 
  the FIFO.

This UART receiver module is suitable for applications requiring reliable 
serial communication with configurable data word length, baud rate, and 
buffering capability.
*/

module uart_rx #(
    parameter WORD_SIZE = 8,               // Number of data bits
    parameter BUFFER_LEN = 64,             // FIFO buffer length
    parameter CLOCK_HZ = 50000000,         // System clock frequency
    parameter BPS = 115200,                // Baud rate
    parameter OVERSAMPLE = 16              // Oversampling ratio
)(
    input logic clk,                       // System clock
    input logic reset,                     // Active-high reset
    input logic uart_rx,                   // UART receive line
    input logic remove,                    // Remove control signal (FIFO-style)
    output logic [WORD_SIZE-1:0] dout,     // Data output bus
    output logic full,                     // Full status flag (FIFO-style)
    output logic empty                     // Empty status flag (FIFO-style)
);

    // Internal parameters
    localparam SAMPLE_THRESHOLD = CLOCK_HZ / (BPS * OVERSAMPLE);

    // State encoding
    typedef enum logic [1:0] {
        IDLE = 2'b00,
        START_BIT = 2'b01,
        DATA_BITS = 2'b10,
        STOP_BIT = 2'b11
    } state_t;

    state_t state, next_state;

    // Internal signals
    logic [WORD_SIZE-1:0] shift_reg;
    logic [$clog2(SAMPLE_THRESHOLD)-1:0] sample_timer;
    logic [$clog2(OVERSAMPLE)-1:0] sample_count;
    logic [$clog2(WORD_SIZE+1)-1:0] bit_count;
    logic [$clog2(OVERSAMPLE+1)-1:0] sample_sum; // Sum of samples
    logic write_to_fifo;

    // Instantiate the FIFO module
    fifo #(
        .WORD_BITS(WORD_SIZE),
        .BUFFER_LEN(BUFFER_LEN)
    ) fifo_inst (
        .clk(clk),
        .reset(reset),
        .din(shift_reg),
        .append(write_to_fifo),
        .remove(remove),
        .dout(dout),
        .full(full),
        .empty(empty)
    );

    // State machine and sampling logic
    always_ff @(posedge clk or posedge reset) begin
        if (reset) begin
            state <= IDLE;
            sample_timer <= 0;
            sample_count <= 0;
            bit_count <= 0;
            shift_reg <= 0;
            sample_sum <= 0;
            write_to_fifo <= 0;
        end else begin
            write_to_fifo <= 0;  // Default to no write
            sample_timer <= sample_timer + 1;

            if (sample_timer == SAMPLE_THRESHOLD - 1) begin
                sample_timer <= 0;
                sample_count <= sample_count + 1;

                case (state)
                    IDLE: begin
                        if (uart_rx == 0) begin  // Start bit detected
                            sample_count <= 0;
                            sample_sum <= 0;
                            next_state <= START_BIT;
                        end
                    end

                    START_BIT: begin
                        sample_sum <= sample_sum + uart_rx;

                        if (sample_count == OVERSAMPLE - 1) begin
                            // Majority voting: If more than half of the samples are 1, the bit is 1
                            if (sample_sum >= (OVERSAMPLE >> 1)) begin
                                next_state <= DATA_BITS;
                            end else begin
                                next_state <= IDLE;  // False start, return to IDLE
                            end
                            sample_sum <= 0;
                            bit_count <= 0;
                        end
                    end

                    DATA_BITS: begin
                        sample_sum <= sample_sum + uart_rx;

                        if (sample_count == OVERSAMPLE - 1) begin
                            // Majority voting: If more than half of the samples are 1, the bit is 1
                            shift_reg <= { (sample_sum >= (OVERSAMPLE >> 1)), shift_reg[WORD_SIZE-1:1] };
                            sample_sum <= 0;
                            sample_count <= 0;
                            bit_count <= bit_count + 1;

                            if (bit_count == WORD_SIZE - 1) begin
                                next_state <= STOP_BIT;
                            end
                        end
                    end

                    STOP_BIT: begin
                        sample_sum <= sample_sum + uart_rx;

                        if (sample_count == OVERSAMPLE - 1) begin
                            // Majority voting for the stop bit
                            if (sample_sum >= (OVERSAMPLE >> 1)) begin
                                // Stop bit valid, write to FIFO
                                if (!full) begin
                                    write_to_fifo <= 1;  // Write the received word to the FIFO
                                end
                            end
                            next_state <= IDLE;
                            sample_sum <= 0;
                        end
                    end
                endcase
            end
        end
    end

endmodule
