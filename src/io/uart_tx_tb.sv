module tb_uart_tx;

    // Parameters
    parameter WORD_SIZE = 8;
    parameter BUFFER_LEN = 2048;
    parameter CLOCK_HZ = 50_000_000;
    parameter BPS = 115200;
    localparam TICKS_PER_BIT = CLOCK_HZ / BPS;

    // Signals
    logic clk;
    logic reset;
    logic [WORD_SIZE-1:0] din;
    logic append;
    logic uart_tx;
    logic full;
    logic empty;

    // DUT (Device Under Test)
    uart_tx #(
        .WORD_SIZE(WORD_SIZE),
        .BUFFER_LEN(BUFFER_LEN),
        .CLOCK_HZ(CLOCK_HZ),
        .BPS(BPS)
    ) dut (
        .clk(clk),
        .reset(reset),
        .din(din),
        .append(append),
        .uart_tx(uart_tx),
        .full(full),
        .empty(empty)
    );

    // Clock generation
    always #10 clk = ~clk;  // 50 MHz clock

    // Testbench procedure
    initial begin
        // Initialize signals
        clk = 0;
        reset = 0;
        din = 0;
        append = 0;

        // Apply reset
        $display("Applying reset...");
        reset = 1;
        #20;
        reset = 0;
        #20;
        if (empty !== 1 || full !== 0) $fatal("Initial state failed!");

        // Test case 1: Transmit a single byte
        $display("Test Case 1: Transmitting a single byte...");
        din = 8'hA5;  // Example byte
        append = 1;
        #20;
        append = 0;

        // Wait for transmission
        wait(empty === 1);
        #10;
        if (uart_tx !== 1) $fatal("Test Case 1 failed! UART did not return to idle.");

        // Test case 2: Transmit multiple bytes
        $display("Test Case 2: Transmitting multiple bytes...");
        for (int i = 0; i < 10; i++) begin
            din = i;
            append = 1;
            #20;
            append = 0;
            #20;
        end

        // Wait for all bytes to be transmitted
        wait(empty === 1);
        #10;
        if (uart_tx !== 1) $fatal("Test Case 2 failed! UART did not return to idle.");

        // Test case 3: Buffer full condition
        $display("Test Case 3: Buffer full condition...");
        for (int i = 0; i < BUFFER_LEN; i++) begin
            din = i;
            append = 1;
            #20;
            append = 0;
            #20;
        end
        if (full !== 1) $fatal("Test Case 3 failed! Buffer did not report full condition.");

        // Wait for transmission to start and clear the buffer
        wait(empty === 1);
        #10;
        if (uart_tx !== 1 || full !== 0) $fatal("Test Case 3 failed! Buffer did not clear properly.");

        // Test case 4: Reset during transmission
        $display("Test Case 4: Reset during transmission...");
        din = 8'hFF;  // Arbitrary data
        append = 1;
        #20;
        append = 0;

        #100;  // Wait for a few bits to be transmitted
        reset = 1;
        #20;
        reset = 0;

        if (uart_tx !== 1 || empty !== 1 || full !== 0) $fatal("Test Case 4 failed! Reset during transmission did not behave correctly.");

        $display("All test cases passed!");
        $stop;
    end

endmodule
