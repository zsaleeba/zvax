module fifo_tb;

    // Parameters
    parameter WORD_BITS = 8;
    parameter BUFFER_LEN = 4; // Using a smaller buffer for easier testing

    // Signals
    logic clk;
    logic reset;
    logic [WORD_BITS-1:0] din;
    logic append;
    logic remove;
    logic [WORD_BITS-1:0] dout;
    logic full;
    logic empty;

    // Instantiate the FIFO
    fifo #(
        .WORD_BITS(WORD_BITS),
        .BUFFER_LEN(BUFFER_LEN)
    ) uut (
        .clk(clk),
        .reset(reset),
        .din(din),
        .append(append),
        .remove(remove),
        .dout(dout),
        .full(full),
        .empty(empty)
    );

    // Clock generation
    always #5 clk = ~clk;  // 10ns clock period

    // Test vectors
    initial begin
        // Initialize signals
        clk = 0;
        reset = 0;
        din = 0;
        append = 0;
        remove = 0;

        // Apply reset
        $display("Applying reset...");
        reset = 1;
        #10;
        reset = 0;
        #10;
        assert(empty == 1) else $fatal("FIFO should be empty after reset");
        assert(full == 0) else $fatal("FIFO should not be full after reset");
        $display("Reset test passed");

        // Write data into the FIFO
        $display("Writing data to FIFO...");
        append = 1;
        for (int i = 0; i < BUFFER_LEN; i++) begin
            din = i;
            #10;
            assert(full == (i == BUFFER_LEN-1)) else $fatal("FIFO full flag incorrect");
        end
        append = 0;
        #10;
        assert(full == 1) else $fatal("FIFO should be full");
        assert(empty == 0) else $fatal("FIFO should not be empty");
        $display("Write test passed");

        // Read data from the FIFO
        $display("Reading data from FIFO...");
        remove = 1;
        for (int i = 0; i < BUFFER_LEN; i++) begin
            #10;
            assert(dout == i) else $fatal("FIFO read data mismatch");
            assert(empty == (i == BUFFER_LEN-1)) else $fatal("FIFO empty flag incorrect");
        end
        remove = 0;
        #10;
        assert(empty == 1) else $fatal("FIFO should be empty");
        assert(full == 0) else $fatal("FIFO should not be full");
        $display("Read test passed");

        // Test wrap-around
        $display("Testing wrap-around...");
        append = 1;
        for (int i = 0; i < BUFFER_LEN; i++) begin
            din = i + 10;
            #10;
        end
        append = 0;
        remove = 1;
        for (int i = 0; i < BUFFER_LEN; i++) begin
            #10;
            assert(dout == i + 10) else $fatal("FIFO wrap-around data mismatch");
        end
        remove = 0;
        #10;
        assert(empty == 1) else $fatal("FIFO should be empty after wrap-around");
        assert(full == 0) else $fatal("FIFO should not be full after wrap-around");
        $display("Wrap-around test passed");

        // Test simultaneous append and remove
        $display("Testing simultaneous append and remove...");
        din = 100;
        append = 1;
        remove = 1;
        #10;
        assert(dout == 100) else $fatal("Simultaneous append/remove data mismatch");
        append = 0;
        remove = 0;
        #10;
        assert(empty == 1) else $fatal("FIFO should be empty after simultaneous operation");
        $display("Simultaneous append and remove test passed");

        $display("All tests passed successfully!");
        $finish;
    end
endmodule
