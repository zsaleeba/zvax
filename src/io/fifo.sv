/*
================================================================================
                                FIFO Module
================================================================================
Description:
------------
This module implements a First-In-First-Out (FIFO) buffer with parameterizable 
word width and buffer length. The FIFO operates synchronously with a single 
clock signal and supports independent append and remove operations. The module 
also provides status flags to indicate when the FIFO is full or empty. The 
`dout` output always presents the next item in the FIFO, but only removes it 
if the `remove` signal is asserted on the clock.

Parameters:
-----------
- WORD_SIZE (default: 8 bits):
    Defines the width of the data that the FIFO will store. The width of the 
    input data (`din`) and the output data (`dout`) is determined by this 
    parameter.

- BUFFER_LEN (default: 64 words):
    Defines the depth of the FIFO, i.e., the maximum number of words that can 
    be stored in the FIFO at any given time. The FIFO can hold up to 
    `BUFFER_LEN` elements.

Inputs:
-------
- clk (1 bit):
    The clock signal. All operations (append and remove) are synchronized to 
    the rising edge of this clock.

- reset (1 bit):
    Active-high reset signal. When asserted, this signal resets the FIFO, 
    clearing all stored data, and resetting the read and write pointers and 
    the count of stored elements to zero.

- din (WORD_SIZE bits):
    Data input bus. This signal provides the data to be stored in the FIFO when 
    the `append` input is asserted.

- append (1 bit):
    Append control signal. When this signal is asserted (high) and the FIFO is 
    not full, the data on the `din` bus is written into the FIFO at the next 
    rising clock edge.

- remove (1 bit):
    Remove control signal. When this signal is asserted (high) and the FIFO is 
    not empty, the data at the current read pointer location is removed from 
    the FIFO at the next rising clock edge. The `dout` signal always presents 
    the next item in the FIFO, regardless of whether `remove` is asserted.

Outputs:
--------
- dout (WORD_SIZE bits):
    Data output bus. This signal carries the data being read from the FIFO and 
    always presents the next item to be removed, even if `remove` is not 
    asserted.

- full (1 bit):
    Full status flag. This signal is asserted (high) when the FIFO is full, 
    indicating that no more data can be written until some data is removed.

- empty (1 bit):
    Empty status flag. This signal is asserted (high) when the FIFO is empty, 
    indicating that no data is available for reading.

Operation:
----------
The FIFO operates using a dual-pointer mechanism with `write_ptr` for writing 
and `read_ptr` for reading. A counter (`count`) keeps track of the number of 
elements currently stored in the FIFO.

- Write Operation (Append):
  When the `append` signal is asserted, and the FIFO is not full, the data on 
  the `din` bus is written into the memory location indicated by `write_ptr`. 
  The `write_ptr` is then incremented (with wrap-around logic) and the `count` 
  is incremented to reflect the additional data in the FIFO.

- Read Operation (Remove):
  When the `remove` signal is asserted, and the FIFO is not empty, the data at 
  the memory location indicated by `read_ptr` is removed from the FIFO, and 
  the `read_ptr` is incremented (with wrap-around logic). The `count` is 
  decremented to reflect the removal of data from the FIFO.

- Output Logic:
  The `dout` signal always presents the data stored at the memory location 
  indicated by `read_ptr`, providing the next item in the FIFO, regardless of 
  whether `remove` is asserted.

- Full and Empty Flags:
  The `full` flag is asserted when `count` equals `BUFFER_LEN`, indicating that 
  the FIFO cannot accept more data. The `empty` flag is asserted when `count` 
  equals zero, indicating that no data is available for reading.

Overall, this FIFO module is suitable for buffering data streams in 
applications where data needs to be stored temporarily before being processed 
or transmitted, with guarantees of in-order processing.
*/

module fifo #(
    parameter WORD_SIZE = 8,
    parameter BUFFER_LEN = 64
)(
    input logic clk,
    input logic reset,
    input logic [WORD_SIZE-1:0] din,
    input logic append,
    input logic remove,
    output logic [WORD_SIZE-1:0] dout,
    output logic full,
    output logic empty
);

    // Internal parameters
    localparam ADDR_WIDTH = $clog2(BUFFER_LEN);

    // Internal signals
    logic [WORD_SIZE-1:0] fifo_mem [BUFFER_LEN-1:0];
    logic [ADDR_WIDTH-1:0] write_ptr;
    logic [ADDR_WIDTH-1:0] read_ptr;
    logic [ADDR_WIDTH:0] count;

    // Write logic
    always_ff @(posedge clk or posedge reset) begin
        if (reset) begin
            write_ptr <= 0;
            count <= 0;
        end else if (append && !full) begin
            fifo_mem[write_ptr] <= din;
            write_ptr <= write_ptr + 1;
            count <= count + 1;
        end
    end

    // Read logic
    always_ff @(posedge clk or posedge reset) begin
        if (reset) begin
            read_ptr <= 0;
        end else if (remove && !empty) begin
            read_ptr <= read_ptr + 1;
            count <= count - 1;
        end
    end

    // Output logic: Always present the next item
    always_ff @(posedge clk or posedge reset) begin
        if (reset) begin
            dout <= 0;
        end else if (!empty) begin
            dout <= fifo_mem[read_ptr];
        end
    end

    // Full and empty flags
    assign full = (count == BUFFER_LEN);
    assign empty = (count == 0);

endmodule
